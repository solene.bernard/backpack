# TOOLS
import sys, os
sys.path.append('models/')

from statsmodels.distributions.empirical_distribution import ECDF
import numpy as np
from backpack import BackPack
from srnet import TrainGlobalConfig as TrainGlobalConfig_sr
from srnet import get_net as get_net_sr
from xunet import TrainGlobalConfig as TrainGlobalConfig_xu
from xunet import get_net as get_net_xu
from efficientnet import TrainGlobalConfig as TrainGlobalConfig_ef
from efficientnet import get_net as get_net_ef
import os
import argparse
import sys

import torch
import torch.nn as nn
import torch.nn.functional as F



def backpack_attack(data_dir_cover, cost_dir, image_size, QF, folder_model, emb_rate,
                    N_samples, nets, image_path, tau_0, ecdf_list, attack, lr, precision, n_iter_max_backpack):

    c_coeffs = np.load(data_dir_cover + image_path)
    nz_AC = np.sum(c_coeffs != 0)-np.sum(c_coeffs[::8, ::8] != 0)
    rho = np.load(cost_dir+image_path)
    if(rho.shape == (image_size, image_size)):
        rho = np.reshape(rho, (1, image_size, image_size))
        rho = np.concatenate(
            (np.copy(rho), np.zeros_like(rho), np.copy(rho)), axis=0)
        rho[0, c_coeffs < -1023] = 10e30
        rho[2, c_coeffs > 1023] = 10e30
    entropy = emb_rate*nz_AC

    tau = tau_0
    backpack = BackPack(image_size, QF, folder_model, c_coeffs,
                        rho, entropy, N_samples, nets, ecdf_list, attack)
    optimizer = torch.optim.Adam([backpack.rho_vec], lr=lr)
    proba_cover = backpack.proba_cover
    best_probas_soft, mean_probas_soft, mean_probas_hard, stego_hard = backpack(
        tau)
    mean_p_soft = mean_probas_soft
    mean_p_hard = mean_probas_hard

    i = 0
    print(proba_cover, mean_p_soft, mean_p_hard)

    while True:

        while((np.max(mean_p_soft-proba_cover) > precision) and (np.max(mean_p_hard-proba_cover) > precision) and (i < n_iter_max_backpack)):

            optimizer.zero_grad()
            best_probas_soft, mean_probas_soft, mean_probas_hard, stego_hard = backpack(
                tau)
            best_probas_soft.backward()

            # g = torch.autograd.grad(best_probas_soft, backpack.rho_vec)[0]
            # if(torch.isnan(g).sum()>0):
            #     print(np.where(torch.isnan(g)))
            optimizer.step()

            mean_p_soft = mean_probas_soft
            mean_p_hard = mean_probas_hard

            lr = optimizer.param_groups[0]['lr']
            print(i, np.round(mean_p_soft, 2), np.round(
                mean_p_hard, 2), np.round(tau, 4), np.round(lr, 4))

            #tab.append([mean_p_soft, mean_p_hard, tau])

            i += 1

            if(torch.isnan(backpack.rho_vec).sum() > 0):
                print('Nan in cost map')
                return(None, None)

        if((np.max(mean_p_hard-proba_cover) <= precision) or (i >= n_iter_max_backpack)):
            return(backpack.rho_vec.cpu().detach().numpy(), stego_hard.cpu().detach().numpy()[0, 0])
        else:
            tau /= 2
            best_probas_soft, mean_probas_soft, mean_probas_hard, stego_hard = backpack(
                tau)
            mean_p_soft = mean_probas_soft
            mean_p_hard = mean_probas_hard


def run_attack(iteration_step, folder_model, data_dir_prot, data_dir_cover, cost_dir,
               image_size, QF, emb_rate,  model, version_eff, stride, n_loops, attack_last,
               permutation_files, attack, tau_0, lr, precision, n_iter_max_backpack, N_samples, idx_start, batch_adv):

    models = model.split(',')
    attack_last = attack_last == 'yes'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    nets = []

    ecdf_list = []  # For calibration
    if(attack_last):
        n_classifier_min = max(iteration_step-3, 0)
    else:
        n_classifier_min = 0
    for i in range(n_classifier_min, iteration_step):
        for model in models:
            if(model == 'efnet'):
                net = get_net_ef(version_eff, stride).to(device)
            elif(model == 'xunet'):
                net = get_net_xu(folder_model, n_loops, image_size).to(device)
            elif(model == 'srnet'):
                net = get_net_sr(image_size).to(device)

            paths = os.listdir(data_dir_prot+'train_'+model+'_'+str(i)+'/')
            paths = [int(x.split('-')[-1][:-9])
                     for x in paths if 'best-checkpoint' in x]
            best_epoch = str(max(paths))
            path = data_dir_prot+'train_'+model+'_' + \
                str(i)+'/best-checkpoint-'+'0' * \
                (3-len(best_epoch))+best_epoch+'epoch.bin'

            checkpoint = torch.load(path)
            net.load_state_dict(checkpoint['model_state_dict'])
            net.eval()
            for x in net.parameters():
                x.requires_grad = False
            nets.append(net)
            ecdf = ECDF(np.load(data_dir_prot+'cover/eval_' +
                                model+'_'+str(i)+'/probas.npy'))
            ecdf_list.append(ecdf)

    files = np.load(permutation_files)
    files = files[idx_start*batch_adv:(idx_start+1)*batch_adv]
    files = files[np.random.permutation(len(files))]
    print(files)

    path_save = data_dir_prot + "data_adv_"+str(iteration_step) + "/"

    for image_path in files:
        if not os.path.exists(path_save+'adv_cost/'+image_path[:-4]+'.npy'):
            rho, stego = backpack_attack(data_dir_cover, cost_dir, image_size, QF,
                                         folder_model, emb_rate, N_samples, nets, image_path[
                                             :-4]+'.npy', tau_0, ecdf_list,
                                         attack, lr, precision, n_iter_max_backpack)
            np.save(path_save+'adv_cost/'+image_path[:-4]+'.npy', rho)
            np.save(path_save+'adv_final/'+image_path[:-4]+'.npy', stego)


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--iteration_step', type=int)

    argparser.add_argument('--data_dir_prot', type=str)
    argparser.add_argument('--data_dir_cover', type=str)
    argparser.add_argument('--cost_dir', type=str)
    argparser.add_argument('--permutation_files', type=str)
    argparser.add_argument('--folder_model', type=str)
    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--emb_rate', type=float)
    argparser.add_argument('--QF', type=int)

    # Model parameters
    argparser.add_argument(
        '--model', type=str, help='Model : efnet, xunet or srnet or multiple models separated by comma')
    # for efnet
    argparser.add_argument('--version_eff', type=str,
                           help='Version of efficient-net, from b0 to b7')
    argparser.add_argument('--stride', type=int,
                           help='Stride at the beginning. Values=1 or 2')
    # for xunet
    argparser.add_argument('--n_loops', type=int,
                           help='Number of loops in the xunet architecture')

    # For SGD
    argparser.add_argument('--attack', type=str)
    argparser.add_argument('--attack_last', type=str, default='no')
    argparser.add_argument('--lr', type=float)
    argparser.add_argument('--n_iter_max_backpack', type=int)
    argparser.add_argument('--N_samples', type=int)
    argparser.add_argument('--tau_0', type=float)
    argparser.add_argument('--precision', type=float)

    argparser.add_argument('--idx_start', type=int)
    argparser.add_argument('--batch_adv', type=int)
    params = argparser.parse_args()

    run_attack(**vars(params))
