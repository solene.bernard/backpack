import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class get_net(nn.Module):

    def __init__(self, image_size):
        super(get_net, self).__init__()
        self.im_size = image_size
        
        def _conv2d(in_channels, out_channels, stride=1, kernel_size=3, padding=1):
            return nn.Conv2d(in_channels=in_channels,\
                            out_channels=out_channels,\
                            kernel_size=kernel_size, \
                            stride=stride, \
                            padding=padding, \
                            bias=True)

        def _batch_norm(in_channels):
            return torch.nn.BatchNorm2d(in_channels, \
                            eps=1e-05, \
                            momentum=0.1, \
                            affine=True, \
                            track_running_stats=True)

       
        # Declare parameters thanks to ModuleList since we are looping
        block = nn.ModuleList([]) 

        # 2 layers of type 1
        block.append(_conv2d(in_channels=1, out_channels=64))
        block.append(_batch_norm(in_channels=64))
        block.append(_conv2d(in_channels=64, out_channels=16))
        block.append(_batch_norm(in_channels=16))

        # 5 layers of type 2
        for i in range(5):
            # layer of type 1
            block.append(_conv2d(in_channels=16, out_channels=16))
            block.append(_batch_norm(in_channels=16))
            # Conv and BN
            block.append(_conv2d(in_channels=16, out_channels=16))
            block.append(_batch_norm(in_channels=16))

        # 4 layers of type 3
        in_chan, prev_chan = 16, 16
        for i in range(4):
            # layer of type 1
            block.append(_conv2d(in_channels=prev_chan, out_channels=in_chan))
            block.append(_batch_norm(in_channels=in_chan))
            # Conv and BN
            block.append(_conv2d(in_channels=in_chan, out_channels=in_chan))
            block.append(_batch_norm(in_channels=in_chan))
            # Shortcut
            block.append(_conv2d(in_channels=prev_chan, out_channels=in_chan, \
                    stride=2, kernel_size=1, padding=0))
            block.append(_batch_norm(in_channels=in_chan))
            prev_chan = in_chan
            if(i==0):
                in_chan*=4
            else:
                in_chan*=2

        # 1 layer of type 4
        block.append(_conv2d(in_channels=256, out_channels=512))
        block.append(_batch_norm(in_channels=512))
        block.append(_conv2d(in_channels=512, out_channels=512))
        block.append(_batch_norm(in_channels=512))

        # Fully connected layer
        block.append(torch.nn.Linear(512, 2, bias=False))

        self.block = block
                

    def init(self):
        def weights_init(m):
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_uniform_(m.weight.data)
                nn.init.constant_(m.bias.data, 0.2)
            if isinstance(m, nn.Linear):
                nn.init.normal_(m.weight.data, mean=0.0, std=0.01)
        self.apply(weights_init)
        

    def forward(self, x):

        x *= 255.

        # 2 layers of type 1
        for i in range(2):
            x = self.block[i*2+0](x)
            x = self.block[i*2+1](x)
            x = torch.nn.ReLU()(x)

        # 5 layers of type 2
        for i in range(5):
            # layer of type 1
            y = self.block[4+i*4+0](x)
            y = self.block[4+i*4+1](y)
            y = torch.nn.ReLU()(y)
            # Conv and BN
            y = self.block[4+i*4+2](y)
            y = self.block[4+i*4+3](y)
            x = x + y
     
        # 4 layers of type 3
        for i in range(4):
            # layer of type 1
            y = self.block[24+i*6+0](x)
            y = self.block[24+i*6+1](y)
            y = torch.nn.ReLU()(y)
            # Conv and BN
            y = self.block[24+i*6+2](y)
            y = self.block[24+i*6+3](y)
            # Avg pooling
            y = torch.nn.AvgPool2d(kernel_size=3, stride=2, padding=1)(y)
            # Shortcut
            z = self.block[24+i*6+4](x)
            z = self.block[24+i*6+5](z)
            x = z + y
        
        # 1 layer of type 4
        x = self.block[48+0](x)
        x = self.block[48+1](x)
        x = torch.nn.ReLU()(x)
        # Conv and BN
        x = self.block[48+2](x)
        x = self.block[48+3](x)
        # Global avg pooling
        w_s = self.im_size//(2**4) # size of the output depends on input size image
        x = torch.nn.AvgPool2d(kernel_size=w_s, stride=1, padding=0)(x)
        
        # Fully connected layer     
        x = x[:,:,0,0].unsqueeze_(1) # Correct shape
        x = self.block[-1](x)
    
        return(x[:,0])

# CONFIG
class TrainGlobalConfig():

    def __init__(self, num_of_threads, batch_size_classif, epoch_num):
        self.num_workers = num_of_threads
        self.batch_size = batch_size_classif
        self.n_epochs = epoch_num
        self.lr = 0.001
        self.verbose = True
        self.verbose_step = 1
        self.step_scheduler = False  # do scheduler.step after optimizer.step
        self.validation_scheduler = True  # do scheduler.step after validation stage loss
    
        self.SchedulerClass = torch.optim.lr_scheduler.ReduceLROnPlateau
        self.scheduler_params = dict(
            mode='min',
            factor=0.5,
            patience=1,
            verbose=False, 
            threshold=0.0001,
            threshold_mode='abs',
            cooldown=0, 
            min_lr=1e-8,
            eps=1e-08
        )


