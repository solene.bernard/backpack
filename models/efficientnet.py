from efficientnet_pytorch.utils import efficientnet
from efficientnet_pytorch import EfficientNet
import torch

import torch.nn as nn
import torch.nn.functional as F

def get_net(version_eff, stride=1):

    n_channels_dict = {'efficientnet-b0': 1280, 'efficientnet-b1': 1280, 'efficientnet-b2': 1408,
                           'efficientnet-b3': 1536, 'efficientnet-b4': 1792, 'efficientnet-b5': 2048,
                           'efficientnet-b6': 2304, 'efficientnet-b7': 2560}

    model = 'efficientnet-'+version_eff
    in_features = n_channels_dict[model]
    
    net = EfficientNet.from_name(model)

    # For conv stem stride=1
    if(stride==1):
        net._conv_stem.in_channels = 1 
        net._conv_stem.stride=1

    # Si tu veux des images grayscale sans avoir à recopier l'image trois fois, on peut soit prendre une partie des poids, soit les sommer
    # Pas de différence chez moi, donc je prend juste les poids du premier canal
    net._conv_stem.weight = torch.nn.Parameter(net._conv_stem.weight[:,0:1,:,:])#torch.cat([net._conv_stem.weight, net._conv_stem.weight], axis=1))    
    net._fc = nn.Linear(in_features=in_features, out_features=2, bias=True) #b2 1408, b3 1536

    return net


# CONFIG
class TrainGlobalConfig():

    def __init__(self, num_of_threads, batch_size_classif, epoch_num):
        self.num_workers = num_of_threads
        self.batch_size = batch_size_classif
        self.n_epochs = epoch_num
        self.lr = 0.0005
        self.verbose = True
        self.verbose_step = 1
        self.step_scheduler = False  # do scheduler.step after optimizer.step
        self.validation_scheduler = True  # do scheduler.step after validation stage loss
    
        self.SchedulerClass = torch.optim.lr_scheduler.ReduceLROnPlateau
        self.scheduler_params = dict(
            mode='min',
            factor=0.5,
            patience=1,
            verbose=False, 
            threshold=0.0001,
            threshold_mode='abs',
            cooldown=0, 
            min_lr=1e-8,
            eps=1e-08
        )
    




