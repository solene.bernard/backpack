import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np



class get_net(nn.Module):

    def __init__(self, folder_model, n_loops, image_size):
        super(get_net, self).__init__()
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.n_loops = n_loops
        self.DCT4_kernel = torch.tensor(np.load(folder_model+'DCT_4.npy')\
                                         .reshape((4,4,16,1)).transpose((2,3,0,1))).to(self.device).float()
        self.im_size = image_size
        
        def _conv2d(in_channels, out_channels, stride):
            return nn.Conv2d(in_channels=in_channels,\
                            out_channels=out_channels,\
                            kernel_size=3, \
                            stride=stride, \
                            padding=1, \
                            bias=False)

        def _batch_norm(in_channels):
            return torch.nn.BatchNorm2d(in_channels, \
                            eps=1e-05, \
                            momentum=0.1, \
                            affine=True, \
                            track_running_stats=True)
        
        # Declare parameters thanks to ModuleList since we are looping
        block = nn.ModuleList([]) 
        for i in range(self.n_loops):
        
            # DECLARE PARAMETERS FOR BIG BLOCK with conv2d in shortcuts
            in_channels = 12*(2**i)
            if i==0: # If it is the first conv_block in the CNN
                in_ch=16
            else:
                in_ch=in_channels
            # Main path
            # First subblock
            block.append(_conv2d(in_ch, in_channels, 1))
            block.append(_batch_norm(in_channels))
            # Second subblock 
            block.append(_conv2d(in_channels, 2*in_channels, 2))
            block.append(_batch_norm(2*in_channels))
            # Second path : shortcut
            block.append(_conv2d(in_ch, 2*in_channels, 2))
            block.append(_batch_norm(2*in_channels))
            
            # DECLAIRE PARAMETERS FOR BIG BLOCK without conv2d in shortcuts
            # First subblock 
            block.append(_conv2d(2*in_channels, 2*in_channels, 1))
            block.append(_batch_norm(2*in_channels))
            # Second subblock
            block.append(_conv2d(2*in_channels, 2*in_channels, 1))
            block.append(_batch_norm(2*in_channels))
        
        # Fully connected
        in_features = 12*(2**(self.n_loops))
        block.append(torch.nn.Linear(in_features, 2, bias=False))
    
        self.block = block
                
        

    def forward(self, x):
        x *= 255.
        # DCT4 filters
        x = F.conv2d(x, self.DCT4_kernel, stride=1, padding=1) 
        x = torch.abs(x)
        x = torch.clamp(x, 0, 8)

        # N loops
        for i in range(self.n_loops):
            # First block with conv2D in shortcut
            y = self.block[i*10+0](x)
            y = self.block[i*10+1](y)
            y = torch.nn.ReLU()(y)
            y = self.block[i*10+2](y)
            y = self.block[i*10+3](y)
            # Shortcut
            z = self.block[i*10+4](x)
            z = self.block[i*10+5](z) 
            x = z + y # Shortcut
            
            # Second block without conv2D in shortcut
            x = torch.nn.ReLU()(x)
            y = self.block[i*10+6](x)
            y = self.block[i*10+7](y)
            y = torch.nn.ReLU()(y)
            y = self.block[i*10+8](y)
            y = self.block[i*10+9](y)
            x = y + x # Shortcut
            x = torch.nn.ReLU()(x)
        
        w_s = int(self.im_size/2**(self.n_loops)) # size of the output depends on the number of loops and input size image
        x = torch.nn.AvgPool2d(kernel_size=[w_s,w_s])(x)
        # Fully connected layer      
        x = x[:,:,0,0].unsqueeze_(1) # Correct shape
        x = self.block[-1](x)
        
        return(x[:,0])

# CONFIG
class TrainGlobalConfig():

    def __init__(self, num_of_threads, batch_size_classif, epoch_num):
        self.num_workers = num_of_threads
        self.batch_size = batch_size_classif
        self.n_epochs = epoch_num
        self.lr = 0.001
        self.verbose = True
        self.verbose_step = 1
        self.step_scheduler = False  # do scheduler.step after optimizer.step
        self.validation_scheduler = True  # do scheduler.step after validation stage loss
    
        # self.SchedulerClass = torch.optim.lr_scheduler.StepLR
        # self.scheduler_params = dict(
        #     step_size=5000, 
        #     gamma=0.9, 
        #     last_epoch=-1, 
        #     verbose=False
        # )
        self.SchedulerClass = torch.optim.lr_scheduler.ReduceLROnPlateau
        self.scheduler_params = dict(
            mode='min',
            factor=0.5,
            patience=1,
            verbose=False, 
            threshold=0.0001,
            threshold_mode='abs',
            cooldown=0, 
            min_lr=1e-8,
            eps=1e-08
        )