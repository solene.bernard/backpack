Fitter prepared. Device is cuda:0

2021-04-26T09:47:45.181104
LR: 0.001
Emb_rate: 1.2
[RESULT]: Train. Epoch: 0, summary_loss: 0.46653, final_score: 0.20845, time: 647.80818
[RESULT]: Val. Epoch: 0, summary_loss: 2.61759, final_score: 0.48002, time: 199.41531

2021-04-26T10:01:52.834758
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 1, summary_loss: 0.18362, final_score: 0.01900, time: 637.79749
[RESULT]: Val. Epoch: 1, summary_loss: 1.48727, final_score: 0.47902, time: 215.57560

2021-04-26T10:16:06.553729
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 2, summary_loss: 0.16076, final_score: 0.01137, time: 647.79765
[RESULT]: Val. Epoch: 2, summary_loss: 1.22666, final_score: 0.45055, time: 203.14394

2021-04-26T10:30:17.971094
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 3, summary_loss: 0.16445, final_score: 0.01225, time: 647.51563
[RESULT]: Val. Epoch: 3, summary_loss: 2.11409, final_score: 0.47103, time: 205.91400

2021-04-26T10:44:31.602627
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 4, summary_loss: 0.17419, final_score: 0.01687, time: 655.63127
[RESULT]: Val. Epoch: 4, summary_loss: 3.01678, final_score: 0.48002, time: 203.66919

2021-04-26T10:58:51.093886
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 5, summary_loss: 0.17580, final_score: 0.01712, time: 653.53254
[RESULT]: Val. Epoch: 5, summary_loss: 1.38926, final_score: 0.44705, time: 202.57373

2021-04-26T11:13:07.393861
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.15821, final_score: 0.01025, time: 679.21056
[RESULT]: Val. Epoch: 6, summary_loss: 1.43767, final_score: 0.45854, time: 202.87938

2021-04-26T11:27:49.657280
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.18987, final_score: 0.02537, time: 686.23749
[RESULT]: Val. Epoch: 7, summary_loss: 2.50336, final_score: 0.46853, time: 202.90086

2021-04-26T11:42:38.978270
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.18251, final_score: 0.02149, time: 673.36028
[RESULT]: Val. Epoch: 8, summary_loss: 1.99218, final_score: 0.45355, time: 202.70501

2021-04-26T11:57:15.203687
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.19793, final_score: 0.03012, time: 676.62080
[RESULT]: Val. Epoch: 9, summary_loss: 1.78947, final_score: 0.44356, time: 202.46851

2021-04-26T12:11:54.464370
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 10, summary_loss: 0.19506, final_score: 0.02937, time: 669.54129
[RESULT]: Val. Epoch: 10, summary_loss: 2.69203, final_score: 0.46304, time: 203.21163

2021-04-26T12:26:27.408122
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 11, summary_loss: 0.20559, final_score: 0.03487, time: 685.89687
[RESULT]: Val. Epoch: 11, summary_loss: 1.96020, final_score: 0.46553, time: 202.38798

2021-04-26T12:41:15.890282
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 12, summary_loss: 0.21981, final_score: 0.04299, time: 680.28723
[RESULT]: Val. Epoch: 12, summary_loss: 1.85339, final_score: 0.43856, time: 203.07733

2021-04-26T12:55:59.416682
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 13, summary_loss: 0.24046, final_score: 0.05399, time: 686.06290
[RESULT]: Val. Epoch: 13, summary_loss: 1.28382, final_score: 0.43157, time: 203.11228

2021-04-26T13:10:48.777815
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 14, summary_loss: 0.22656, final_score: 0.04674, time: 672.47998
[RESULT]: Val. Epoch: 14, summary_loss: 1.77621, final_score: 0.42308, time: 203.64534

2021-04-26T13:25:25.114927
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 15, summary_loss: 0.24707, final_score: 0.06098, time: 679.90146
[RESULT]: Val. Epoch: 15, summary_loss: 1.93670, final_score: 0.44456, time: 202.74944

2021-04-26T13:40:08.031517
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 16, summary_loss: 0.25391, final_score: 0.06511, time: 681.78799
[RESULT]: Val. Epoch: 16, summary_loss: 1.99685, final_score: 0.43606, time: 203.62761

2021-04-26T13:54:53.641344
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 17, summary_loss: 0.28172, final_score: 0.07836, time: 680.97461
[RESULT]: Val. Epoch: 17, summary_loss: 1.05556, final_score: 0.42308, time: 206.22731

2021-04-26T14:09:41.304739
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 18, summary_loss: 0.26787, final_score: 0.07048, time: 681.62578
[RESULT]: Val. Epoch: 18, summary_loss: 1.66519, final_score: 0.42358, time: 206.34734

2021-04-26T14:24:29.483820
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 19, summary_loss: 0.30682, final_score: 0.09473, time: 680.64729
[RESULT]: Val. Epoch: 19, summary_loss: 1.18940, final_score: 0.41359, time: 206.65576

2021-04-26T14:39:17.017795
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 20, summary_loss: 0.30944, final_score: 0.09873, time: 682.66315
[RESULT]: Val. Epoch: 20, summary_loss: 1.04541, final_score: 0.41259, time: 206.19856

2021-04-26T14:54:06.227823
LR: 0.001
Emb_rate: 0.3765727153080001
[RESULT]: Train. Epoch: 21, summary_loss: 0.32869, final_score: 0.10922, time: 680.17991
[RESULT]: Val. Epoch: 21, summary_loss: 1.80547, final_score: 0.43457, time: 205.68367

2021-04-26T15:08:52.260713
LR: 0.001
Emb_rate: 0.3765727153080001
[RESULT]: Train. Epoch: 22, summary_loss: 0.32664, final_score: 0.10835, time: 667.00014
[RESULT]: Val. Epoch: 22, summary_loss: 1.57916, final_score: 0.41558, time: 203.72627

2021-04-26T15:23:23.175459
LR: 0.001
Emb_rate: 0.3389154437772001
[RESULT]: Train. Epoch: 23, summary_loss: 0.36265, final_score: 0.13134, time: 673.88407
[RESULT]: Val. Epoch: 23, summary_loss: 1.24792, final_score: 0.41459, time: 205.94042

2021-04-26T15:38:03.172040
LR: 0.001
Emb_rate: 0.3389154437772001
[RESULT]: Train. Epoch: 24, summary_loss: 0.34949, final_score: 0.12209, time: 679.86101
[RESULT]: Val. Epoch: 24, summary_loss: 1.53584, final_score: 0.41159, time: 202.54676

2021-04-26T15:52:45.816931
LR: 0.001
Emb_rate: 0.3050238993994801
[RESULT]: Train. Epoch: 25, summary_loss: 0.38895, final_score: 0.14809, time: 672.69777
[RESULT]: Val. Epoch: 25, summary_loss: 1.11786, final_score: 0.39710, time: 203.52531

2021-04-26T16:07:22.233515
LR: 0.001
Emb_rate: 0.3050238993994801
[RESULT]: Train. Epoch: 26, summary_loss: 0.37853, final_score: 0.14584, time: 671.95245
[RESULT]: Val. Epoch: 26, summary_loss: 1.27258, final_score: 0.41309, time: 203.50339

2021-04-26T16:21:57.978027
LR: 0.001
Emb_rate: 0.2745215094595321
[RESULT]: Train. Epoch: 27, summary_loss: 0.41055, final_score: 0.16033, time: 670.03026
[RESULT]: Val. Epoch: 27, summary_loss: 0.97403, final_score: 0.39311, time: 206.48937

2021-04-26T16:36:34.850832
LR: 0.001
Emb_rate: 0.2745215094595321
[RESULT]: Train. Epoch: 28, summary_loss: 0.40955, final_score: 0.16421, time: 666.72797
[RESULT]: Val. Epoch: 28, summary_loss: 0.96333, final_score: 0.38462, time: 204.16401

2021-04-26T16:51:06.180013
LR: 0.001
Emb_rate: 0.24706935851357886
[RESULT]: Train. Epoch: 29, summary_loss: 0.43107, final_score: 0.18033, time: 672.73569
[RESULT]: Val. Epoch: 29, summary_loss: 1.61924, final_score: 0.39910, time: 203.56923
Fitter prepared. Device is cuda:0

2021-04-28T10:05:02.001047
LR: 0.001
Emb_rate: 0.24
[RESULT]: Train. Epoch: 30, summary_loss: 0.43901, final_score: 0.18583, time: 670.03966
[RESULT]: Val. Epoch: 30, summary_loss: 0.98015, final_score: 0.38362, time: 201.49861

2021-04-28T10:19:33.723778
LR: 0.001
Emb_rate: 0.216
[RESULT]: Train. Epoch: 31, summary_loss: 0.46583, final_score: 0.20482, time: 662.73002
[RESULT]: Val. Epoch: 31, summary_loss: 0.84601, final_score: 0.38112, time: 200.20963

2021-04-28T10:33:57.297513
LR: 0.001
Emb_rate: 0.216
[RESULT]: Train. Epoch: 32, summary_loss: 0.45929, final_score: 0.19958, time: 664.37477
[RESULT]: Val. Epoch: 32, summary_loss: 1.19469, final_score: 0.37762, time: 199.11684

2021-04-28T10:48:20.965669
LR: 0.001
Emb_rate: 0.1944
[RESULT]: Train. Epoch: 33, summary_loss: 0.48782, final_score: 0.22469, time: 668.25154
[RESULT]: Val. Epoch: 33, summary_loss: 0.97575, final_score: 0.37862, time: 200.39152

2021-04-28T11:02:49.810846
LR: 0.001
Emb_rate: 0.1944
[RESULT]: Train. Epoch: 34, summary_loss: 0.48539, final_score: 0.22682, time: 665.46960
[RESULT]: Val. Epoch: 34, summary_loss: 0.86914, final_score: 0.38262, time: 199.91907

2021-04-28T11:17:15.380361
LR: 0.001
Emb_rate: 0.17496
[RESULT]: Train. Epoch: 35, summary_loss: 0.50773, final_score: 0.24319, time: 665.99118
[RESULT]: Val. Epoch: 35, summary_loss: 1.88383, final_score: 0.42158, time: 200.60358

2021-04-28T11:31:42.297315
LR: 0.001
Emb_rate: 0.17496
[RESULT]: Train. Epoch: 36, summary_loss: 0.50719, final_score: 0.24506, time: 676.49385
[RESULT]: Val. Epoch: 36, summary_loss: 0.68849, final_score: 0.35564, time: 199.61468

2021-04-28T11:46:18.805486
LR: 0.001
Emb_rate: 0.15746400000000002
[RESULT]: Train. Epoch: 37, summary_loss: 0.53387, final_score: 0.26381, time: 659.37483
[RESULT]: Val. Epoch: 37, summary_loss: 0.92357, final_score: 0.37213, time: 199.15123

2021-04-28T12:00:37.511071
LR: 0.001
Emb_rate: 0.15746400000000002
[RESULT]: Train. Epoch: 38, summary_loss: 0.52559, final_score: 0.25781, time: 662.77672
[RESULT]: Val. Epoch: 38, summary_loss: 0.87759, final_score: 0.37762, time: 202.62457

2021-04-28T12:15:03.104602
LR: 0.001
Emb_rate: 0.14171760000000003
[RESULT]: Train. Epoch: 39, summary_loss: 0.54556, final_score: 0.27543, time: 665.82074
[RESULT]: Val. Epoch: 39, summary_loss: 0.78534, final_score: 0.38412, time: 202.84920

2021-04-28T12:29:31.974626
LR: 0.001
Emb_rate: 0.14171760000000003
[RESULT]: Train. Epoch: 40, summary_loss: 0.53779, final_score: 0.27068, time: 675.80680
[RESULT]: Val. Epoch: 40, summary_loss: 1.08446, final_score: 0.39011, time: 202.78889

2021-04-28T12:44:10.754016
LR: 0.001
Emb_rate: 0.12754584000000002
[RESULT]: Train. Epoch: 41, summary_loss: 0.55730, final_score: 0.28705, time: 682.57216
[RESULT]: Val. Epoch: 41, summary_loss: 0.69114, final_score: 0.36014, time: 203.69537
