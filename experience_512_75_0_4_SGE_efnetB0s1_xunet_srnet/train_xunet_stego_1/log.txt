Fitter prepared. Device is cuda:0

2021-06-25T12:55:22.630832
LR: 0.001
Emb_rate: 1.2
Fitter prepared. Device is cuda:0

2021-06-25T12:58:18.790351
LR: 0.001
Emb_rate: 1.2
Fitter prepared. Device is cuda:0

2021-06-25T13:07:01.368832
LR: 0.001
Emb_rate: 1.2
[RESULT]: Train. Epoch: 0, summary_loss: 0.73446, final_score: 0.49838, time: 111.13417
[RESULT]: Val. Epoch: 0, summary_loss: 0.69378, final_score: 0.49550, time: 32.83594

2021-06-25T13:09:25.744286
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 1, summary_loss: 0.69342, final_score: 0.49638, time: 91.62950
[RESULT]: Val. Epoch: 1, summary_loss: 0.69320, final_score: 0.48951, time: 26.75422

2021-06-25T13:11:24.468287
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 2, summary_loss: 0.69085, final_score: 0.47051, time: 92.41708
[RESULT]: Val. Epoch: 2, summary_loss: 1.08113, final_score: 0.48102, time: 26.27322

2021-06-25T13:13:23.320956
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 3, summary_loss: 0.68161, final_score: 0.43564, time: 91.43521
[RESULT]: Val. Epoch: 3, summary_loss: 0.97471, final_score: 0.47303, time: 26.43233

2021-06-25T13:15:21.382652
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 4, summary_loss: 0.66102, final_score: 0.39303, time: 90.25668
[RESULT]: Val. Epoch: 4, summary_loss: 1.12363, final_score: 0.43706, time: 26.39587

2021-06-25T13:17:18.200622
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 5, summary_loss: 0.62603, final_score: 0.33892, time: 90.91511
[RESULT]: Val. Epoch: 5, summary_loss: 1.49882, final_score: 0.41359, time: 26.31770

2021-06-25T13:19:15.598248
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.59905, final_score: 0.30717, time: 93.01556
[RESULT]: Val. Epoch: 6, summary_loss: 0.81680, final_score: 0.35614, time: 26.30480

2021-06-25T13:21:15.102413
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.56744, final_score: 0.27706, time: 93.30545
[RESULT]: Val. Epoch: 7, summary_loss: 1.51717, final_score: 0.32567, time: 26.70336

2021-06-25T13:23:15.288391
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.55521, final_score: 0.26643, time: 91.35354
[RESULT]: Val. Epoch: 8, summary_loss: 1.82578, final_score: 0.38611, time: 26.36872

2021-06-25T13:25:13.196253
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.53459, final_score: 0.25169, time: 92.21858
[RESULT]: Val. Epoch: 9, summary_loss: 0.63135, final_score: 0.28771, time: 26.26996

2021-06-25T13:27:12.036842
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 10, summary_loss: 0.52508, final_score: 0.24531, time: 92.47006
[RESULT]: Val. Epoch: 10, summary_loss: 0.65355, final_score: 0.28771, time: 25.92795

2021-06-25T13:29:10.604974
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 11, summary_loss: 0.50830, final_score: 0.23182, time: 92.84406
[RESULT]: Val. Epoch: 11, summary_loss: 0.59817, final_score: 0.22877, time: 27.11560

2021-06-25T13:31:10.944915
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 12, summary_loss: 0.49954, final_score: 0.22632, time: 93.63755
[RESULT]: Val. Epoch: 12, summary_loss: 0.80060, final_score: 0.29570, time: 26.84618

2021-06-25T13:33:11.600459
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 13, summary_loss: 0.47327, final_score: 0.20582, time: 95.09385
[RESULT]: Val. Epoch: 13, summary_loss: 0.55482, final_score: 0.21229, time: 26.12797

2021-06-25T13:35:13.171330
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 14, summary_loss: 0.47017, final_score: 0.20282, time: 93.79077
[RESULT]: Val. Epoch: 14, summary_loss: 0.61621, final_score: 0.27722, time: 26.84154

2021-06-25T13:37:14.055072
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 15, summary_loss: 0.46013, final_score: 0.19745, time: 93.04424
[RESULT]: Val. Epoch: 15, summary_loss: 0.78093, final_score: 0.30869, time: 26.10475

2021-06-25T13:39:13.395814
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 16, summary_loss: 0.45890, final_score: 0.19545, time: 93.48888
[RESULT]: Val. Epoch: 16, summary_loss: 0.58030, final_score: 0.22827, time: 26.02379

2021-06-25T13:41:13.074235
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 17, summary_loss: 0.45201, final_score: 0.19108, time: 90.53256
[RESULT]: Val. Epoch: 17, summary_loss: 0.73241, final_score: 0.23427, time: 26.39008

2021-06-25T13:43:10.156956
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 18, summary_loss: 0.44202, final_score: 0.18495, time: 93.57011
[RESULT]: Val. Epoch: 18, summary_loss: 1.22128, final_score: 0.29720, time: 26.87367

2021-06-25T13:45:10.777572
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 19, summary_loss: 0.43349, final_score: 0.18158, time: 93.73033
[RESULT]: Val. Epoch: 19, summary_loss: 0.58537, final_score: 0.21628, time: 26.46318

2021-06-25T13:47:11.150406
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 20, summary_loss: 0.42575, final_score: 0.17658, time: 94.66544
[RESULT]: Val. Epoch: 20, summary_loss: 1.28832, final_score: 0.24126, time: 27.71574

2021-06-25T13:49:13.702373
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 21, summary_loss: 0.41649, final_score: 0.16433, time: 94.33719
[RESULT]: Val. Epoch: 21, summary_loss: 0.70959, final_score: 0.25624, time: 27.43609

2021-06-25T13:51:15.656601
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 22, summary_loss: 0.40665, final_score: 0.15696, time: 94.98939
[RESULT]: Val. Epoch: 22, summary_loss: 0.56413, final_score: 0.21878, time: 26.70102

2021-06-25T13:53:17.513313
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 23, summary_loss: 0.39976, final_score: 0.15671, time: 94.81677
[RESULT]: Val. Epoch: 23, summary_loss: 0.59093, final_score: 0.20330, time: 27.49440

2021-06-25T13:55:19.983729
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 24, summary_loss: 0.40271, final_score: 0.15959, time: 93.71080
[RESULT]: Val. Epoch: 24, summary_loss: 0.81983, final_score: 0.24975, time: 26.67982

2021-06-25T13:57:20.567527
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 25, summary_loss: 0.36716, final_score: 0.13184, time: 96.86995
[RESULT]: Val. Epoch: 25, summary_loss: 0.64819, final_score: 0.19630, time: 26.81084

2021-06-25T13:59:24.433661
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 26, summary_loss: 0.35999, final_score: 0.12559, time: 93.62694
[RESULT]: Val. Epoch: 26, summary_loss: 0.66466, final_score: 0.22228, time: 26.81236

2021-06-25T14:01:25.050518
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 27, summary_loss: 0.32818, final_score: 0.11072, time: 93.64050
[RESULT]: Val. Epoch: 27, summary_loss: 0.60287, final_score: 0.20330, time: 26.51553

2021-06-25T14:03:25.370919
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 28, summary_loss: 0.32672, final_score: 0.10772, time: 90.89624
[RESULT]: Val. Epoch: 28, summary_loss: 0.53478, final_score: 0.17782, time: 26.44898

2021-06-25T14:05:23.070132
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 29, summary_loss: 0.31713, final_score: 0.10085, time: 93.66560
[RESULT]: Val. Epoch: 29, summary_loss: 0.52616, final_score: 0.18332, time: 26.49338
Fitter prepared. Device is cuda:0

2021-06-26T08:54:45.598848
LR: 0.000125
Emb_rate: 1.2
[RESULT]: Train. Epoch: 30, summary_loss: 0.37217, final_score: 0.13984, time: 97.65524
[RESULT]: Val. Epoch: 30, summary_loss: 0.48564, final_score: 0.18931, time: 30.53014

2021-06-26T08:56:54.195073
LR: 0.000125
Emb_rate: 1.08
[RESULT]: Train. Epoch: 31, summary_loss: 0.37016, final_score: 0.13634, time: 92.90333
[RESULT]: Val. Epoch: 31, summary_loss: 0.47642, final_score: 0.18581, time: 26.62365

2021-06-26T08:58:54.095059
LR: 0.000125
Emb_rate: 1.08
[RESULT]: Train. Epoch: 32, summary_loss: 0.36466, final_score: 0.13472, time: 93.69856
[RESULT]: Val. Epoch: 32, summary_loss: 0.46373, final_score: 0.18332, time: 26.70628

2021-06-26T09:00:54.849586
LR: 0.000125
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 33, summary_loss: 0.36137, final_score: 0.13309, time: 94.43152
[RESULT]: Val. Epoch: 33, summary_loss: 0.47023, final_score: 0.19381, time: 26.78824

2021-06-26T09:02:56.240702
LR: 0.000125
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 34, summary_loss: 0.35910, final_score: 0.13234, time: 94.18977
[RESULT]: Val. Epoch: 34, summary_loss: 0.51978, final_score: 0.18581, time: 27.15020

2021-06-26T09:04:57.774932
LR: 0.000125
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 35, summary_loss: 0.35895, final_score: 0.13272, time: 93.77634
[RESULT]: Val. Epoch: 35, summary_loss: 0.54709, final_score: 0.20929, time: 26.41698

2021-06-26T09:06:58.137808
LR: 0.000125
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 36, summary_loss: 0.35630, final_score: 0.13072, time: 93.70343
[RESULT]: Val. Epoch: 36, summary_loss: 0.52367, final_score: 0.17882, time: 26.24896

2021-06-26T09:08:58.270644
LR: 0.000125
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 37, summary_loss: 0.34924, final_score: 0.12584, time: 94.04265
[RESULT]: Val. Epoch: 37, summary_loss: 0.49694, final_score: 0.18382, time: 26.55239

2021-06-26T09:10:59.036067
LR: 0.000125
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 38, summary_loss: 0.35134, final_score: 0.12647, time: 93.65509
[RESULT]: Val. Epoch: 38, summary_loss: 0.50441, final_score: 0.18781, time: 27.00727

2021-06-26T09:12:59.884220
LR: 0.000125
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 39, summary_loss: 0.34407, final_score: 0.12359, time: 93.45072
[RESULT]: Val. Epoch: 39, summary_loss: 0.48896, final_score: 0.17932, time: 26.24423

2021-06-26T09:14:59.753095
LR: 0.000125
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 40, summary_loss: 0.34587, final_score: 0.12559, time: 92.97832
[RESULT]: Val. Epoch: 40, summary_loss: 0.67072, final_score: 0.22677, time: 26.46089

2021-06-26T09:16:59.374065
LR: 0.000125
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 41, summary_loss: 0.33807, final_score: 0.11560, time: 94.66201
[RESULT]: Val. Epoch: 41, summary_loss: 0.49552, final_score: 0.18082, time: 26.82114

2021-06-26T09:19:01.030157
LR: 0.000125
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 42, summary_loss: 0.33965, final_score: 0.11822, time: 94.31378
[RESULT]: Val. Epoch: 42, summary_loss: 0.51181, final_score: 0.19331, time: 27.08081

2021-06-26T09:21:02.591299
LR: 0.000125
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 43, summary_loss: 0.33733, final_score: 0.11635, time: 93.30225
[RESULT]: Val. Epoch: 43, summary_loss: 0.82327, final_score: 0.20330, time: 26.72871

2021-06-26T09:23:02.788337
LR: 0.000125
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 44, summary_loss: 0.33416, final_score: 0.11535, time: 93.97211
[RESULT]: Val. Epoch: 44, summary_loss: 0.50709, final_score: 0.18482, time: 28.19489

2021-06-26T09:25:05.132031
LR: 0.000125
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 45, summary_loss: 0.33264, final_score: 0.11385, time: 96.39408
[RESULT]: Val. Epoch: 45, summary_loss: 0.51801, final_score: 0.17732, time: 26.77805

2021-06-26T09:27:08.545446
LR: 0.000125
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 46, summary_loss: 0.32645, final_score: 0.11022, time: 95.46185
[RESULT]: Val. Epoch: 46, summary_loss: 0.46844, final_score: 0.17582, time: 28.76571

2021-06-26T09:29:12.959539
LR: 0.000125
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 47, summary_loss: 0.32121, final_score: 0.10797, time: 94.90948
[RESULT]: Val. Epoch: 47, summary_loss: 0.48355, final_score: 0.18182, time: 27.08986

2021-06-26T09:31:15.141456
LR: 0.000125
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 48, summary_loss: 0.32086, final_score: 0.10147, time: 97.38224
[RESULT]: Val. Epoch: 48, summary_loss: 0.63337, final_score: 0.17333, time: 26.97280

2021-06-26T09:33:19.686720
LR: 0.000125
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 49, summary_loss: 0.32008, final_score: 0.10422, time: 93.71527
[RESULT]: Val. Epoch: 49, summary_loss: 0.55483, final_score: 0.19780, time: 26.77048

2021-06-26T09:35:20.347258
LR: 0.000125
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 50, summary_loss: 0.31663, final_score: 0.10485, time: 96.29278
[RESULT]: Val. Epoch: 50, summary_loss: 0.52606, final_score: 0.18681, time: 26.98395

2021-06-26T09:37:23.811552
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 51, summary_loss: 0.31386, final_score: 0.09735, time: 95.99069
[RESULT]: Val. Epoch: 51, summary_loss: 0.75812, final_score: 0.19830, time: 27.13539

2021-06-26T09:39:27.112067
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 52, summary_loss: 0.30657, final_score: 0.09560, time: 96.74377
[RESULT]: Val. Epoch: 52, summary_loss: 0.46112, final_score: 0.17283, time: 27.25492

2021-06-26T09:41:31.503649
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 53, summary_loss: 0.29958, final_score: 0.09173, time: 96.58630
[RESULT]: Val. Epoch: 53, summary_loss: 0.48113, final_score: 0.17283, time: 26.76529

2021-06-26T09:43:35.053783
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 54, summary_loss: 0.29665, final_score: 0.08935, time: 96.72644
[RESULT]: Val. Epoch: 54, summary_loss: 0.59282, final_score: 0.17832, time: 27.33035

2021-06-26T09:45:39.301578
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 55, summary_loss: 0.29593, final_score: 0.08773, time: 93.17107
[RESULT]: Val. Epoch: 55, summary_loss: 0.46631, final_score: 0.17133, time: 26.52681

2021-06-26T09:47:39.183202
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 56, summary_loss: 0.28850, final_score: 0.08623, time: 97.48012
[RESULT]: Val. Epoch: 56, summary_loss: 0.46882, final_score: 0.17333, time: 26.95663

2021-06-26T09:49:43.803659
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 57, summary_loss: 0.28930, final_score: 0.08335, time: 95.93409
[RESULT]: Val. Epoch: 57, summary_loss: 0.46886, final_score: 0.16933, time: 27.33763

2021-06-26T09:51:47.250412
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 58, summary_loss: 0.28975, final_score: 0.08460, time: 97.35658
[RESULT]: Val. Epoch: 58, summary_loss: 0.47102, final_score: 0.17083, time: 27.17674

2021-06-26T09:53:51.950035
LR: 7.8125e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 59, summary_loss: 0.28465, final_score: 0.07861, time: 96.19712
[RESULT]: Val. Epoch: 59, summary_loss: 0.46858, final_score: 0.17033, time: 26.87433
