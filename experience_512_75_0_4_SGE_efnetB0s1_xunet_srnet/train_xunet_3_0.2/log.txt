Fitter prepared. Device is cuda:0

2021-04-26T08:50:31.331371
LR: 0.001
Emb_rate: 1.2
[RESULT]: Train. Epoch: 0, summary_loss: 0.52695, final_score: 0.26531, time: 667.68201
[RESULT]: Val. Epoch: 0, summary_loss: 1.64035, final_score: 0.49950, time: 206.97561

2021-04-26T09:05:06.487863
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 1, summary_loss: 0.27181, final_score: 0.07398, time: 677.81942
[RESULT]: Val. Epoch: 1, summary_loss: 2.33448, final_score: 0.50000, time: 207.03914

2021-04-26T09:19:51.527345
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 2, summary_loss: 0.21958, final_score: 0.04324, time: 676.11247
Fitter prepared. Device is cuda:0

2021-04-26T09:37:40.105987
LR: 0.001
Emb_rate: 1.2
[RESULT]: Train. Epoch: 0, summary_loss: 0.51689, final_score: 0.24631, time: 658.93309
[RESULT]: Val. Epoch: 0, summary_loss: 1.66347, final_score: 0.50000, time: 205.03070

2021-04-26T09:52:04.499356
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 1, summary_loss: 0.23921, final_score: 0.05299, time: 684.10522
[RESULT]: Val. Epoch: 1, summary_loss: 1.64712, final_score: 0.49900, time: 203.04202

2021-04-26T10:06:52.072355
LR: 0.001
Emb_rate: 1.08
[RESULT]: Train. Epoch: 2, summary_loss: 0.20776, final_score: 0.03537, time: 658.28485
[RESULT]: Val. Epoch: 2, summary_loss: 2.55084, final_score: 0.50000, time: 206.71596

2021-04-26T10:21:17.257603
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 3, summary_loss: 0.21672, final_score: 0.04036, time: 665.44691
[RESULT]: Val. Epoch: 3, summary_loss: 2.77565, final_score: 0.50000, time: 204.29789

2021-04-26T10:35:47.174164
LR: 0.001
Emb_rate: 0.9720000000000001
[RESULT]: Train. Epoch: 4, summary_loss: 0.19242, final_score: 0.02812, time: 665.77943
[RESULT]: Val. Epoch: 4, summary_loss: 1.78202, final_score: 0.50000, time: 203.61472

2021-04-26T10:50:16.735081
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 5, summary_loss: 0.25075, final_score: 0.06311, time: 658.56977
[RESULT]: Val. Epoch: 5, summary_loss: 2.35209, final_score: 0.49950, time: 202.89468

2021-04-26T11:04:38.373293
LR: 0.001
Emb_rate: 0.8748000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.23509, final_score: 0.05186, time: 664.20813
[RESULT]: Val. Epoch: 6, summary_loss: 1.93404, final_score: 0.50000, time: 203.65672

2021-04-26T11:19:06.408360
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.28555, final_score: 0.08010, time: 679.33425
[RESULT]: Val. Epoch: 7, summary_loss: 2.45511, final_score: 0.49900, time: 203.94349

2021-04-26T11:33:49.868434
LR: 0.001
Emb_rate: 0.7873200000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.25204, final_score: 0.06348, time: 674.16033
[RESULT]: Val. Epoch: 8, summary_loss: 2.73924, final_score: 0.49950, time: 203.68229

2021-04-26T11:48:27.894652
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.32024, final_score: 0.10397, time: 670.29461
[RESULT]: Val. Epoch: 9, summary_loss: 5.95728, final_score: 0.49950, time: 202.89049

2021-04-26T12:03:01.246385
LR: 0.001
Emb_rate: 0.7085880000000001
[RESULT]: Train. Epoch: 10, summary_loss: 0.32922, final_score: 0.10947, time: 665.92099
[RESULT]: Val. Epoch: 10, summary_loss: 3.49516, final_score: 0.50000, time: 203.92171

2021-04-26T12:17:31.269933
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 11, summary_loss: 0.38777, final_score: 0.14471, time: 675.08509
[RESULT]: Val. Epoch: 11, summary_loss: 1.49856, final_score: 0.49950, time: 202.95561

2021-04-26T12:32:09.665454
LR: 0.001
Emb_rate: 0.6377292000000001
[RESULT]: Train. Epoch: 12, summary_loss: 0.37489, final_score: 0.13672, time: 680.21116
[RESULT]: Val. Epoch: 12, summary_loss: 1.58749, final_score: 0.49950, time: 203.70530

2021-04-26T12:46:53.749724
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 13, summary_loss: 0.44023, final_score: 0.17946, time: 669.43802
[RESULT]: Val. Epoch: 13, summary_loss: 1.64554, final_score: 0.49800, time: 203.93827

2021-04-26T13:01:27.299002
LR: 0.001
Emb_rate: 0.5739562800000001
[RESULT]: Train. Epoch: 14, summary_loss: 0.42191, final_score: 0.17096, time: 676.64621
[RESULT]: Val. Epoch: 14, summary_loss: 2.24370, final_score: 0.49151, time: 203.72827

2021-04-26T13:16:07.832932
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 15, summary_loss: 0.47162, final_score: 0.20582, time: 685.08782
[RESULT]: Val. Epoch: 15, summary_loss: 1.38231, final_score: 0.49451, time: 203.29741

2021-04-26T13:30:56.678055
LR: 0.001
Emb_rate: 0.5165606520000001
[RESULT]: Train. Epoch: 16, summary_loss: 0.46126, final_score: 0.19583, time: 670.03288
[RESULT]: Val. Epoch: 16, summary_loss: 1.59151, final_score: 0.45804, time: 204.22642

2021-04-26T13:45:31.105349
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 17, summary_loss: 0.49557, final_score: 0.22494, time: 688.80952
[RESULT]: Val. Epoch: 17, summary_loss: 1.17195, final_score: 0.43856, time: 205.59447

2021-04-26T14:00:25.896853
LR: 0.001
Emb_rate: 0.46490458680000013
[RESULT]: Train. Epoch: 18, summary_loss: 0.48440, final_score: 0.20945, time: 684.51551
[RESULT]: Val. Epoch: 18, summary_loss: 1.04760, final_score: 0.36813, time: 210.35243

2021-04-26T14:15:21.117052
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 19, summary_loss: 0.50578, final_score: 0.22557, time: 694.74043
[RESULT]: Val. Epoch: 19, summary_loss: 0.58701, final_score: 0.28671, time: 210.20594

2021-04-26T14:30:26.390917
LR: 0.001
Emb_rate: 0.4184141281200001
[RESULT]: Train. Epoch: 20, summary_loss: 0.49511, final_score: 0.21970, time: 691.52068
[RESULT]: Val. Epoch: 20, summary_loss: 0.65544, final_score: 0.30769, time: 204.46743

2021-04-26T14:45:22.554878
LR: 0.001
Emb_rate: 0.3765727153080001
[RESULT]: Train. Epoch: 21, summary_loss: 0.49293, final_score: 0.22007, time: 680.67821
[RESULT]: Val. Epoch: 21, summary_loss: 1.16800, final_score: 0.32418, time: 203.67064

2021-04-26T15:00:07.095406
LR: 0.001
Emb_rate: 0.3765727153080001
[RESULT]: Train. Epoch: 22, summary_loss: 0.48268, final_score: 0.20957, time: 712.32906
[RESULT]: Val. Epoch: 22, summary_loss: 0.52160, final_score: 0.22428, time: 203.45800

2021-04-26T15:15:23.204233
LR: 0.001
Emb_rate: 0.3389154437772001
[RESULT]: Train. Epoch: 23, summary_loss: 0.47919, final_score: 0.21357, time: 690.43168
[RESULT]: Val. Epoch: 23, summary_loss: 0.50964, final_score: 0.22028, time: 208.41465

2021-04-26T15:30:22.386683
LR: 0.001
Emb_rate: 0.3389154437772001
[RESULT]: Train. Epoch: 24, summary_loss: 0.46417, final_score: 0.19858, time: 696.33556
[RESULT]: Val. Epoch: 24, summary_loss: 0.82923, final_score: 0.22777, time: 209.60743

2021-04-26T15:45:28.499047
LR: 0.001
Emb_rate: 0.3050238993994801
[RESULT]: Train. Epoch: 25, summary_loss: 0.45298, final_score: 0.19195, time: 674.43471
[RESULT]: Val. Epoch: 25, summary_loss: 1.12080, final_score: 0.24775, time: 204.99402

2021-04-26T16:00:08.104570
LR: 0.001
Emb_rate: 0.3050238993994801
[RESULT]: Train. Epoch: 26, summary_loss: 0.43923, final_score: 0.17833, time: 685.52714
[RESULT]: Val. Epoch: 26, summary_loss: 0.60589, final_score: 0.20779, time: 209.55517

2021-04-26T16:15:03.372350
LR: 0.001
Emb_rate: 0.2745215094595321
[RESULT]: Train. Epoch: 27, summary_loss: 0.42671, final_score: 0.17208, time: 675.03748
[RESULT]: Val. Epoch: 27, summary_loss: 0.55724, final_score: 0.22428, time: 203.73440

2021-04-26T16:29:42.420345
LR: 0.001
Emb_rate: 0.2745215094595321
[RESULT]: Train. Epoch: 28, summary_loss: 0.42151, final_score: 0.16596, time: 681.38593
[RESULT]: Val. Epoch: 28, summary_loss: 0.79479, final_score: 0.24575, time: 203.49926

2021-04-26T16:44:27.463874
LR: 0.001
Emb_rate: 0.24706935851357886
[RESULT]: Train. Epoch: 29, summary_loss: 0.40995, final_score: 0.16146, time: 690.79693
[RESULT]: Val. Epoch: 29, summary_loss: 2.73762, final_score: 0.36064, time: 203.95476
Fitter prepared. Device is cuda:0
