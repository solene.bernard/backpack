Fitter prepared. Device is cuda:0

2021-04-19T06:46:36.752895
LR: 0.0005
Emb_rate: 1.0
[RESULT]: Train. Epoch: 1, summary_loss: 0.44328, final_score: 0.20295, time: 773.30344
[RESULT]: Val. Epoch: 1, summary_loss: 1.43230, final_score: 0.42557, time: 179.00703

2021-04-19T07:02:29.456993
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 2, summary_loss: 0.31864, final_score: 0.10060, time: 758.38839
[RESULT]: Val. Epoch: 2, summary_loss: 1.67968, final_score: 0.41958, time: 197.06500

2021-04-19T07:18:25.109909
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 3, summary_loss: 0.26666, final_score: 0.07436, time: 749.78796
[RESULT]: Val. Epoch: 3, summary_loss: 1.64414, final_score: 0.40609, time: 187.74909

2021-04-19T07:34:02.875541
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 4, summary_loss: 0.29336, final_score: 0.08973, time: 741.09868
[RESULT]: Val. Epoch: 4, summary_loss: 1.42515, final_score: 0.40959, time: 176.15598

2021-04-19T07:49:20.537097
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 5, summary_loss: 0.26527, final_score: 0.07973, time: 742.46101
[RESULT]: Val. Epoch: 5, summary_loss: 2.10666, final_score: 0.41508, time: 187.95412

2021-04-19T08:04:51.138152
LR: 0.0005
Emb_rate: 0.7290000000000001
Fitter prepared. Device is cuda:0

2021-04-19T18:41:08.642639
LR: 0.0005
Emb_rate: 1.0
[RESULT]: Train. Epoch: 1, summary_loss: 0.47737, final_score: 0.23207, time: 786.14268
[RESULT]: Val. Epoch: 1, summary_loss: 1.12468, final_score: 0.43906, time: 172.54792

2021-04-19T18:57:07.731678
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 2, summary_loss: 0.33665, final_score: 0.11560, time: 729.55439
[RESULT]: Val. Epoch: 2, summary_loss: 1.16209, final_score: 0.42607, time: 169.69648

2021-04-19T19:12:07.183369
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 3, summary_loss: 0.28452, final_score: 0.08735, time: 734.54312
[RESULT]: Val. Epoch: 3, summary_loss: 1.31979, final_score: 0.41508, time: 169.41596

2021-04-19T19:27:11.387317
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 4, summary_loss: 0.30609, final_score: 0.09810, time: 751.90603
[RESULT]: Val. Epoch: 4, summary_loss: 1.81883, final_score: 0.41459, time: 169.69129

2021-04-19T19:42:33.176783
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 5, summary_loss: 0.28051, final_score: 0.08260, time: 730.95309
[RESULT]: Val. Epoch: 5, summary_loss: 1.57500, final_score: 0.39810, time: 167.99674

2021-04-19T19:57:32.309293
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.31937, final_score: 0.10722, time: 735.50927
[RESULT]: Val. Epoch: 6, summary_loss: 1.34070, final_score: 0.40110, time: 167.91637

2021-04-19T20:12:35.942021
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.30655, final_score: 0.10222, time: 738.08575
[RESULT]: Val. Epoch: 7, summary_loss: 1.77714, final_score: 0.41159, time: 166.89743

2021-04-19T20:27:41.126229
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.37113, final_score: 0.14059, time: 747.11871
[RESULT]: Val. Epoch: 8, summary_loss: 1.54774, final_score: 0.38861, time: 166.70131

2021-04-19T20:42:55.167645
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.35099, final_score: 0.12884, time: 734.29406
[RESULT]: Val. Epoch: 9, summary_loss: 1.18642, final_score: 0.39610, time: 165.27648

2021-04-19T20:57:54.921690
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 10, summary_loss: 0.40693, final_score: 0.16546, time: 751.38886
[RESULT]: Val. Epoch: 10, summary_loss: 0.80724, final_score: 0.37063, time: 167.21007

2021-04-19T21:13:13.924623
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 11, summary_loss: 0.39955, final_score: 0.16246, time: 742.89235
[RESULT]: Val. Epoch: 11, summary_loss: 0.95691, final_score: 0.36663, time: 165.42960

2021-04-19T21:28:22.441901
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 12, summary_loss: 0.46854, final_score: 0.21545, time: 733.80649
[RESULT]: Val. Epoch: 12, summary_loss: 0.81337, final_score: 0.35365, time: 165.93953

2021-04-19T21:43:22.417313
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 13, summary_loss: 0.44726, final_score: 0.19870, time: 735.38461
[RESULT]: Val. Epoch: 13, summary_loss: 0.66380, final_score: 0.35664, time: 164.95873

2021-04-19T21:58:23.134346
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 14, summary_loss: 0.51624, final_score: 0.24919, time: 735.93973
[RESULT]: Val. Epoch: 14, summary_loss: 0.85946, final_score: 0.34665, time: 163.64824

2021-04-19T22:13:22.897019
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 15, summary_loss: 0.49864, final_score: 0.23732, time: 729.73192
[RESULT]: Val. Epoch: 15, summary_loss: 0.85448, final_score: 0.32667, time: 165.28364

2021-04-19T22:28:18.090176
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 16, summary_loss: 0.55939, final_score: 0.29018, time: 743.37867
[RESULT]: Val. Epoch: 16, summary_loss: 0.71481, final_score: 0.35015, time: 165.98252

2021-04-19T22:43:27.628625
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 17, summary_loss: 0.54665, final_score: 0.28318, time: 742.92577
[RESULT]: Val. Epoch: 17, summary_loss: 0.62211, final_score: 0.32817, time: 165.02472

2021-04-19T22:58:35.938406
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 18, summary_loss: 0.58210, final_score: 0.31717, time: 735.08998
[RESULT]: Val. Epoch: 18, summary_loss: 0.76191, final_score: 0.32767, time: 164.51817

2021-04-19T23:13:35.741652
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 19, summary_loss: 0.57830, final_score: 0.31092, time: 741.18757
[RESULT]: Val. Epoch: 19, summary_loss: 0.59845, final_score: 0.31269, time: 163.44419

2021-04-19T23:28:40.769838
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 20, summary_loss: 0.57512, final_score: 0.30892, time: 731.55611
[RESULT]: Val. Epoch: 20, summary_loss: 0.55676, final_score: 0.27572, time: 164.86913

2021-04-19T23:43:37.540769
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 21, summary_loss: 0.56312, final_score: 0.29980, time: 741.89931
[RESULT]: Val. Epoch: 21, summary_loss: 0.62585, final_score: 0.32368, time: 165.12319

2021-04-19T23:58:44.730220
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 22, summary_loss: 0.56126, final_score: 0.29580, time: 724.56501
[RESULT]: Val. Epoch: 22, summary_loss: 0.65063, final_score: 0.30270, time: 165.76097

2021-04-20T00:13:35.225637
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 23, summary_loss: 0.51954, final_score: 0.26293, time: 736.41555
[RESULT]: Val. Epoch: 23, summary_loss: 0.56405, final_score: 0.26573, time: 166.80786

2021-04-20T00:28:38.634538
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 24, summary_loss: 0.50703, final_score: 0.25131, time: 741.08236
[RESULT]: Val. Epoch: 24, summary_loss: 0.53809, final_score: 0.24775, time: 164.93450

2021-04-20T00:43:44.979860
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 25, summary_loss: 0.50095, final_score: 0.24619, time: 724.57404
[RESULT]: Val. Epoch: 25, summary_loss: 0.66300, final_score: 0.26274, time: 166.55208

2021-04-20T00:58:36.276382
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 26, summary_loss: 0.49023, final_score: 0.23519, time: 725.93102
[RESULT]: Val. Epoch: 26, summary_loss: 0.51967, final_score: 0.23676, time: 165.96945

2021-04-20T01:13:28.482509
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 27, summary_loss: 0.48155, final_score: 0.22944, time: 727.35663
[RESULT]: Val. Epoch: 27, summary_loss: 0.53949, final_score: 0.25175, time: 163.98416

2021-04-20T01:28:19.991168
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 28, summary_loss: 0.47842, final_score: 0.23157, time: 742.50146
[RESULT]: Val. Epoch: 28, summary_loss: 0.49954, final_score: 0.22577, time: 164.53107

2021-04-20T01:43:27.356441
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 29, summary_loss: 0.47140, final_score: 0.22269, time: 722.34062
[RESULT]: Val. Epoch: 29, summary_loss: 0.48160, final_score: 0.21978, time: 166.47781

2021-04-20T01:58:16.498841
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 30, summary_loss: 0.46083, final_score: 0.21495, time: 742.54762
[RESULT]: Val. Epoch: 30, summary_loss: 0.51615, final_score: 0.23926, time: 164.36111
Fitter prepared. Device is cuda:0

2021-04-28T09:36:32.682731
LR: 0.00025
Emb_rate: 0.2
[RESULT]: Train. Epoch: 31, summary_loss: 0.65854, final_score: 0.39628, time: 785.03612
[RESULT]: Val. Epoch: 31, summary_loss: 0.70610, final_score: 0.40160, time: 166.26262

2021-04-28T09:52:24.228144
LR: 0.00025
Emb_rate: 0.18000000000000002
