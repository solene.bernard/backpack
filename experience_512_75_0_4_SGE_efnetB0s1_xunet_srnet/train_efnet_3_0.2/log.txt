Fitter prepared. Device is cuda:0

2021-04-26T09:31:40.859434
LR: 0.0005
Emb_rate: 1.0
[RESULT]: Train. Epoch: 1, summary_loss: 0.46403, final_score: 0.22082, time: 759.89895
[RESULT]: Val. Epoch: 1, summary_loss: 1.61314, final_score: 0.49800, time: 181.90306

2021-04-26T09:47:22.981534
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 2, summary_loss: 0.31024, final_score: 0.09885, time: 775.72641
[RESULT]: Val. Epoch: 2, summary_loss: 1.65319, final_score: 0.49950, time: 232.63727

2021-04-26T10:04:11.538851
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 3, summary_loss: 0.27014, final_score: 0.07411, time: 813.44190
[RESULT]: Val. Epoch: 3, summary_loss: 2.84616, final_score: 0.49850, time: 187.82921

2021-04-26T10:20:53.022280
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 4, summary_loss: 0.28595, final_score: 0.08735, time: 768.53532
[RESULT]: Val. Epoch: 4, summary_loss: 1.85338, final_score: 0.49950, time: 189.88578

2021-04-26T10:36:51.640278
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 5, summary_loss: 0.27080, final_score: 0.08098, time: 773.28084
[RESULT]: Val. Epoch: 5, summary_loss: 2.28717, final_score: 0.49850, time: 188.78612

2021-04-26T10:52:53.859579
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.31102, final_score: 0.10360, time: 779.95394
[RESULT]: Val. Epoch: 6, summary_loss: 1.68300, final_score: 0.49950, time: 188.43408

2021-04-26T11:09:02.416547
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.29646, final_score: 0.09585, time: 770.78542
[RESULT]: Val. Epoch: 7, summary_loss: 3.77968, final_score: 0.49950, time: 188.12409

2021-04-26T11:25:01.487209
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.35786, final_score: 0.13084, time: 783.41525
[RESULT]: Val. Epoch: 8, summary_loss: 1.98318, final_score: 0.49900, time: 191.41373

2021-04-26T11:41:16.485458
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.33607, final_score: 0.11847, time: 776.85529
[RESULT]: Val. Epoch: 9, summary_loss: 1.32060, final_score: 0.49900, time: 190.68007

2021-04-26T11:57:24.363894
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 10, summary_loss: 0.40223, final_score: 0.16158, time: 804.46131
[RESULT]: Val. Epoch: 10, summary_loss: 1.63675, final_score: 0.49850, time: 186.08549

2021-04-26T12:13:55.073389
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 11, summary_loss: 0.39079, final_score: 0.15459, time: 782.17791
[RESULT]: Val. Epoch: 11, summary_loss: 2.20134, final_score: 0.49900, time: 192.90415

2021-04-26T12:30:10.303854
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 12, summary_loss: 0.45654, final_score: 0.20295, time: 793.44857
[RESULT]: Val. Epoch: 12, summary_loss: 1.46025, final_score: 0.49850, time: 189.91426

2021-04-26T12:46:33.840658
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 13, summary_loss: 0.44461, final_score: 0.19520, time: 792.46565
[RESULT]: Val. Epoch: 13, summary_loss: 1.77451, final_score: 0.49950, time: 190.92163

2021-04-26T13:02:57.425121
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 14, summary_loss: 0.51352, final_score: 0.24894, time: 783.14103
[RESULT]: Val. Epoch: 14, summary_loss: 1.39658, final_score: 0.49800, time: 187.30109

2021-04-26T13:19:08.019154
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 15, summary_loss: 0.50105, final_score: 0.23694, time: 778.72723
[RESULT]: Val. Epoch: 15, summary_loss: 1.44828, final_score: 0.49700, time: 181.42484

2021-04-26T13:35:08.338803
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 16, summary_loss: 0.55435, final_score: 0.28618, time: 784.74546
[RESULT]: Val. Epoch: 16, summary_loss: 1.16943, final_score: 0.49700, time: 190.71833

2021-04-26T13:51:24.209370
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 17, summary_loss: 0.54921, final_score: 0.28430, time: 807.37053
[RESULT]: Val. Epoch: 17, summary_loss: 1.21956, final_score: 0.49750, time: 191.50704

2021-04-26T14:08:03.240725
LR: 0.0005
Emb_rate: 0.38742048900000015
[RESULT]: Train. Epoch: 18, summary_loss: 0.59846, final_score: 0.33654, time: 809.95386
[RESULT]: Val. Epoch: 18, summary_loss: 0.92157, final_score: 0.49151, time: 190.26253

2021-04-26T14:24:43.829713
LR: 0.0005
Emb_rate: 0.38742048900000015
[RESULT]: Train. Epoch: 19, summary_loss: 0.58988, final_score: 0.32154, time: 802.72294
[RESULT]: Val. Epoch: 19, summary_loss: 1.13600, final_score: 0.49151, time: 185.51690

2021-04-26T14:41:12.282409
LR: 0.0005
Emb_rate: 0.34867844010000015
[RESULT]: Train. Epoch: 20, summary_loss: 0.62349, final_score: 0.36266, time: 838.14583
[RESULT]: Val. Epoch: 20, summary_loss: 1.25946, final_score: 0.48751, time: 190.61583

2021-04-26T14:58:21.240351
LR: 0.0005
Emb_rate: 0.34867844010000015
[RESULT]: Train. Epoch: 21, summary_loss: 0.61602, final_score: 0.34754, time: 802.37367
[RESULT]: Val. Epoch: 21, summary_loss: 0.95338, final_score: 0.47852, time: 185.29157

2021-04-26T15:14:49.116964
LR: 0.0005
Emb_rate: 0.31381059609000017
[RESULT]: Train. Epoch: 22, summary_loss: 0.64034, final_score: 0.37391, time: 798.39661
[RESULT]: Val. Epoch: 22, summary_loss: 0.73949, final_score: 0.45455, time: 219.19994

2021-04-26T15:31:47.091854
LR: 0.0005
Emb_rate: 0.31381059609000017
[RESULT]: Train. Epoch: 23, summary_loss: 0.63362, final_score: 0.36591, time: 789.67024
[RESULT]: Val. Epoch: 23, summary_loss: 0.75492, final_score: 0.43457, time: 195.85755

2021-04-26T15:48:12.772739
LR: 0.0005
Emb_rate: 0.28242953648100017
[RESULT]: Train. Epoch: 24, summary_loss: 0.61238, final_score: 0.33754, time: 796.00332
[RESULT]: Val. Epoch: 24, summary_loss: 0.56623, final_score: 0.26474, time: 187.96069

2021-04-26T16:04:37.060800
LR: 0.0005
Emb_rate: 0.28242953648100017
[RESULT]: Train. Epoch: 25, summary_loss: 0.53632, final_score: 0.26368, time: 802.97043
[RESULT]: Val. Epoch: 25, summary_loss: 1.10342, final_score: 0.24126, time: 171.64869

2021-04-26T16:20:51.872425
LR: 0.0005
Emb_rate: 0.25418658283290013
[RESULT]: Train. Epoch: 26, summary_loss: 0.48526, final_score: 0.21795, time: 794.00560
[RESULT]: Val. Epoch: 26, summary_loss: 0.62376, final_score: 0.20729, time: 189.38328

2021-04-26T16:37:16.329785
LR: 0.0005
Emb_rate: 0.25418658283290013
[RESULT]: Train. Epoch: 27, summary_loss: 0.45736, final_score: 0.19795, time: 781.05814
[RESULT]: Val. Epoch: 27, summary_loss: 0.72108, final_score: 0.20480, time: 188.77410

2021-04-26T16:53:26.362901
LR: 0.0005
Emb_rate: 0.22876792454961012
[RESULT]: Train. Epoch: 28, summary_loss: 0.42441, final_score: 0.17771, time: 818.30639
[RESULT]: Val. Epoch: 28, summary_loss: 0.41498, final_score: 0.16683, time: 186.70602

2021-04-26T17:10:11.706348
LR: 0.0005
Emb_rate: 0.22876792454961012
[RESULT]: Train. Epoch: 29, summary_loss: 0.41186, final_score: 0.16733, time: 831.57560
[RESULT]: Val. Epoch: 29, summary_loss: 0.36380, final_score: 0.12737, time: 199.31931

2021-04-26T17:27:23.014362
LR: 0.0005
Emb_rate: 0.2058911320946491
[RESULT]: Train. Epoch: 30, summary_loss: 0.38676, final_score: 0.14884, time: 815.29753
[RESULT]: Val. Epoch: 30, summary_loss: 0.53831, final_score: 0.12737, time: 186.39940
Fitter prepared. Device is cuda:0
