Fitter prepared. Device is cuda:0

2021-06-10T19:19:47.144092
LR: 0.001
Emb_rate: 1.0
[RESULT]: Train. Epoch: 0, summary_loss: 0.68731, final_score: 0.45514, time: 677.08043
[RESULT]: Val. Epoch: 0, summary_loss: 3.27126, final_score: 0.49251, time: 188.21371

2021-06-10T19:34:12.879213
LR: 0.001
Emb_rate: 0.9
[RESULT]: Train. Epoch: 1, summary_loss: 0.50298, final_score: 0.22707, time: 682.58422
[RESULT]: Val. Epoch: 1, summary_loss: 0.96637, final_score: 0.43257, time: 185.80895

2021-06-10T19:48:42.671201
LR: 0.001
Emb_rate: 0.9
[RESULT]: Train. Epoch: 2, summary_loss: 0.40013, final_score: 0.15596, time: 675.05807
[RESULT]: Val. Epoch: 2, summary_loss: 1.52525, final_score: 0.44705, time: 186.24561

2021-06-10T20:03:04.187907
LR: 0.001
Emb_rate: 0.81
[RESULT]: Train. Epoch: 3, summary_loss: 0.43356, final_score: 0.18145, time: 682.81344
[RESULT]: Val. Epoch: 3, summary_loss: 0.85419, final_score: 0.42158, time: 187.44044

2021-06-10T20:17:34.837615
LR: 0.001
Emb_rate: 0.81
[RESULT]: Train. Epoch: 4, summary_loss: 0.41802, final_score: 0.17421, time: 685.80909
[RESULT]: Val. Epoch: 4, summary_loss: 0.86067, final_score: 0.42557, time: 193.27600

2021-06-10T20:32:14.180709
LR: 0.001
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 5, summary_loss: 0.46686, final_score: 0.21132, time: 694.69328
[RESULT]: Val. Epoch: 5, summary_loss: 0.86891, final_score: 0.41858, time: 187.88971

2021-06-10T20:46:56.947211
LR: 0.001
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.45822, final_score: 0.20857, time: 685.40362
[RESULT]: Val. Epoch: 6, summary_loss: 0.79272, final_score: 0.41808, time: 188.27934

2021-06-10T21:01:31.166274
LR: 0.001
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.50827, final_score: 0.24856, time: 690.56607
[RESULT]: Val. Epoch: 7, summary_loss: 0.73001, final_score: 0.41758, time: 190.57430

2021-06-10T21:16:12.702328
LR: 0.001
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.50718, final_score: 0.24756, time: 690.19730
[RESULT]: Val. Epoch: 8, summary_loss: 0.73617, final_score: 0.40859, time: 189.24308

2021-06-10T21:30:52.330073
LR: 0.001
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 9, summary_loss: 0.55591, final_score: 0.28830, time: 700.33402
[RESULT]: Val. Epoch: 9, summary_loss: 0.68520, final_score: 0.40909, time: 194.03127

2021-06-10T21:45:47.067990
LR: 0.001
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 10, summary_loss: 0.54580, final_score: 0.27881, time: 701.44615
[RESULT]: Val. Epoch: 10, summary_loss: 0.70717, final_score: 0.41508, time: 187.21244

2021-06-10T22:00:35.971298
LR: 0.001
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 11, summary_loss: 0.59303, final_score: 0.32542, time: 697.04667
[RESULT]: Val. Epoch: 11, summary_loss: 0.69791, final_score: 0.40809, time: 186.43429

2021-06-10T22:15:19.635009
LR: 0.001
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 12, summary_loss: 0.58670, final_score: 0.31830, time: 695.58123
[RESULT]: Val. Epoch: 12, summary_loss: 0.71739, final_score: 0.40559, time: 186.58981

2021-06-10T22:30:02.021764
LR: 0.001
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 13, summary_loss: 0.62161, final_score: 0.35954, time: 696.32514
[RESULT]: Val. Epoch: 13, summary_loss: 0.78059, final_score: 0.41259, time: 185.83294

2021-06-10T22:44:44.380907
LR: 0.001
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 14, summary_loss: 0.62060, final_score: 0.35854, time: 696.72542
[RESULT]: Val. Epoch: 14, summary_loss: 0.67023, final_score: 0.40260, time: 186.69610

2021-06-10T22:59:28.166339
LR: 0.001
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 15, summary_loss: 0.64812, final_score: 0.38703, time: 691.99194
[RESULT]: Val. Epoch: 15, summary_loss: 0.69258, final_score: 0.41059, time: 184.74545

2021-06-10T23:14:05.134744
LR: 0.001
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 16, summary_loss: 0.64894, final_score: 0.38915, time: 703.67975
[RESULT]: Val. Epoch: 16, summary_loss: 0.67335, final_score: 0.40160, time: 190.52221

2021-06-10T23:28:59.528901
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 17, summary_loss: 0.65881, final_score: 0.40602, time: 693.63919
[RESULT]: Val. Epoch: 17, summary_loss: 0.67554, final_score: 0.40559, time: 186.26223

2021-06-10T23:43:39.651070
LR: 0.001
Emb_rate: 0.4
[RESULT]: Train. Epoch: 18, summary_loss: 0.65816, final_score: 0.40740, time: 697.84232
[RESULT]: Val. Epoch: 18, summary_loss: 0.71612, final_score: 0.41758, time: 186.69843

2021-06-10T23:58:24.391056
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 19, summary_loss: 0.65421, final_score: 0.40552, time: 694.39797
[RESULT]: Val. Epoch: 19, summary_loss: 0.65954, final_score: 0.39910, time: 192.90936

2021-06-11T00:13:12.170881
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 20, summary_loss: 0.65044, final_score: 0.39928, time: 693.30720
[RESULT]: Val. Epoch: 20, summary_loss: 0.87252, final_score: 0.42258, time: 189.15027

2021-06-11T00:27:54.817642
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 21, summary_loss: 0.65252, final_score: 0.40177, time: 704.17414
[RESULT]: Val. Epoch: 21, summary_loss: 0.66115, final_score: 0.39860, time: 188.80439

2021-06-11T00:42:48.015513
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 22, summary_loss: 0.64745, final_score: 0.39215, time: 701.08243
[RESULT]: Val. Epoch: 22, summary_loss: 0.65652, final_score: 0.40010, time: 187.23476

2021-06-11T00:57:36.903341
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 23, summary_loss: 0.64858, final_score: 0.39815, time: 694.43013
[RESULT]: Val. Epoch: 23, summary_loss: 0.69695, final_score: 0.40260, time: 187.22537

2021-06-11T01:12:18.749684
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 24, summary_loss: 0.64840, final_score: 0.39403, time: 703.13372
[RESULT]: Val. Epoch: 24, summary_loss: 0.65689, final_score: 0.39510, time: 185.79853

2021-06-11T01:27:07.875209
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 25, summary_loss: 0.64005, final_score: 0.38878, time: 695.86677
[RESULT]: Val. Epoch: 25, summary_loss: 0.67700, final_score: 0.39710, time: 190.29328

2021-06-11T01:41:54.250348
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 26, summary_loss: 0.64505, final_score: 0.39690, time: 701.36822
[RESULT]: Val. Epoch: 26, summary_loss: 0.65173, final_score: 0.38511, time: 185.37624

2021-06-11T01:56:41.384912
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 27, summary_loss: 0.64348, final_score: 0.39053, time: 696.08961
[RESULT]: Val. Epoch: 27, summary_loss: 0.67011, final_score: 0.39610, time: 185.37327

2021-06-11T02:11:23.047955
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 28, summary_loss: 0.64366, final_score: 0.39278, time: 702.21345
[RESULT]: Val. Epoch: 28, summary_loss: 0.65673, final_score: 0.38362, time: 187.15324

2021-06-11T02:26:12.694778
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 29, summary_loss: 0.64200, final_score: 0.38878, time: 711.19274
[RESULT]: Val. Epoch: 29, summary_loss: 0.64960, final_score: 0.38911, time: 186.69439
