Fitter prepared. Device is cuda:0

2021-06-25T13:10:55.808293
LR: 0.0005
Emb_rate: 1.0
[RESULT]: Train. Epoch: 1, summary_loss: 0.70753, final_score: 0.49950, time: 396.39662
[RESULT]: Val. Epoch: 1, summary_loss: 0.70361, final_score: 0.49600, time: 24.41295

2021-06-25T13:17:56.973781
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 2, summary_loss: 0.70009, final_score: 0.48988, time: 388.06204
[RESULT]: Val. Epoch: 2, summary_loss: 0.69941, final_score: 0.49650, time: 24.28344

2021-06-25T13:24:49.688625
LR: 0.0005
Emb_rate: 0.9
[RESULT]: Train. Epoch: 3, summary_loss: 0.69862, final_score: 0.49738, time: 387.45537
[RESULT]: Val. Epoch: 3, summary_loss: 0.69446, final_score: 0.49650, time: 24.58200

2021-06-25T13:31:42.084325
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 4, summary_loss: 0.69832, final_score: 0.49288, time: 387.40908
[RESULT]: Val. Epoch: 4, summary_loss: 0.69558, final_score: 0.49351, time: 24.44517

2021-06-25T13:38:34.098589
LR: 0.0005
Emb_rate: 0.81
[RESULT]: Train. Epoch: 5, summary_loss: 0.69699, final_score: 0.49550, time: 387.86132
[RESULT]: Val. Epoch: 5, summary_loss: 0.69464, final_score: 0.49650, time: 24.32813

2021-06-25T13:45:26.456789
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 6, summary_loss: 0.69709, final_score: 0.49425, time: 387.51171
[RESULT]: Val. Epoch: 6, summary_loss: 0.69509, final_score: 0.49700, time: 24.78928

2021-06-25T13:52:19.015976
LR: 0.0005
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 7, summary_loss: 0.69653, final_score: 0.49763, time: 387.35743
[RESULT]: Val. Epoch: 7, summary_loss: 0.69481, final_score: 0.49650, time: 24.99536

2021-06-25T13:59:11.568233
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 8, summary_loss: 0.69682, final_score: 0.49788, time: 387.30101
[RESULT]: Val. Epoch: 8, summary_loss: 0.69525, final_score: 0.49550, time: 24.29747

2021-06-25T14:06:03.324199
LR: 0.0005
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 9, summary_loss: 0.69555, final_score: 0.48350, time: 387.81760
[RESULT]: Val. Epoch: 9, summary_loss: 0.70270, final_score: 0.49700, time: 24.28934

2021-06-25T14:12:55.585838
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 10, summary_loss: 0.69625, final_score: 0.49150, time: 387.45835
[RESULT]: Val. Epoch: 10, summary_loss: 0.69921, final_score: 0.49700, time: 25.79453

2021-06-25T14:19:49.071500
LR: 0.0005
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 11, summary_loss: 0.69622, final_score: 0.49350, time: 387.17709
[RESULT]: Val. Epoch: 11, summary_loss: 0.69508, final_score: 0.49600, time: 24.67909

2021-06-25T14:26:41.110438
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 12, summary_loss: 0.69583, final_score: 0.49575, time: 387.65794
[RESULT]: Val. Epoch: 12, summary_loss: 0.69426, final_score: 0.49600, time: 24.84380

2021-06-25T14:33:33.992465
LR: 0.0005
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 13, summary_loss: 0.69531, final_score: 0.49238, time: 386.92307
[RESULT]: Val. Epoch: 13, summary_loss: 0.69381, final_score: 0.49700, time: 25.55408

2021-06-25T14:40:26.920155
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 14, summary_loss: 0.69543, final_score: 0.49400, time: 387.24716
[RESULT]: Val. Epoch: 14, summary_loss: 0.69652, final_score: 0.49550, time: 25.72363

2021-06-25T14:47:20.080913
LR: 0.0005
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 15, summary_loss: 0.69551, final_score: 0.49650, time: 387.02115
[RESULT]: Val. Epoch: 15, summary_loss: 0.69411, final_score: 0.49451, time: 25.90948

2021-06-25T14:54:13.201760
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 16, summary_loss: 0.69524, final_score: 0.49550, time: 386.98000
[RESULT]: Val. Epoch: 16, summary_loss: 0.69357, final_score: 0.49600, time: 24.87103

2021-06-25T15:01:05.450499
LR: 0.0005
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 17, summary_loss: 0.69475, final_score: 0.48463, time: 387.57235
[RESULT]: Val. Epoch: 17, summary_loss: 0.69822, final_score: 0.49600, time: 25.95603

2021-06-25T15:07:59.188905
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 18, summary_loss: 0.69507, final_score: 0.49413, time: 386.90082
[RESULT]: Val. Epoch: 18, summary_loss: 0.69318, final_score: 0.49500, time: 25.05427

2021-06-25T15:14:51.524331
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 19, summary_loss: 0.69440, final_score: 0.49063, time: 387.00987
[RESULT]: Val. Epoch: 19, summary_loss: 0.70545, final_score: 0.49550, time: 24.85646

2021-06-25T15:21:43.599206
LR: 0.0005
Emb_rate: 0.4
[RESULT]: Train. Epoch: 20, summary_loss: 0.69574, final_score: 0.49738, time: 387.70616
[RESULT]: Val. Epoch: 20, summary_loss: 0.69743, final_score: 0.49351, time: 24.32190

2021-06-25T15:28:35.806033
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 21, summary_loss: 0.69396, final_score: 0.49838, time: 387.75330
[RESULT]: Val. Epoch: 21, summary_loss: 0.69330, final_score: 0.49151, time: 24.86023

2021-06-25T15:35:28.627798
LR: 0.00025
Emb_rate: 0.4
[RESULT]: Train. Epoch: 22, summary_loss: 0.69409, final_score: 0.49600, time: 387.44009
[RESULT]: Val. Epoch: 22, summary_loss: 0.69355, final_score: 0.49401, time: 24.29712

2021-06-25T15:42:20.561864
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 23, summary_loss: 0.69370, final_score: 0.49400, time: 387.54780
[RESULT]: Val. Epoch: 23, summary_loss: 0.69331, final_score: 0.49451, time: 24.37190

2021-06-25T15:49:12.653888
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 24, summary_loss: 0.69345, final_score: 0.49375, time: 387.35056
[RESULT]: Val. Epoch: 24, summary_loss: 0.69380, final_score: 0.49401, time: 24.34858

2021-06-25T15:56:04.515329
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 25, summary_loss: 0.69355, final_score: 0.49763, time: 387.60078
[RESULT]: Val. Epoch: 25, summary_loss: 0.69316, final_score: 0.49351, time: 24.58894

2021-06-25T16:02:57.051639
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 26, summary_loss: 0.69348, final_score: 0.49838, time: 387.60114
[RESULT]: Val. Epoch: 26, summary_loss: 0.69315, final_score: 0.49251, time: 24.44711

2021-06-25T16:09:49.451826
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 27, summary_loss: 0.69342, final_score: 0.49788, time: 387.55608
[RESULT]: Val. Epoch: 27, summary_loss: 0.69315, final_score: 0.49351, time: 24.65323

2021-06-25T16:16:41.819317
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 28, summary_loss: 0.69327, final_score: 0.49338, time: 387.61567
[RESULT]: Val. Epoch: 28, summary_loss: 0.69317, final_score: 0.49251, time: 24.42060

2021-06-25T16:23:34.058242
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 29, summary_loss: 0.69334, final_score: 0.49825, time: 387.73225
[RESULT]: Val. Epoch: 29, summary_loss: 0.69314, final_score: 0.49051, time: 24.48609

2021-06-25T16:30:26.625451
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 30, summary_loss: 0.69314, final_score: 0.49325, time: 387.36983
[RESULT]: Val. Epoch: 30, summary_loss: 0.69320, final_score: 0.49451, time: 26.21957
Fitter prepared. Device is cuda:0

2021-06-26T08:51:34.877242
LR: 0.000125
Emb_rate: 1.0
[RESULT]: Train. Epoch: 25, summary_loss: 0.32861, final_score: 0.12084, time: 393.23026
[RESULT]: Val. Epoch: 25, summary_loss: 0.77536, final_score: 0.16583, time: 24.66131

2021-06-26T08:58:33.046768
LR: 0.000125
Emb_rate: 0.9
[RESULT]: Train. Epoch: 26, summary_loss: 0.32601, final_score: 0.12172, time: 387.46520
[RESULT]: Val. Epoch: 26, summary_loss: 0.41421, final_score: 0.13536, time: 24.82988

2021-06-26T09:05:25.522874
LR: 0.000125
Emb_rate: 0.9
[RESULT]: Train. Epoch: 27, summary_loss: 0.31648, final_score: 0.11760, time: 387.10900
[RESULT]: Val. Epoch: 27, summary_loss: 0.54164, final_score: 0.14735, time: 24.71899

2021-06-26T09:12:17.540378
LR: 0.000125
Emb_rate: 0.81
[RESULT]: Train. Epoch: 28, summary_loss: 0.31816, final_score: 0.11585, time: 387.03677
[RESULT]: Val. Epoch: 28, summary_loss: 0.55721, final_score: 0.15534, time: 24.59663

2021-06-26T09:19:09.342298
LR: 0.000125
Emb_rate: 0.81
[RESULT]: Train. Epoch: 29, summary_loss: 0.31335, final_score: 0.11760, time: 386.95399
[RESULT]: Val. Epoch: 29, summary_loss: 0.80229, final_score: 0.17932, time: 24.27713

2021-06-26T09:26:00.743512
LR: 0.000125
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 30, summary_loss: 0.31026, final_score: 0.11297, time: 386.96069
[RESULT]: Val. Epoch: 30, summary_loss: 0.45101, final_score: 0.12238, time: 24.44484

2021-06-26T09:32:52.317332
LR: 0.000125
Emb_rate: 0.7290000000000001
[RESULT]: Train. Epoch: 31, summary_loss: 0.30543, final_score: 0.10772, time: 387.61148
[RESULT]: Val. Epoch: 31, summary_loss: 0.57072, final_score: 0.15235, time: 24.72772

2021-06-26T09:39:44.813624
LR: 0.000125
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 32, summary_loss: 0.30044, final_score: 0.10560, time: 387.65959
[RESULT]: Val. Epoch: 32, summary_loss: 0.69960, final_score: 0.14386, time: 24.52030

2021-06-26T09:46:37.144045
LR: 0.000125
Emb_rate: 0.6561000000000001
[RESULT]: Train. Epoch: 33, summary_loss: 0.29912, final_score: 0.10860, time: 387.30411
[RESULT]: Val. Epoch: 33, summary_loss: 0.38176, final_score: 0.13137, time: 24.65409

2021-06-26T09:53:29.274801
LR: 0.000125
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 34, summary_loss: 0.29752, final_score: 0.10522, time: 387.29272
[RESULT]: Val. Epoch: 34, summary_loss: 0.39452, final_score: 0.12537, time: 24.43143

2021-06-26T10:00:21.168802
LR: 0.000125
Emb_rate: 0.5904900000000002
[RESULT]: Train. Epoch: 35, summary_loss: 0.29284, final_score: 0.10222, time: 387.09337
[RESULT]: Val. Epoch: 35, summary_loss: 0.59254, final_score: 0.15185, time: 24.50208

2021-06-26T10:07:12.972495
LR: 0.000125
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 36, summary_loss: 0.28738, final_score: 0.09898, time: 387.15306
[RESULT]: Val. Epoch: 36, summary_loss: 0.50444, final_score: 0.13237, time: 24.65564

2021-06-26T10:14:04.953963
LR: 0.000125
Emb_rate: 0.5314410000000002
[RESULT]: Train. Epoch: 37, summary_loss: 0.28047, final_score: 0.09473, time: 387.63248
[RESULT]: Val. Epoch: 37, summary_loss: 0.57930, final_score: 0.13536, time: 24.43974

2021-06-26T10:20:57.224989
LR: 0.000125
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 38, summary_loss: 0.28527, final_score: 0.09798, time: 387.53238
[RESULT]: Val. Epoch: 38, summary_loss: 0.74173, final_score: 0.14735, time: 24.52825

2021-06-26T10:27:49.452391
LR: 0.000125
Emb_rate: 0.47829690000000014
[RESULT]: Train. Epoch: 39, summary_loss: 0.27912, final_score: 0.08898, time: 387.58322
[RESULT]: Val. Epoch: 39, summary_loss: 0.65632, final_score: 0.15934, time: 24.66971

2021-06-26T10:34:41.882648
LR: 0.000125
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 40, summary_loss: 0.27441, final_score: 0.09098, time: 387.64766
[RESULT]: Val. Epoch: 40, summary_loss: 0.54924, final_score: 0.13237, time: 24.70901

2021-06-26T10:41:34.407493
LR: 0.000125
Emb_rate: 0.43046721000000016
[RESULT]: Train. Epoch: 41, summary_loss: 0.27413, final_score: 0.08810, time: 387.86559
[RESULT]: Val. Epoch: 41, summary_loss: 0.35241, final_score: 0.13736, time: 24.51835

2021-06-26T10:48:26.983544
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 42, summary_loss: 0.27318, final_score: 0.08610, time: 386.95095
[RESULT]: Val. Epoch: 42, summary_loss: 0.41677, final_score: 0.12038, time: 24.53792

2021-06-26T10:55:18.625770
LR: 0.000125
Emb_rate: 0.4
[RESULT]: Train. Epoch: 43, summary_loss: 0.26768, final_score: 0.08585, time: 387.84921
[RESULT]: Val. Epoch: 43, summary_loss: 0.45870, final_score: 0.12937, time: 24.52682

2021-06-26T11:02:11.162315
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 44, summary_loss: 0.25181, final_score: 0.07298, time: 387.01991
[RESULT]: Val. Epoch: 44, summary_loss: 0.47015, final_score: 0.11938, time: 24.60477

2021-06-26T11:09:02.939671
LR: 6.25e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 45, summary_loss: 0.25109, final_score: 0.07436, time: 387.66273
[RESULT]: Val. Epoch: 45, summary_loss: 0.40709, final_score: 0.12537, time: 24.52839

2021-06-26T11:15:55.339143
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 46, summary_loss: 0.23999, final_score: 0.06661, time: 387.74166
[RESULT]: Val. Epoch: 46, summary_loss: 0.52285, final_score: 0.12338, time: 25.01832

2021-06-26T11:22:48.305611
LR: 3.125e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 47, summary_loss: 0.23524, final_score: 0.06498, time: 387.57106
[RESULT]: Val. Epoch: 47, summary_loss: 0.59043, final_score: 0.13536, time: 24.63105

2021-06-26T11:29:40.671954
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 48, summary_loss: 0.23568, final_score: 0.06523, time: 388.12516
[RESULT]: Val. Epoch: 48, summary_loss: 0.54253, final_score: 0.13087, time: 24.63321

2021-06-26T11:36:33.624107
LR: 1.5625e-05
Emb_rate: 0.4
[RESULT]: Train. Epoch: 49, summary_loss: 0.23871, final_score: 0.06623, time: 387.19102
[RESULT]: Val. Epoch: 49, summary_loss: 0.45426, final_score: 0.12238, time: 24.63515

2021-06-26T11:43:25.605314
LR: 7.8125e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 50, summary_loss: 0.23108, final_score: 0.06498, time: 386.74639
[RESULT]: Val. Epoch: 50, summary_loss: 0.46794, final_score: 0.12088, time: 24.38114

2021-06-26T11:50:16.924842
LR: 7.8125e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 51, summary_loss: 0.23162, final_score: 0.06436, time: 386.47189
[RESULT]: Val. Epoch: 51, summary_loss: 0.48955, final_score: 0.12288, time: 25.23351

2021-06-26T11:57:08.840197
LR: 3.90625e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 52, summary_loss: 0.22705, final_score: 0.06011, time: 387.06831
[RESULT]: Val. Epoch: 52, summary_loss: 0.53186, final_score: 0.12637, time: 24.80012

2021-06-26T12:04:00.892398
LR: 3.90625e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 53, summary_loss: 0.23023, final_score: 0.06236, time: 387.10743
[RESULT]: Val. Epoch: 53, summary_loss: 0.46651, final_score: 0.12288, time: 24.61362

2021-06-26T12:10:52.814244
LR: 1.953125e-06
Emb_rate: 0.4
[RESULT]: Train. Epoch: 54, summary_loss: 0.22779, final_score: 0.06086, time: 387.25374
[RESULT]: Val. Epoch: 54, summary_loss: 0.52609, final_score: 0.12887, time: 24.41384
