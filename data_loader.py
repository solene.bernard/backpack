import numpy as np
from glob import glob
from sklearn.model_selection import GroupKFold
import cv2
from skimage import io
import torch
from torch import nn
import os
from datetime import datetime
import time
import random
import cv2
import pandas as pd
import numpy as np
import albumentations as A
import matplotlib.pyplot as plt
from albumentations.pytorch.transforms import ToTensorV2
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SequentialSampler, RandomSampler
import sklearn
from PIL import Image

from os import path, mkdir, makedirs
from tools_stegano import compute_spatial_from_jpeg, compute_proba, HILL


# TRANSFORMS
def get_train_transforms():
    return A.Compose([
        A.HorizontalFlip(p=0.5),
        A.VerticalFlip(p=0.5),
        #A.ToGray(always_apply=True, p=1.0),
        #A.Resize(height=512, width=512, p=1.0),
        # Si le V2 n'exite pas chez toi, tu peux utiliser ToTensor également
        ToTensorV2(p=1.0),
    ], p=1.0)


def get_valid_transforms():
    return A.Compose([
        #A.Resize(height=512, width=512, p=1.0),
        #A.ToGray(always_apply=True, p=1.0),
        ToTensorV2(p=1.0),
    ], p=1.0)

# DATASET CLASS


def onehot(size, target):
    vec = torch.zeros(size, dtype=torch.float32)
    vec[target] = 1.
    return vec


def onehot(size, target):
    vec = torch.zeros(size, dtype=torch.float32)
    vec[target] = 1.
    return vec


class DatasetRetriever(Dataset):

    def __init__(self, image_names, folder_model, QF, emb_rate, image_size,
                 data_dir_cover, data_dir_stego_0, cost_dir, data_dir_prot,
                 H1_filter, L1_filter, L2_filter, indexs_db, train_on_cost_map=False,
                 labels=None,  transforms=None, pair_training=False, spatial=False):

        super().__init__()
        self.image_names = image_names
        self.indexs_db = indexs_db
        self.labels = labels
        self.transforms = transforms
        self.c_quant = np.load(folder_model + 'c_quant_'+str(QF)+'.npy')
        self.WET_COST = 10 ** 13
        self.emb_rate = emb_rate
        self.im_size = image_size
        self.cover_path = data_dir_cover
        self.data_dir_stego_0 = data_dir_stego_0
        self.cost_dir = cost_dir
        self.pair_training = pair_training
        self.data_dir_prot = data_dir_prot
        self.spatial = spatial
        self.train_on_cost_map = train_on_cost_map
        if self.spatial:
            if(H1_filter is None):
                self.H1_filter = 4 * np.array([[-0.25, 0.5, -0.25],
                                               [0.5, -1, 0.5],
                                               [-0.25, 0.5, -0.25]])
                self.L1_filter = (1.0/9.0)*np.ones((3, 3))
                self.L2_filter = (1.0/225.0)*np.ones((15, 15))
            else:
                self.H1_filter = np.load(H1_filter).reshape((3, 3))
                self.L1_filter = np.load(L1_filter).reshape((3, 3))
                self.L2_filter = np.load(L2_filter).reshape((15, 15))

    def __getitem__(self, index: int):

        # IF PAIR TRAINING : return a pair (cover, stego) image
        if self.pair_training:
            image_name = self.image_names[index]

            if(self.spatial):
                cover = np.asarray(Image.open(
                    path.join(self.cover_path, image_name[:-3]+'pgm')), dtype=np.float32)
                message_length = cover.size*self.emb_rate
            else:
                cover = np.load(path.join(self.cover_path,
                                          image_name[:-3]+'npy')).astype(np.float32)
                nz_AC = np.sum(cover != 0)-np.sum(cover[::8, ::8] != 0)
                message_length = nz_AC*self.emb_rate
            index_db = self.indexs_db[index]

            if self.train_on_cost_map:  # Load the cost map and generate new stego
                if(self.spatial):
                    rho = HILL(cover, self.H1_filter,
                               self.L1_filter, self.L2_filter)
                else:
                    try:
                        cost_dir = self.data_dir_prot + \
                            'data_adv_' + str(index_db) + '/adv_cost/'
                        rho = np.load(cost_dir+image_name[:-3]+'npy')
                    except:
                        cost_dir = self.cost_dir
                        rho = np.load(cost_dir+image_name[:-3]+'npy')
                if(rho.shape == (self.im_size, self.im_size)):
                    rho = np.reshape(rho, (1, self.im_size, self.im_size))
                    rho = np.concatenate(
                        (np.copy(rho), np.zeros_like(rho), np.copy(rho)), axis=0)
                if(self.spatial):
                    rho[0, cover <= 0] = self.WET_COST
                    rho[2, cover >= 255] = self.WET_COST
                if not self.spatial:
                    rho[0, cover < -1023] = self.WET_COST
                    rho[2, cover > 1023] = self.WET_COST

                p = compute_proba(rho, message_length)
                u = np.random.uniform(0, 1, (3, self.im_size, self.im_size))
                stego = (cover + np.argmax(-np.log(-np.log(u)) +
                                           np.log(p+1e-30), axis=0) - 1).astype(np.float32)

            else:  # Load the stego
                try:
                    dir_stego = self.data_dir_prot + \
                        'data_adv_' + str(index_db) + '/adv_final/'
                    stego = np.load(dir_stego+image_name[:-3]+'npy')
                except:
                    dir_stego = self.data_dir_stego_0
                    stego = np.load(dir_stego+image_name[:-3]+'npy')

            if not self.spatial:
                cover = compute_spatial_from_jpeg(cover, self.c_quant)/255
                stego = compute_spatial_from_jpeg(stego, self.c_quant)/255

            if self.transforms:
                # To have the same transformation on cover and stego
                seed = np.random.randint(2147483647)
                random.seed(seed)
                cover = self.transforms(image=cover)['image']
                random.seed(seed)
                stego = self.transforms(image=stego)['image']
            else:
                cover = torch.tensor(cover.reshape(
                    (1, self.im_size, self.im_size)))
                stego = torch.tensor(stego.reshape(
                    (1, self.im_size, self.im_size)))
            cover = cover[0:1, :, :]
            stego = stego[0:1, :, :]
            target_cover = onehot(2, 0)
            target_stego = onehot(2, 1)
            return((cover, stego), (target_cover, target_stego))

        # IF NOT PAIR TRAINING : return an image cover or stego, depending on the value of label[index]
        # (0 for cover and 1 for stego)
        else:
            image_name, label = self.image_names[index], self.labels[index]
            if(self.spatial):
                cover = np.asarray(Image.open(
                    path.join(self.cover_path, image_name[:-3]+'pgm')), dtype=np.float32)
                message_length = cover.size*self.emb_rate
            else:
                cover = np.load(path.join(self.cover_path,
                                          image_name[:-3]+'npy')).astype(np.float32)
                nz_AC = np.sum(cover != 0)-np.sum(cover[::8, ::8] != 0)
                message_length = nz_AC*self.emb_rate

            if label == 0:
                image = cover
            elif label == 1:
                index_db = self.indexs_db[index]

                if self.train_on_cost_map:  # Load the cost map and generate new stego
                    if(self.spatial):
                        rho = HILL(cover, self.H1_filter,
                                   self.L1_filter, self.L2_filter)
                    else:
                        try:
                            cost_dir = self.data_dir_prot + \
                                'data_adv_' + str(index_db) + '/adv_cost/'
                            rho = np.load(cost_dir+image_name[:-3]+'npy')
                        except:
                            cost_dir = self.cost_dir
                            rho = np.load(cost_dir+image_name[:-3]+'npy')
                    if(rho.shape == (self.im_size, self.im_size)):
                        rho = np.reshape(rho, (1, self.im_size, self.im_size))
                        rho = np.concatenate(
                            (np.copy(rho), np.zeros_like(rho), np.copy(rho)), axis=0)
                    if(self.spatial):
                        rho[0, cover <= 0] = self.WET_COST
                        rho[2, cover >= 255] = self.WET_COST
                    if not self.spatial:
                        rho[0, cover < -1023] = self.WET_COST
                        rho[2, cover > 1023] = self.WET_COST
                    p = compute_proba(rho, message_length)
                    u = np.random.uniform(
                        0, 1, (3, self.im_size, self.im_size))
                    stego = cover + \
                        np.argmax(-np.log(-np.log(u)) +
                                  np.log(p+1e-30), axis=0) - 1
                else:  # Load the stego
                    try:
                        dir_stego = self.data_dir_prot + \
                            'data_adv_' + str(index_db) + '/adv_final/'
                        stego = np.load(dir_stego+image_name[:-3]+'npy')
                    except:
                        dir_stego = self.data_dir_stego_0
                        stego = np.load(dir_stego+image_name[:-3]+'npy')

                image = stego

            image = image.astype(np.float32)

            if not self.spatial:
                image = compute_spatial_from_jpeg(image, self.c_quant)/255.

            if self.transforms:
                sample = {'image': image}
                sample = self.transforms(**sample)
                image = sample['image']
            else:
                image = image.reshape((1, self.im_size, self.im_size))
            image = image[0:1, :, :]

            target = onehot(2, label)
            return image, target

    def __len__(self) -> int:
        return self.image_names.shape[0]

    def get_labels(self):
        return list(self.labels)


def load_dataset(iteration_step, permutation_files, train_size, valid_size, test_size,
                 data_dir_prot, pair_training):

    dataset = []
    im_list = np.load(permutation_files)
    folds = np.zeros(train_size + valid_size + test_size, dtype=np.int8)
    folds[train_size:] += 1
    folds[train_size+valid_size:] += 1
    n_images = train_size+valid_size + test_size

    if(iteration_step > 0):
        indexs_db = np.load(data_dir_prot + 'data_train_' +
                            str(iteration_step)+'/index.npy')
    else:
        indexs_db = np.zeros(n_images, dtype=np.int8)

    if pair_training:
        for im, fold, ind in zip(im_list, folds, indexs_db):
            dataset.append({
                'image_name': im,
                'fold':  fold,
                'indexs_db':  ind
            })

    else:
        for label in range(2):  # 0 for cover and 1 for stego
            for im, fold, ind in zip(im_list, folds, indexs_db):
                if(label == 0):
                    index = -1
                else:
                    index = ind
                dataset.append({
                    'image_name': im,
                    'label': label,
                    'fold':  fold,
                    'indexs_db':  index
                })

    dataset = pd.DataFrame(dataset)
    return(dataset)
