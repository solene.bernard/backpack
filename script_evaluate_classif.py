import sys, os
sys.path.append('models/')

from srnet import TrainGlobalConfig as TrainGlobalConfig_sr
from srnet import get_net as get_net_sr
from xunet import TrainGlobalConfig as TrainGlobalConfig_xu
from xunet import get_net as get_net_xu
from efficientnet import TrainGlobalConfig as TrainGlobalConfig_ef
from efficientnet import get_net as get_net_ef
import os
import numpy as np
from scipy.fftpack import dct, idct
import sys
import torch
from functools import partial
import argparse
from data_loader import compute_spatial_from_jpeg

import sys
sys.path.append('models/')


def softmax(array):
    exp = np.exp(array-np.max(array, axis=1, keepdims=True))
    return(exp/np.sum(exp, axis=1, keepdims=True))


class cover_stego_loader(object):

    def __init__(self, iteration_step, mode, train_size, valid_size, test_size,
                 batch_size_eval, QF, image_size, folder_model,
                 data_dir_prot, data_dir_cover, data_dir_stego_0, permutation_files):  # mode = stego or cover
        n_images = train_size + valid_size + test_size
        self.files = np.load(permutation_files)[:n_images]
        self.train_counter = 0
        self.train_data_size = len(self.files)
        self.train_num_batches = int(
            np.ceil(1.0 * self.train_data_size / batch_size_eval))
        self.iteration_step = iteration_step
        self.mode = mode
        self.c_quant = np.load(folder_model + 'c_quant_'+str(QF)+'.npy')
        self.image_size = image_size
        self.QF = QF
        self.batch_size_eval = batch_size_eval
        self.data_dir_prot = data_dir_prot
        self.data_dir_cover = data_dir_cover
        self.data_dir_stego_0 = data_dir_stego_0

    def next_batch(self):

        borne_sup = min(self.train_counter +
                        self.batch_size_eval, len(self.files))
        n_images = borne_sup-self.train_counter

        next_batch_X = np.zeros(
            (n_images, self.image_size, self.image_size), dtype=np.float32)

        files_batch = self.files[self.train_counter:borne_sup]
        for i, file in enumerate(files_batch):
            if(self.mode == 'stego'):
                if(self.iteration_step > 0):
                    try:
                        image = np.load(
                            self.data_dir_prot+'data_adv_'+str(self.iteration_step)+'/adv_final/'+file[:-4]+'.npy')
                    except:
                        image = np.load(
                            self.data_dir_stego_0 + file[:-4]+'.npy')
                else:
                    image = np.load(self.data_dir_stego_0 + file[:-4]+'.npy')
            elif(self.mode == 'cover'):
                image = np.load(self.data_dir_cover + file[:-4] + '.npy')

            spat_image = compute_spatial_from_jpeg(image, self.c_quant)
            spat_image /= 255.0
            next_batch_X[i, :, :] = spat_image

        next_batch_X = np.reshape(
            next_batch_X, (next_batch_X.shape[0], 1, next_batch_X.shape[1], next_batch_X.shape[2]))

        self.train_counter = (self.train_counter +
                              self.batch_size_eval) % self.train_data_size
        return(next_batch_X, files_batch)

    def reset_counter(self):
        self.train_counter = 0


def evaluate_step_i(iteration_f, iteration_adv, model, data_dir_prot, train_size, valid_size, test_size,
                    batch_size_eval, QF, image_size, folder_model,
                    data_dir_cover, data_dir_stego_0, permutation_files,
                    version_eff=None, stride=None, n_loops=None):  # if iteration_adv == -1 : cover

    if(model == 'efnet'):
        net = get_net_ef(version_eff, stride).cuda()
    elif(model == 'xunet'):
        net = get_net_xu(folder_model, n_loops, image_size).cuda()
    elif(model == 'srnet'):
        net = get_net_sr(image_size).cuda()

    best_epoch = [int(x.split('-')[-1][:3])
                  for x in os.listdir(data_dir_prot+'train_'+model+'_'+str(iteration_f)+'/')
                  if 'best' in x]
    best_epoch.sort()
    best_epoch = str(best_epoch[-1])
    path = data_dir_prot+'train_'+model+'_' + \
        str(iteration_f)+'/best-checkpoint-'+'0' * \
        (3-len(best_epoch))+best_epoch+'epoch.bin'
    checkpoint = torch.load(path)
    net.load_state_dict(checkpoint['model_state_dict'])
    net.eval()

    # Create directory
    if(iteration_adv == -1):
        directory = data_dir_prot+'cover/eval_'+model+'_'+str(iteration_f)+'/'
        mode = 'cover'
    else:
        directory = data_dir_prot+'data_adv_' + \
            str(iteration_adv)+'/eval_'+model+'_'+str(iteration_f)+'/'
        mode = 'stego'

    dataloader = cover_stego_loader(iteration_adv, mode, train_size, valid_size, test_size,
                                    batch_size_eval, QF, image_size, folder_model,
                                    data_dir_prot, data_dir_cover, data_dir_stego_0, permutation_files)

    result_fi = np.empty((0, 2))
    dataloader.reset_counter()
    for batch in range(dataloader.train_num_batches):
        batch_x, images_path = dataloader.next_batch()
        with torch.no_grad():
            l = net.forward(torch.tensor(batch_x).cuda())
        result_fi = np.concatenate((result_fi, l.cpu().detach().numpy()))
    np.save(directory+'probas', softmax(result_fi)[:, 1])
    np.save(directory+'logits', result_fi)


if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--data_dir_prot', type=str)
    argparser.add_argument('--folder_model', type=str)
    argparser.add_argument('--data_dir_cover', type=str)
    argparser.add_argument('--data_dir_stego_0', type=str)
    argparser.add_argument('--permutation_files', type=str)

    argparser.add_argument('--iteration_f', type=int)
    argparser.add_argument('--iteration_adv', type=int)

    argparser.add_argument('--train_size', type=int, help='Size of train set')
    argparser.add_argument('--valid_size', type=int, help='Size of valid set')
    argparser.add_argument('--test_size', type=int, help='Size of test  set')

    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--QF', type=int)
    argparser.add_argument('--batch_size_eval', type=int)

    # Model parameters
    argparser.add_argument('--model', type=str,
                           help='Model : efnet, xunet or srnet')
    # for efnet
    argparser.add_argument('--version_eff', type=str,
                           help='Version of efficient-net, from b0 to b7')
    argparser.add_argument('--stride', type=int,
                           help='Stride at the beginning. Values=1 or 2')
    # for xunet
    argparser.add_argument('--n_loops', type=int,
                           help='Number of loops in the xunet architecture')

    params = argparser.parse_args()

    print('Evaluate model '+params.model+' at iteration ' + str(params.iteration_f) +
          ' on the adv images generated at ' + str(params.iteration_adv))

    evaluate_step_i(**vars(params))
