import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.fftpack import dct, idct
from scipy.optimize import root_scalar
from scipy.signal import fftconvolve, convolve2d


def softmax(x, axis=0):
    y = np.exp((x-np.max(x, axis=axis, keepdims=True)))
    return(y/np.sum(y, axis=axis, keepdims=True))


def gibsprobability(rho, lbd):
    return(softmax(-lbd*rho))


def H(rho, lbd):
    return(ternary_entropy(gibsprobability(rho, lbd)))


def compute_nz_AC(c_coeffs):
    s = np.sum(c_coeffs != 0)-np.sum(c_coeffs[::8, ::8] != 0)
    return(s)


def ternary_entropy(p):
    P = np.copy(p)
    P[P == 0] = 1
    H = -((P) * np.log2(P))
    Ht = np.sum(H)
    return Ht


def calc_lambda(rho, ml):
    lbd_r = 1e-30
    lbd_l = lbd_r
    while lbd_r < 1e14:
        if H(rho, lbd_r) < ml:
            break
        lbd_l = lbd_r
        #lbd_r = (lbd_r+1e-30)*10
        lbd_r *= 10
    #print(lbd_l, lbd_r, H(rho, lbd_l), H(rho, lbd_r))
    r = root_scalar(lambda x: ml - H(rho, x),
                    bracket=[lbd_l, lbd_r], xtol=1e-10)
    return(r.root)


def compute_proba(rho, message_length):
    """
    Embedding simulator simulates the embedding made by the best possible 
    ternary coding method (it embeds on the entropy bound). This can be 
    achieved in practice using Multi-layered syndrome-trellis codes (ML STC) 
    that are asymptotically approaching the nzbound
    """
    lbd = calc_lambda(rho, message_length)
    p = gibsprobability(rho, lbd)
    return(p)


def dct2(x):
    return dct(dct(x, norm='ortho').T, norm='ortho').T


def idct2(x):
    return idct(idct(x, norm='ortho').T, norm='ortho').T


def compute_spatial_from_jpeg(jpeg_im, c_quant):
    """
    Compute the 8x8 DCT transform of the jpeg representation
    """
    w, h = jpeg_im.shape
    spatial_im = np.zeros((w, h))
    for bind_i in range(int(w//8)):
        for bind_j in range(int(h//8)):
            block = idct2(jpeg_im[bind_i*8:(bind_i+1)*8,
                                  bind_j*8:(bind_j+1)*8]*(c_quant))+128
            # block[block>255]=255
            # block[block<0]=0
            spatial_im[bind_i*8:(bind_i+1)*8, bind_j*8:(bind_j+1)*8] = block
    spatial_im = spatial_im.astype(np.float32)
    return(spatial_im)


# PYTORCH FUNCTIONS

class find_lambda(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, entropy_vec, rho_vec):
        entropy, rho = entropy_vec.cpu().detach().numpy(), rho_vec.cpu().detach().numpy()
        lbd = calc_lambda(rho, entropy)
        ctx.lbd = lbd
        ctx.rho = rho_vec

        lbd = torch.tensor(lbd, requires_grad=True)
        return(lbd)

    @staticmethod
    def backward(ctx, grad_output):
        with torch.enable_grad():
            lbd = torch.tensor(ctx.lbd, requires_grad=True)

            # Computation of probas and entropy
            softmax_fn = torch.nn.Softmax(dim=0)
            probas = softmax_fn(-lbd*(ctx.rho-torch.min(ctx.rho, dim=0)[0]))
            H_result = - \
                torch.sum(torch.mul(probas, torch.log(probas+1e-30)))/np.log(2.)

            # Gradients
            grad_H_rho = torch.autograd.grad(
                H_result, ctx.rho, retain_graph=True)[0]
            grad_H_lbd = torch.autograd.grad(H_result, lbd)[0]

            # Implicit gradient
            g = -grad_H_rho/grad_H_lbd
        return(None, g)


def get_inv_perm(image_size):
    x = np.arange(image_size**2).reshape((image_size, image_size))
    X = np.zeros((8, 8, image_size//8, image_size//8))
    for i in range(image_size//8):
        for j in range(image_size//8):
            X[:, :, i, j] = x[i*8:(i+1)*8, j*8:(j+1)*8]
    inv_perm = np.argsort(X.flatten())
    return(inv_perm)


class IDCT8_Net(nn.Module):

    def __init__(self, image_size, QF, folder_model):
        super(IDCT8_Net, self).__init__()
        n = image_size//8
        c_quant = np.load(folder_model+'c_quant_'+str(QF) +
                          '.npy')  # 8*8 quantification matrix
        self.c_quant = torch.tensor(
            np.tile(c_quant, (n, n)).reshape((image_size, image_size)))
        self.inv_perm = get_inv_perm(image_size)
        self.IDCT8_kernel = torch.tensor(np.load(folder_model+'DCT_8.npy')
                                         .reshape((8, 8, 64, 1)).transpose((2, 3, 0, 1)))
        self.im_size = image_size

    def forward(self, x):
        x = torch.mul(x, self.c_quant)  # dequantization
        x = F.conv2d(x, self.IDCT8_kernel, stride=(8, 8), padding=0)  # 2D-IDCT
        # Reorder coefficients and reshape
        x = torch.reshape(x, (x.size()[0], self.im_size**2))
        x = x[:, self.inv_perm]
        x = torch.reshape(x, (x.size()[0], 1, self.im_size, self.im_size))
        x += 128
        return x


def HILL(cover, H1=None, L1=None, L2=None):

    if H1 is None:
        H1 = 4 * np.array([[-0.25, 0.5, -0.25],
                           [0.5, -1, 0.5],
                           [-0.25, 0.5, -0.25]])
        L1 = (1.0/9.0)*np.ones((3, 3))
        L2 = (1.0/225.0)*np.ones((15, 15))

    # High pass filter H1
    R = convolve2d(cover, H1.reshape((3, 3)), mode='same', boundary='symm')

    # Low pass filter L1
    xi = convolve2d(abs(R), L1.reshape((3, 3)), mode='same', boundary='symm')
    inv_xi = 1/(xi+1e-20)

    # Low pass filter L2
    rho = convolve2d(inv_xi, L2.reshape((15, 15)),
                     mode='same', boundary='symm')
    # adjust embedding costs
    # rho[rho > WET_COST] = WET_COST # threshold on the costs
    # rho[np.isnan(rho)] = WET_COST # Check if all elements are numbers
    return rho
