import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from tools_stegano import IDCT8_Net, find_lambda
from double_tanh import double_compact_tanh, double_tanh


class BackPack(nn.Module):
    def __init__(self, image_size, QF, folder_model, c_coeffs, rho_0, entropy, N, nets, ecdf_list, attack):
        super(BackPack, self).__init__()
        self.net_decompress = IDCT8_Net(image_size, QF, folder_model)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.N = N
        self.entropy = torch.tensor(entropy)
        self.im_size = image_size
        self.rho_vec = torch.nn.Parameter(
            data=torch.tensor(rho_0), requires_grad=True)
        self.c_coeffs = torch.tensor(c_coeffs).double()
        self.spatial_cover = self.net_decompress.forward(
            torch.reshape(self.c_coeffs, (1, 1, self.im_size, self.im_size)))/255
        self.nets = nets
        proba_cover = [torch.nn.Softmax(dim=1)(net.forward(self.spatial_cover.to(self.device).float()))[0, 1]
                       for net in nets]
        self.proba_cover = np.array(
            [ecdf(x.cpu().detach().numpy()) for x, ecdf in zip(proba_cover, ecdf_list)])
        self.find_lambda_fn = find_lambda.apply

        self.attack = attack
        if(self.attack == 'SGE'):
            self.mod = torch.reshape(torch.tensor([-1, 0, 1]), (3, 1, 1))
            self.smoothing_fn = lambda u, p, tau: \
                torch.sum(torch.nn.Softmax(dim=1)((-torch.log(-torch.log(u))+torch.log(p+1e-30))/tau)*self.mod, axis=1) if tau > 0 \
                else torch.argmax(-torch.log(-torch.log(u))+torch.log(p+1e-30), axis=1) - 1
        elif(self.attack == 'DoCoTanh'):
            self.smoothing_fn = double_compact_tanh.apply
        elif(self.attack == 'DoTanh'):
            self.smoothing_fn = double_tanh
        self.ecdf_list = ecdf_list

    def forward(self, tau):
        if(self.attack == 'SGE'):
            u = torch.rand(size=(self.N, 3, self.im_size, self.im_size))
        else:
            u = torch.rand(size=(self.N, self.im_size, self.im_size))

        # Compute modifications for soft stego
        lbd = self.find_lambda_fn(self.entropy, self.rho_vec)
        probas = torch.nn.Softmax(
            dim=0)(-lbd*(self.rho_vec-torch.min(self.rho_vec, dim=0)[0]))
        b = self.smoothing_fn(u, probas, tau)
        stego_soft = torch.reshape(
            self.c_coeffs+b, (self.N, 1, self.im_size, self.im_size))

        spatial_image_soft = self.net_decompress.forward(stego_soft)/255
        logits = [torch.reshape(net.forward(spatial_image_soft.to(self.device).float()), (1, self.N, 2))
                  for net in self.nets]
        logits = torch.cat(logits)
        probas_soft = torch.nn.Softmax(dim=2)(logits)[:, :, -1]
        mean_probas_soft = torch.mean(probas_soft, axis=-1)
        mean_probas_soft_cpu = mean_probas_soft.clone().cpu().detach().numpy()
        mean_probas_soft_cpu = np.array(
            [ecdf(x) for ecdf, x in zip(self.ecdf_list, mean_probas_soft_cpu)])
        argmax = np.argmax(mean_probas_soft_cpu)
        best_probas_soft = mean_probas_soft[argmax]

        # Compute modifications for real/hard stego
        with torch.no_grad():
            b_hard = self.smoothing_fn(u, probas, 0)
            stego_hard = torch.reshape(
                self.c_coeffs+b_hard, (self.N, 1, self.im_size, self.im_size))
            spatial_image_hard = self.net_decompress.forward(stego_hard)/255
            logits = [torch.reshape(net.forward(spatial_image_hard.to(self.device).float()), (1, self.N, 2))
                      for net in self.nets]
            logits = torch.cat(logits)
            probas_hard = torch.nn.Softmax(dim=2)(logits)[:, :, -1]
            mean_probas_hard = torch.mean(
                probas_hard, axis=-1).cpu().detach().numpy()
            mean_probas_hard_cpu = np.array(
                [ecdf(x) for ecdf, x in zip(self.ecdf_list, mean_probas_hard)])

        return best_probas_soft, mean_probas_soft_cpu, mean_probas_hard_cpu, stego_hard
