import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from tools_stegano import IDCT8_Net, find_lambda


def logit(y, eps=1e-20):
    return -1.0 * torch.log((1.0 - torch.min(y, torch.tensor(1.-eps).double())) / torch.max(y, torch.tensor(eps).double()))


def compact_tanh(u, tau):
    return(torch.tanh(logit(u)/tau))


def g_warp(u, l, c, r):
    a = (u-l)/(r-l)
    a = torch.clamp((u-l)/(r-l), 0, 1)
    b = np.log(0.5)/(torch.log((c-l)/(r-l)))
    e = b*torch.log(a)
    e = torch.exp(e)
    return(e)

# Double Tanh


def double_tanh(u, p, tau):
    if(tau > 0):
        b = -0.5*(torch.tanh((p[0]-u)/tau)+torch.tanh((p[0]+p[1]-u)/tau))
    else:
        cumsum = torch.cumsum(p, axis=0)
        b = torch.full_like(u, torch.tensor(-1).double())
        b[u > cumsum[0, None]] += 1
        b[u > cumsum[1, None]] += 1
    return(b)

# Double Compact Tanh


class double_compact_tanh(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, u, p, tau):
        if tau > 0:
            with torch.enable_grad():
                gamma = (p[0]+1-p[-1])/2
                step1 = torch.ones_like(u).double()
                step2 = -torch.ones_like(u).double()
                p = (p[:, None]).repeat((1, len(u), 1, 1))
                gamma = (gamma[None]).repeat((len(u), 1, 1))

                # Handle 0 probabilities of -1 or +1
                # (probability of modification 0 is assumed to be never equal to 0)
                bool1 = (u < gamma) & (p[1] < 1.)
                bool2 = (u > gamma) & (p[1] < 1.)

                step1[bool1] = compact_tanh(
                    g_warp(u[bool1], 0, p[0, bool1], gamma[bool1]), tau)
                step2[bool2] = compact_tanh(
                    g_warp(u[bool2], gamma[bool2], (p[0]+p[1])[bool2], 1.), tau)

                b = 0.5*(step1 + step2)

                # Save data
                ctx.tau = tau
                ctx.gamma = gamma
                ctx.u = u
                ctx.p = p

        else:
            cumsum = torch.cumsum(p, axis=0)
            b = torch.full_like(u, torch.tensor(-1).double())
            b[u > cumsum[0, None]] += 1
            b[u > cumsum[1, None]] += 1

        return(b)

    # Compute manually the gradient for "if" cases
    @staticmethod
    def backward(ctx, grad_output):

        # Same computation than in forward, to retrieve the gradient
        with torch.enable_grad():

            gamma = (ctx.p[0]+1-ctx.p[-1])/2

            bool1 = (ctx.u < gamma) & (ctx.p[1] < 1.)
            bool2 = (ctx.u > gamma) & (ctx.p[1] < 1.)

            p1 = ctx.p[:, bool1]
            p2 = ctx.p[:, bool2]
            gam1 = ctx.gamma[bool1]
            gam2 = ctx.gamma[bool2]
            u1 = ctx.u[bool1]
            u2 = ctx.u[bool2]

            step1 = compact_tanh(g_warp(u1, 0, p1[0], gam1), ctx.tau)
            step2 = compact_tanh(g_warp(u2, gam2, p2[0]+p2[1], 1.), ctx.tau)

            gr = torch.zeros_like(ctx.p)
            gr1 = 0.5 * \
                torch.autograd.grad(
                    step1, p1, grad_outputs=grad_output[bool1], retain_graph=True)[0]
            gr2 = 0.5 * \
                torch.autograd.grad(
                    step2, p2, grad_outputs=grad_output[bool2], retain_graph=True)[0]

            gr[:, bool1] = gr1
            gr[:, bool2] = gr2
            gr = gr[:, 0]

            # Avoid NaNs
            gr[torch.isnan(gr)] = 0

        return(None, gr, None)
