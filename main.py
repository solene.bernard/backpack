#!/usr/bin/env python

import os
import time

import numpy as np
import argparse
import sys
from shutil import copy
import os
import glob


from generate_train_db import generate_train_db
from write_description import describe_exp_txt
from write_jobs import create_training_dictionnary, wait, write_command, run_job


def p_error(iteration_f, model, train_size, valid_size, test_size, data_dir_prot):
    if(iteration_f > 0):
        indexs = np.load(data_dir_prot + 'data_train_' +
                         str(iteration_f)+'/index.npy')
    else:
        indexs = np.zeros(train_size+valid_size+test_size, dtype=np.int8)
    probas_stego_z = np.array([np.load(data_dir_prot+'data_adv_'+str(i)+'/eval_'+model+'_'+str(iteration_f)+'/probas.npy')
                               for i in range(iteration_f+1)])
    predictions_cover = np.load(
        data_dir_prot+'cover/eval_'+model+'_'+str(iteration_f)+'/probas.npy')
    predictions_stego = np.array([probas_stego_z[ind, i]
                                  for i, ind in enumerate(indexs)])

    # Choose best threshold
    thresholds = np.linspace(0, 1, 51)[1:-1]
    PE_train, PE_valid, PE_test = [], [], []

    for t in thresholds:
        pred_cover, pred_stego = predictions_cover[:
                                                   train_size], predictions_stego[:train_size]
        fa = np.sum(pred_cover > t)/train_size
        md = np.sum(pred_stego <= t)/train_size
        pe_train = (fa+md)/2
        pred_cover, pred_stego = predictions_cover[train_size:train_size +
                                                   valid_size], predictions_stego[train_size:train_size+valid_size]
        fa = np.sum(pred_cover > t)/valid_size
        md = np.sum(pred_stego <= t)/valid_size
        pe_valid = (fa+md)/2
        pred_cover, pred_stego = predictions_cover[train_size +
                                                   valid_size:], predictions_stego[train_size+valid_size:]
        fa = np.sum(pred_cover > t)/test_size
        md = np.sum(pred_stego <= t)/test_size
        pe_test = (fa+md)/2
        PE_train.append(pe_train)
        PE_valid.append(pe_valid)
        PE_test.append(pe_test)
    PE_valid = np.asarray(PE_valid)
    mini = np.argmin(PE_valid)  # choose best thresholds on validation set
    dir_log = data_dir_prot+'train_'+model+'_'+str(iteration_f)
    return(PE_train[mini], PE_valid[mini], PE_test[mini])


def create_folder(path, list_dir):
    for d in list_dir:
        if not os.path.exists(path+d+'/'):
            os.makedirs(path+d+'/')


def run_iteration(iteration_step, label, data_dir_prot, data_dir_cover, data_dir_stego_0, cost_dir,
                  image_size, QF, folder_model, permutation_files, version_eff, stride, n_loops,
                  model, train_size, valid_size, test_size, attack, attack_last, emb_rate,
                  batch_adv, n_iter_max_backpack, N_samples, tau_0, precision, lr,
                  num_of_threads, training_dictionnary, spatial, strategy, server, name_account):

    n_images = train_size+valid_size+test_size
    models = model.split(',')

    def custom_command(mode, iteration_step, my_model):
        return(write_command(mode, iteration_step, my_model, data_dir_prot, data_dir_cover, data_dir_stego_0, cost_dir,
                             image_size, QF, folder_model, permutation_files, version_eff, stride, n_loops,
                             train_size, valid_size, test_size, attack, attack_last, emb_rate,
                             batch_adv, n_iter_max_backpack, N_samples, tau_0, precision, lr,
                             num_of_threads, training_dictionnary, spatial))

    if(iteration_step==0):
        create_folder(data_dir_prot, ['data_adv_0', 'cover'])

    else:

        # GENERATE ADV DATA BASE OF THE BEST LAST CLASSIFIER
        directory_adv = data_dir_prot+'data_adv_'+str(iteration_step)+'/'
        create_folder(directory_adv, ['adv_final', 'adv_cost'])

        print('Generating adv step ' + str(iteration_step))
        num_batch = n_images // batch_adv
        command = 'script_attack.py ' + \
            custom_command('attack', iteration_step, model)
        
        if(server):
            command += ' --idx_start=$SLURM_ARRAY_TASK_ID'
            run_job('attack', label, command, iteration_step, name_account,
                    gpu=True, num_batch=num_batch)
            wait(label)
        else:
            os.system('python '+ command + ' --idx_start=0')

        # EVALUATION OF ALL THE CLASSIFIERS ON THE NEW ADV DATA BASE
        for i in range(iteration_step):
            for my_model in models:
                if(i) == -1:
                    directory = data_dir_prot + \
                        'cover/eval_'+my_model+'_'+str(i)+'/'
                else:
                    directory = data_dir_prot+'data_adv_' + \
                        str(iteration_step)+'/eval_'+my_model+'_'+str(i)+'/'
                if not os.path.exists(directory):
                    os.makedirs(directory)
                print('Evaluation of classifier '+my_model + ' ' +
                      str(i)+' on database ' + str(iteration_step))
                command = 'script_evaluate_classif.py' + custom_command('classif', iteration_step, my_model) \
                    + ' --iteration_f=' + \
                    str(i)+' --iteration_adv='+str(iteration_step)

                if(server):
                    run_job('eval_'+str(my_model), label,
                        command, iteration_step, name_account, gpu=True)
                else:
                    os.system('python '+ command)
                
        if(server):
            wait(label)

        # GENERATION OF THE TRAIN DATA BASE
        generate_train_db(iteration_step, strategy, models,
                          permutation_files, data_dir_prot)

    # TRAINING NEW CLASSIFIER FOR EACH MODEL
    for my_model in models:
        print('Training '+my_model+' at iteration ' + str(iteration_step))
        create_folder(data_dir_prot, [
                      'train_'+my_model+'_'+str(iteration_step)])
        command = 'script_train.py' + \
            custom_command('train', iteration_step, my_model)
        
        if(server):
            run_job('train_'+my_model, label, command, iteration_step, name_account,
                    num_of_threads=num_of_threads, gpu=True)
        else:
            os.system('python '+ command)

    if(server):
        wait(label)

    # EVALUATE NEW CLASSIFIERS ON ALL STEGO DATA BASES AND ON COVER
    for i in range(-1, iteration_step+1):  # -1 for cover
        for my_model in models:
            if(i) == -1:
                directory = data_dir_prot + 'cover/'
            else:
                directory = data_dir_prot+'data_adv_'+str(i)+'/'
            create_folder(
                directory, ['eval_'+my_model+'_'+str(iteration_step)])
            print('Evaluation of classifier '+my_model + ' ' +
                  str(iteration_step)+' on database ' + str(i))
            command = 'script_evaluate_classif.py' + custom_command('classif', iteration_step, my_model) \
                + ' --iteration_f='+str(iteration_step) + \
                ' --iteration_adv='+str(i)

            if(server):
                run_job('eval_'+str(model), label,
                    command, iteration_step, name_account, gpu=True)
            else:
                os.system('python '+ command)

    if(server):
        wait(label)

    for my_model in models:
        print(my_model, p_error(iteration_step, my_model,
                                train_size, valid_size, test_size, data_dir_prot))

    return(True)


def run_protocol(begin_step, server, name_account, number_steps, label, data_dir_prot, data_dir_cover, data_dir_stego_0, cost_dir,
                 image_size, QF, folder_model, permutation_files, version_eff, stride, n_loops,
                 model, train_size, valid_size, test_size, attack, attack_last, emb_rate,
                 batch_adv, n_iter_max_backpack, N_samples, tau_0, precision, lr, strategy,
                 num_of_threads, spatial, batch_size_classif_xu, batch_size_eval_xu, epoch_num_xu,
                 CL_xu, start_emb_rate_xu, pair_training_xu, batch_size_classif_sr, batch_size_eval_sr, epoch_num_sr,
                 CL_sr, start_emb_rate_sr, pair_training_sr, batch_size_classif_ef, batch_size_eval_ef, epoch_num_ef,
                 CL_ef, start_emb_rate_ef, pair_training_ef, train_on_cost_map):

    training_dictionnary = create_training_dictionnary(model, batch_size_classif_xu, batch_size_eval_xu, epoch_num_xu,
                                                       CL_xu, start_emb_rate_xu, pair_training_xu, batch_size_classif_sr, batch_size_eval_sr, epoch_num_sr,
                                                       CL_sr, start_emb_rate_sr, pair_training_sr, batch_size_classif_ef, batch_size_eval_ef, epoch_num_ef,
                                                       CL_ef, start_emb_rate_ef, pair_training_ef, train_on_cost_map)

    iteration_step = begin_step

    describe_exp_txt(begin_step, number_steps, num_of_threads,
                     QF, image_size, emb_rate, data_dir_cover, data_dir_stego_0, cost_dir,
                     strategy, model, version_eff, stride, n_loops,
                     train_size, valid_size, test_size, permutation_files, training_dictionnary,
                     attack, n_iter_max_backpack, N_samples, tau_0, precision, data_dir_prot)

    while iteration_step < begin_step+number_steps+1:

        run_iteration(iteration_step, label, data_dir_prot, data_dir_cover, data_dir_stego_0, cost_dir,
                      image_size, QF, folder_model, permutation_files, version_eff, stride, n_loops,
                      model, train_size, valid_size, test_size, attack, attack_last, emb_rate,
                      batch_adv, n_iter_max_backpack, N_samples, tau_0, precision, lr,
                      num_of_threads, training_dictionnary, spatial, strategy, server, name_account)

        iteration_step += 1


if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--begin_step', type=int)
    argparser.add_argument('--server', type=str, default='no')
    argparser.add_argument('--name_account', type=str, default='srp', help='If run on a server, name of the account from which launch the computations')
    argparser.add_argument('--number_steps', type=int, default=10)
    argparser.add_argument('--folder_model', type=str,
                           help='The path to the folder where the architecture of models are saved')
    argparser.add_argument('--permutation_files', type=str,
                           help='The path to the permutation of indexes of images (to shuffle for train, valid and test set).')
    argparser.add_argument('--num_of_threads', type=int,
                           default=10, help='number of cpus')

    # MODEL ARCHITECTURE
    argparser.add_argument('--model', type=str, help='model in the protocol : efnet, xunet or srnet, \
                    or multiple models separated by commas like "xunet,srnet"')
    # For Efficient net
    argparser.add_argument('--version_eff', type=str, default='b3',
                           help='Version of efficient-net, from b0 to b7')
    argparser.add_argument('--stride', type=int, default=1,
                           help='Stride at the beginning. Values=1 or 2')
    # for xunet
    argparser.add_argument('--n_loops', type=int, default=5,
                           help='Number of loops in th xunet architecture')

    argparser.add_argument('--data_dir_prot', type=str)
    argparser.add_argument('--data_dir_cover', type=str)
    argparser.add_argument('--data_dir_stego_0', type=str)
    argparser.add_argument('--cost_dir', type=str)

    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--QF', type=int)
    argparser.add_argument('--emb_rate', type=float)
    argparser.add_argument('--spatial', type=str, default='no')

    # FOR LABELING AND QUEING
    argparser.add_argument('--label', type=str)

    # FOR DATA TRAIN
    # minmax, random or lastit
    argparser.add_argument('--strategy', type=str, default='minmax')

    # FOR ADVERSARIAL COST MAP
    argparser.add_argument('--attack', type=str)
    argparser.add_argument('--attack_last', type=str,default='no')
    argparser.add_argument('--lr', type=float)
    argparser.add_argument('--batch_adv', type=int, default=100)
    argparser.add_argument('--n_iter_max_backpack', type=int)
    argparser.add_argument('--N_samples', type=int)
    argparser.add_argument('--tau_0', type=float)
    argparser.add_argument('--precision', type=float)

    # FOR TRAINING
    argparser.add_argument('--train_size', type=int, default=4000)
    argparser.add_argument('--valid_size', type=int, default=1000)
    argparser.add_argument('--test_size', type=int, default=5000)
    argparser.add_argument('--train_on_cost_map', type=str, default='yes',
                           help='yes or no. If yes stegos are created from the cost maps at during the training, elif no classifiers are trained directly with the stegos.')
    # FOR TRAINING XU
    argparser.add_argument('--batch_size_classif_xu', type=int, default=16)
    argparser.add_argument('--batch_size_eval_xu', type=int, default=16)
    argparser.add_argument('--epoch_num_xu', type=int, default=30)
    argparser.add_argument(
        '--CL_xu', type=str, help='yes or no. If yes starts from emb_rate of start_emb_rate and decrease until reach emb_rate')
    argparser.add_argument('--start_emb_rate_xu', type=float,
                           default=0.7, help='Is CL=yes, is the starting emb_rate')
    argparser.add_argument('--pair_training_xu', type=str, help='yes or no')
    # FOR TRAINING SR
    argparser.add_argument('--batch_size_classif_sr', type=int, default=16)
    argparser.add_argument('--batch_size_eval_sr', type=int, default=16)
    argparser.add_argument('--epoch_num_sr', type=int, default=30)
    argparser.add_argument(
        '--CL_sr', type=str, help='yes or no. If yes starts from emb_rate of start_emb_rate and decrease until reach emb_rate')
    argparser.add_argument('--start_emb_rate_sr', type=float,
                           default=0.7, help='Is CL=yes, is the starting emb_rate')
    argparser.add_argument('--pair_training_sr', type=str, help='yes or no')
    # FOR TRAINING EF
    argparser.add_argument('--batch_size_classif_ef', type=int, default=16)
    argparser.add_argument('--batch_size_eval_ef', type=int, default=16)
    argparser.add_argument('--epoch_num_ef', type=int, default=30)
    argparser.add_argument(
        '--CL_ef', type=str, help='yes or no. If yes starts from emb_rate of start_emb_rate and decrease until reach emb_rate')
    argparser.add_argument('--start_emb_rate_ef', type=float,
                           default=0.7, help='Is CL=yes, is the starting emb_rate')
    argparser.add_argument('--pair_training_ef', type=str, help='yes or no')

    params = argparser.parse_args()

    params.server = params.server=='yes'

    run_protocol(**vars(params))
