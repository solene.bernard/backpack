import os, sys
import numpy as np
from multiprocessing import Pool, cpu_count

# Dirty import of parent module
#sys.path.append(os.path.dirname(__file__) + '/..')
from tools import msg


def apply(map_arg) :
    """Apply arguments to object
    It permits to use a mapping to external threads.
    """
    engine, file = map_arg
    msg('Extracting from ' + file, 'extract')
    return engine.run(file)


class Extractor() :
    """
    An abstract class, every extractor should inherit from it.

    self.conf must contain :
     - extract.nb_cores to specify max number of cores to use
    """

    #file_type = None  # file extension the extractor process on

    def __init__(self, conf) :
        self.conf = conf

    def run(self, file) :
        """
        Runs the extraction on one file. Returns the feature vector.

        :param file:    path to the file that must be read

        :return:        features vector
        :rtype:         a numpy array of dimension 1
        """
        
        raise NameError('This method is abstract')

    def run_set(self, files_list) :
        """
        Runs the extraction on the list of specified files. Returns corresponding features as a matrix.

        :param file:    paths to the files that must be read

        :return:        features matrix
        :rtype:         a numpy array of dimension 2
        """
        pool = Pool(self.conf.nb_cores)
        # dumps exports the methods to be compatible out of the context
        files_list = [(self, x) for x in files_list]
        results = pool.map(apply, files_list)
        #print results[0]
        results = np.array(results[:])
        #print results.shape
        pool.close()
        pool.join()
        msg('Outputed a %s features matrix' % str(results.shape), 'debug')
        return np.array(results)  # each file is represented with a list


class ConcatExtractors(Extractor) :
    """
    Build an extractor from a list of extractors.

    This new extractor outputs the concatenation of each extractors results.

    Attributes of an instance :
        :param sub_extractors:  the extractors to be concatenated
        :type  sub_extractors:  list of Extractor
    """

    def __init__(self, extractors) :
        """
        Init with a list of extractors.
        """
        self.conf = extractors[0].conf
        #self.file_type = extractors[0].file_type
        self.sub_extractors = extractors

    def run(self, file) :
        """
        Run the extraction for each sub_extractors.
        """
        results = [sub.run(file) for sub in self.sub_extractors]
        return np.concatenate(results)
