import os, sys
import numpy as np

import DCTR
import GFR
import JRM

import multiprocessing
import argparse


from extractor import ConcatExtractors

# List available extractors


def save(file, features) :
    """Saves features in a file."""
    np.save(file, features)


def load(file) :
    """Returns features saved in a file"""
    ret = np.load(file)
    msg('Opened ' + file, 'debug')
    return ret


def join(features_list) :
    """
    Given a list of features representing sets of images, returns a feature matrix representing every image.

    :param features_list: list of numpy arrays
    :rtype:               a numpy array (its number of lines is the sum of line counts of features_list's elements)
    """
    return np.concatenate(features_list, axis=0)  # only has to take every lines


def count_features(feat_files) :
    """
    Counts total number of features contained in provided files.

    :param feat_files: a list of files to open
    """
    nb_features = 0
    for file in feat_files :
        feats = load(file)
        nb_features += np.size(feats, axis=0)
    return nb_features

def process(files, sizes, feat_dir, prefix='cover',seed=0) :
    """
    Process for cover or stego : read files and separate them in groups reparted with sizes specified.

    :files:     files we load features from
    :sizes:     list of the number of features needed per files
                the first item corresponds to training, remaining to tests
    :prefix:    string to start the outputed filename with
    """
    current_file = 0  # index of last loaded file
    current_pool = load(files[0])  # features from last loaded file that were not used yet


    print('sizes:' , sizes)
    sum_size = sizes[0]+sizes[1]
    print('sum:' , sum_size)
    #for i in range(len(sizes)) :
    # Fills this matrix with correct number of features
    #print current_pool
    out_feats = np.zeros((0, np.size(current_pool, axis=1)))
    while np.size(out_feats, axis=0) < sum_size :
        # Number of features we want to transfer from current file
        out_feats = np.concatenate([out_feats, current_pool], axis=0)

        # Loads a new file if necessary
        if current_file + 1 < len(files) :
            current_file += 1
            current_pool = load(files[current_file])

    #seed = 1000 #np.random.randint(10000)
    im_idx = range(sum_size)
    if seed!=0:
        np.random.seed(seed)
        np.random.shuffle(im_idx)

    save('%s/%s_train' % (feat_dir, prefix), out_feats[im_idx[:sizes[0]],:])
    save('%s/%s_test_%d' % (feat_dir, prefix, 0), out_feats[im_idx[sizes[0]:],:])



def separate_features(cover_files, stego_files, feat_dir, nb_learn=None, ratio=0.5, learn_split=1,reproducible=1) :
    """
    Keeps `nb_learn` images from cover and stego for training.
    The remaining is used for tests, separated in `learn_split` files.

    The goal of this function is to avoid loading all features in the ram simultaneously.

    :param cover_files:   list of files containing cover features
    :param stego_files:   list of files containing stego features
    :param feat_dir:      directory in which provided features will be saved
    :param nb_learn:      number of stego/cover images used for training
    :param ratio:         if `nb_learn` isn't specified, the ratio to keep
    :param learn_split:   number of splited tests to generate
    """

    msg('Counting features before concatenation', 'concat')
    nb_tot_cover = count_features(cover_files)
    nb_tot_stego = count_features(stego_files)
    nb_both = min(nb_tot_cover, nb_tot_stego)
    msg('Found %d cover and %d stego' % (nb_tot_cover, nb_tot_stego), 'concat')
    # Auto-set of nb_learn
    if nb_learn is None or nb_learn >= nb_both :
        nb_learn = int(nb_both * ratio)

    # cover_files.sort()  # ensure files will be used in same order
    # stego_files.sort()  # ensure files will be used in same order

    # seed = 1000 #np.random.randint(10000)
    # np.random.seed(seed)
    # np.random.shuffle(cover_files)
    # np.random.seed(seed)
    # np.random.shuffle(stego_files)

    if reproducible:
        seed = 1 
    else:
        seed = np.random.randint(10000) # = 0
    # Process for cover
    msg('Concatenates cover features', 'concat')
    sizes = [nb_learn]
    sizes += partition(nb_tot_cover - nb_learn, learn_split)
    process(cover_files, sizes, feat_dir, 'cover',seed)

    # Process for stego
    msg('Concatenates stego features', 'concat')
    sizes = [nb_learn]
    sizes += partition(nb_tot_stego - nb_learn, learn_split)
    process(stego_files, sizes, feat_dir, 'stego',seed)

    msg('Concatenation is over', 'concat')




#files = np.load('../SRNet/permutation_files.npy')
#files = np.array([x[:-4]+'.npy' for x in files])



if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])

    argparser.add_argument('--nb_cores', type=int)

    argparser.add_argument('--iteration_step', type=int, default=0)
    argparser.add_argument('--stego_or_cover', type=str, default='stego')
    argparser.add_argument('--folder_model', type=str)
    argparser.add_argument('--features',type=str)  #DCTR, GFR or JRM

    argparser.add_argument('--path_folder_stego', type=str)
    argparser.add_argument('--path_folder_stego_0', type=str)
    argparser.add_argument('--path_folder_cover', type=str)
    argparser.add_argument('--path_save', type=str)

    argparser.add_argument('--train_size', type=int, default=4000)
    argparser.add_argument('--valid_size', type=int, default=1000)
    argparser.add_argument('--test_size', type=int, default=10000)
    argparser.add_argument('--permutation_files', type=str)

    argparser.add_argument('--QF', type=int)

    conf = argparser.parse_args()

    conf.color = False
    #conf.nb_cores = multiprocessing.cpu_count()
    #conf.nb_cores = len(os.sched_getaffinity(0))
    #conf.nb_cores = 16

    print(conf.nb_cores)

    files = np.load(conf.permutation_files)
    files = np.array([x[:-4]+str('.npy') for x in files])

    #files_tr = files[:conf.train_size]
    #files_te = files[conf.train_size+conf.valid_size:conf.train_size+conf.valid_size+conf.test_size]


    # extractors = [DCTR.Extractor(conf), GFR.Extractor(conf)]
    # extract = ConcatExtractors(extractors)
    # extract.run(files)

    if conf.features=='DCTR':
        extract = DCTR.Extractor(conf)
    elif conf.features=='GFR':
        extract = GFR.Extractor(conf)
    elif conf.features=='JRM':
        extract = JRM.Extractor(conf)

    if conf.stego_or_cover=='stego':

        if(conf.iteration_step>0):
            path_stego = conf.path_folder_stego + 'data_adv_'+str(conf.iteration_step) +'/adv_final/'
            dir_log = conf.path_save + 'data_adv_' + str(conf.iteration_step) + '_'+conf.features+'.npy'
        else:
            path_stego = conf.path_folder_stego_0 
            dir_log = conf.path_save + 'data_adv_0_'+conf.features+'.npy'


        path_stego_0 = conf.path_folder_stego_0 
        f = os.listdir(path_stego)
        files_stego = np.array([path_stego+x if x in f else path_stego_0+x for x in files ])
        features = extract.run_set(files_stego)

        if not os.path.exists(conf.path_save):
            os.makedirs(conf.path_save)

        np.save(dir_log, features)

    else:
        path_cover = conf.path_folder_cover 
        files_cover = np.array([path_cover+x for x in files])
        features = extract.run_set(files_cover)

        if not os.path.exists(conf.path_save):
            os.makedirs(conf.path_save)
            
        np.save(conf.path_save + 'cover_'+conf.features+'.npy', features)





