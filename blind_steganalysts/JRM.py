# ccJRM - Version 1

import numpy as np
from jpeg import jpeg
from scipy.misc import imread
from PIL import Image
import os, sys
import glob
import time
import h5py
import multiprocessing
from multiprocessing import Pool
from scipy.fftpack import dct, idct

#Preprocessing
def dct2(x):
    return dct(dct(x, norm='ortho').T, norm='ortho').T

def idct2(x):
    return idct(idct(x, norm='ortho').T, norm='ortho').T

def compute_spatial_from_jpeg(jpeg_im,c_quant):
    """
    Compute the 8x8 DCT transform of the jpeg representation
    """
    w,h = jpeg_im.shape
    spatial_im = np.zeros((w,h))
    for bind_i in range(int(w//8)):
        for bind_j in range(int(h//8)):
            block = idct2(jpeg_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]*(c_quant))+128
            #block[block>255]=255
            #block[block<0]=0
            spatial_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = block
    spatial_im = spatial_im.astype(np.float32)       
    return(spatial_im)

#0. Tools

def IndexDCT(mode): #For each group ('mode'), returns a list which contains the relevant indices of the DCT plane
    out = []
    for a in range (0,6):
        for b in range (0,6):
            if a+b>0:
                if (mode =='h' or mode=='ih' or mode=='ix'):
                    if a+b<6:
                        out.append([a,b])
                elif (mode=='d1' or mode=='id' or mode=='im'):
                    if b>=a:
                        if a+b<6:
                            out.append([a,b])
                elif (mode=='d2' or mode=='x' or mode=='od2'):
                    if b>a:
                        if a+b<6:
                            out.append([a,b])
                elif (mode=='oh'):
                    if a+b<5:
                        out.append([a,b])
                elif (mode=='km'):
                    if a>0:
                        if a+b<6:
                            out.append([a,b])
                elif (mode=='od1'):
                    if b>=a:                 
                        if a+b<5:
                            out.append([a,b])
                else:
                    print('Mode error')  

    return out

def SignSym(res,T): #Sign Symmetry used for co-occurrences matrices based on differences of absolute values
    res_sym = res[:,:] + res[::-1,::-1]
    res_sym = res_sym.flatten()
    idx = int(0.5*(2*T+1)*(2*T+1)+0.5)
    out = res_sym[:idx]
    return out

# 1. DCT-mode specific components
# We have to distinguish whether the co-occurrence matrices are based on 
# absolute values or on differences of absolute values.
# In each case, we have to distinguish intra-block ou inter-block relationships.

# 1.1 Based on absolute values

# 1.1.1 Intra-block

def IntraABS1(res,mode,dx,dy,T): #Groups 1,2,3,5,6
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out1 = []
    out2 = []
    tab = IndexDCT(mode)

    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []
        
        for i in range (0,tx//8):
            for j in range (0,ty//8):
                x_a1.append(res[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
                x_a2.append(res[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a1 = np.asarray(x_a1)
        y_a1 = np.asarray(y_a1)
        count1 = np.bincount(x_a1 + (T+1)*y_a1,minlength=(T+1)*(T+1))
        out1 = np.append(out1,count1)
        
        
        x_a2 = np.asarray(x_a2)
        y_a2 = np.asarray(y_a2)
        count2 = np.bincount(x_a2 + (T+1)*y_a2,minlength=(T+1)*(T+1))
        out2 = np.append(out2,count2)
    
    out = out1 + out2
    return out

def IntraABS2(res,mode,T): #Group 4
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out1 = []
    out2 = []
    tab = IndexDCT(mode)
    
    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []
        
        dx = tab[z][1] - tab[z][0]
        dy = -dx
        
        for i in range (0,tx/8):
            for j in range (0,ty/8):
                x_a1.append(res[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
                x_a2.append(res[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a1 = np.asarray(x_a1)
        y_a1 = np.asarray(y_a1)
        count1 = np.bincount(x_a1 + (T+1)*y_a1,minlength=(T+1)*(T+1))
        out1 = np.append(out1,count1)
                
        x_a2 = np.asarray(x_a2)
        y_a2 = np.asarray(y_a2)
        count2 = np.bincount(x_a2 + (T+1)*y_a2,minlength=(T+1)*(T+1))
        out2 = np.append(out2,count2)
    
    out = out1 + out2
    return out

# 1.1.2. Inter-block

def InterABS1(res,mode,dx,dy,T): #Groups 7,8,9
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out1 = []
    out2 = []
    tab = IndexDCT(mode)
    
    range_i1 = []
    range_j1 = []
    range_i2 = []
    range_j2 = []
    
    if dx>=0:
        range_i1 = range(0,tx/8-dx)
        range_j2 = range(0,ty/8-dx)
    else:
        range_i1 = range(-dx,tx/8)
        range_j2 = range(-dx,ty/8) 
        
    if dy>=0:
        range_j1 = range(0,ty/8-dy)
        range_i2 = range(0,tx/8-dy)
    else:
        range_j1 = range(-dy,ty/8)
        range_i2 = range(-dy,tx/8)

    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []

        for i in range_i1:
            for j in range_j1:
                x_a1.append(res[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res[8*(i+dx)+tab[z][0],8*(j+dy)+tab[z][1]])
                
        x_a1 = np.asarray(x_a1)
        y_a1 = np.asarray(y_a1)
        count1 = np.bincount(x_a1 + (T+1)*y_a1,minlength=(T+1)*(T+1))
        out1 = np.append(out1,count1)
        
        for i in range_i2:
            for j in range_j2:
                x_a2.append(res[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res[8*(i+dy)+tab[z][1],8*(j+dx)+tab[z][0]])
                
        x_a2 = np.asarray(x_a2)
        y_a2 = np.asarray(y_a2)
        count2 = np.bincount(x_a2 + (T+1)*y_a2,minlength=(T+1)*(T+1))
        out2 = np.append(out2,count2)
    
    out = out1 + out2
    return out

def InterABS2(res,mode,T): #Group 10
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out1 = []
    out2 = []
    tab = IndexDCT(mode)
    
    range_i1 = range(0,tx/8)
    range_j1 = range(0,ty/8-1)
    range_i2 = range(0,tx/8-1)
    range_j2 = range(0,ty/8)
    
    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []
        
        dx = tab[z][1] - tab[z][0]
        dy = -dx + 8
    
        for i in range_i1:
            for j in range_j1:
                x_a1.append(res[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
        x_a1 = np.asarray(x_a1)
        y_a1 = np.asarray(y_a1)
        count1 = np.bincount(x_a1 + (T+1)*y_a1,minlength=(T+1)*(T+1))
        out1 = np.append(out1,count1)

        for i in range_i2:
            for j in range_j2:
                x_a2.append(res[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a2 = np.asarray(x_a2)
        y_a2 = np.asarray(y_a2)
        count2 = np.bincount(x_a2 + (T+1)*y_a2,minlength=(T+1)*(T+1))
        out2 = np.append(out2,count2)

    out = out1 + out2
    return out
    
# 1.2. Based on differences of absolute values
    
# 1.2.1. Intra-block

def IntraOTH1(res1,res2,mode,dx,dy,T): #Groups 1,2,3,5,6
    
    tx1, ty1 = res1.shape
    tx2, ty2 = res2.shape
    
    res1[res1>T] = T
    res2[res2>T] = T
    res1[res1<-T] = -T
    res2[res2<-T] = -T  

    out = []
    tab = IndexDCT(mode)

    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []
        
        for i in range (0,tx1/8):
            for j in range (0,ty1/8):
                x_a1.append(res1[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res1[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
        x_a1 = np.asarray(x_a1) + T
        y_a1 = np.asarray(y_a1) + T
        count1 = np.bincount(x_a1 + (2*T+1)*y_a1,minlength=(2*T+1)*(2*T+1))

        for i in range (0,tx2/8):
            for j in range (0,ty2/8):
                x_a2.append(res2[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res2[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a2 = np.asarray(x_a2) + T
        y_a2 = np.asarray(y_a2) + T
        count2 = np.bincount(x_a2 + (2*T+1)*y_a2,minlength=(2*T+1)*(2*T+1))
        
        tmp = np.reshape(count1+count2,(2*T+1,2*T+1))
        tmp = SignSym(tmp,T)
        out = np.append(out,tmp)
    
    return out

def IntraOTH2(res1,res2,mode,T): #Group 4
    
    tx1, ty1 = res1.shape
    tx2, ty2 = res2.shape
    
    res1[res1>T] = T
    res2[res2>T] = T  
    res1[res1<-T] = -T
    res2[res2<-T] = -T  

    out = []
    tab = IndexDCT(mode)

    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []
        
        dx = tab[z][1] - tab[z][0]
        dy = -dx
        
        for i in range (0,tx1/8):
            for j in range (0,ty1/8):
                x_a1.append(res1[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res1[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
        x_a1 = np.asarray(x_a1) + T
        y_a1 = np.asarray(y_a1) + T
        count1 = np.bincount(x_a1 + (2*T+1)*y_a1,minlength=(2*T+1)*(2*T+1))

        for i in range (0,tx2/8):
            for j in range (0,ty2/8):
                x_a2.append(res2[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res2[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a2 = np.asarray(x_a2) + T
        y_a2 = np.asarray(y_a2) + T
        count2 = np.bincount(x_a2 + (2*T+1)*y_a2,minlength=(2*T+1)*(2*T+1))
        
        tmp = np.reshape(count1+count2,(2*T+1,2*T+1))
        tmp = SignSym(tmp,T)
        out = np.append(out,tmp)

    return out

def InterOTH1(res1,res2,mode,dx,dy,T): #Groups 7,8,9
    
    tx1, ty1 = res1.shape
    tx2, ty2 = res2.shape
    
    res1[res1>T] = T
    res2[res2>T] = T
    res1[res1<-T] = -T
    res2[res2<-T] = -T  

    out = []
    tab = IndexDCT(mode)
    
    range_i1 = []
    range_j1 = []
    range_i2 = []
    range_j2 = []
    
    if dx>=0:
        range_i1 = range(0,tx1/8-dx)
        range_j2 = range(0,ty2/8-dx)
    else:
        range_i1 = range(-dx,tx1/8)
        range_j2 = range(-dx,ty2/8) 
        
    if dy>=0:
        range_j1 = range(0,ty1/8-dy)
        range_i2 = range(0,tx2/8-dy)
    else:
        range_j1 = range(-dy,ty1/8)
        range_i2 = range(-dy,tx2/8)

    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []

        for i in range_i1:
            for j in range_j1:
                x_a1.append(res1[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res1[8*(i+dx)+tab[z][0],8*(j+dy)+tab[z][1]])
                
        x_a1 = np.asarray(x_a1) + T
        y_a1 = np.asarray(y_a1) + T
        count1 = np.bincount(x_a1 + (2*T+1)*y_a1,minlength=(2*T+1)*(2*T+1))

        for i in range_i2:
            for j in range_j2:
                x_a2.append(res2[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res2[8*(i+dy)+tab[z][1],8*(j+dx)+tab[z][0]])
                
        x_a2 = np.asarray(x_a2) + T
        y_a2 = np.asarray(y_a2) + T
        count2 = np.bincount(x_a2 + (2*T+1)*y_a2,minlength=(2*T+1)*(2*T+1))
        
        tmp = np.reshape(count1+count2,(2*T+1,2*T+1))
        tmp = SignSym(tmp,T)
        out = np.append(out,tmp)

    return out

def InterOTH2(res1,res2,mode,T): #Group 10
    
    tx1, ty1 = res1.shape
    tx2, ty2 = res2.shape
    
    res1[res1>T] = T
    res2[res2>T] = T
    res1[res1<-T] = -T
    res2[res2<-T] = -T  

    out = []
    tab = IndexDCT(mode)
    
    range_i1 = range(0,tx1/8)
    range_j1 = range(0,ty1/8-1)
    range_i2 = range(0,tx2/8-1)
    range_j2 = range(0,ty2/8)
    
    for z in range(0,len(tab)):
        x_a1 = []
        y_a1 = []
        x_a2 = []
        y_a2 = []

        dx = tab[z][1] - tab[z][0]
        dy = -dx + 8
    
        for i in range_i1:
            for j in range_j1:
                x_a1.append(res1[8*i+tab[z][0],8*j+tab[z][1]])
                y_a1.append(res1[8*i+tab[z][0]+dx,8*j+tab[z][1]+dy])
                
        x_a1 = np.asarray(x_a1) + T
        y_a1 = np.asarray(y_a1) + T
        count1 = np.bincount(x_a1 + (2*T+1)*y_a1,minlength=(2*T+1)*(2*T+1))   

        for i in range_i2:
            for j in range_j2:
                x_a2.append(res2[8*i+tab[z][1],8*j+tab[z][0]])
                y_a2.append(res2[8*i+tab[z][1]+dy,8*j+tab[z][0]+dx])
                
        x_a2 = np.asarray(x_a2) + T
        y_a2 = np.asarray(y_a2) + T
        count2 = np.bincount(x_a2 + (2*T+1)*y_a2,minlength=(2*T+1)*(2*T+1))
        
        tmp = np.reshape(count1+count2,(2*T+1,2*T+1))
        tmp = SignSym(tmp,T)
        out = np.append(out,tmp)

    return out

# 2. Integral components
# Same distinction as before
    
# 2.1. Based on absolute values
    
# 2.1.1 Intra-block (Group 1 - Partial)

def Extract_Integral_Intra_ABS(res,dx,dy,T):
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out = np.zeros((T+1)*(T+1))
    
    for x in range (0,8):
        for y in range (0,8):    
            x_a = []
            y_a = []
            
            for i in range (0,tx/8):
                for j in range (0,ty/8):
                    if(8*i+x+dx<tx and 8*j+y+dy<ty):
                        x_a.append(res[8*i+x,8*j+y])
                        y_a.append(res[8*i+x+dx,8*j+y+dy])
             
            x_a = np.asarray(x_a)
            y_a = np.asarray(y_a)
            count = np.bincount(x_a + (T+1)*y_a,minlength=(T+1)*(T+1))
            
            out = out + count
    
    return out

# 2.1.2. Inter-block (Group 1 - Partial)

def Extract_Integral_Inter_ABS(res,dx,dy,T):
    
    tx, ty = res.shape
    res[res>T] = T    
    
    out = np.zeros((T+1)*(T+1))

    range_i = []
    range_j = []
    
    if dx>=0:
        range_i = range(0,tx/8-dx)
    else:
        range_i = range(-dx,tx/8)
        
    if dy>=0:
        range_j = range(0,ty/8-dy)
    else:
        range_j = range(-dy,ty/8)

    for x in range (0,8):
        for y in range (0,8):
            x_a = []
            y_a = []
        
            for i in range_i:
                for j in range_j:
                    x_a.append(res[8*i+x,8*j+y])
                    y_a.append(res[8*(i+dx)+x,8*(j+dy)+y])
                    
            x_a = np.asarray(x_a)
            y_a = np.asarray(y_a)
            count = np.bincount(x_a + (T+1)*y_a,minlength=(T+1)*(T+1))
            
            out = out + count

    return out

# 2.2. Based on differences of absolute values

# 2.2.1. Intra-block (Group 2)

def Extract_Integral_Intra_OTH(res,dx,dy,T):
    
    tx, ty = res.shape
    res[res>T] = T
    res[res<-T] = -T
    
    out = np.zeros(int(0.5*(2*T+1)*(2*T+1)+0.5))

    for x in range (0,8):
        for y in range (0,8):    
            x_a = []
            y_a = []
            
            for i in range (0,tx/8):
                for j in range (0,ty/8):
                    if(8*i+x+dx<tx and 8*j+y+dy<ty):
                        x_a.append(res[8*i+x,8*j+y])
                        y_a.append(res[8*i+x+dx,8*j+y+dy])
             
            x_a = np.asarray(x_a) + T
            y_a = np.asarray(y_a) + T
            count = np.bincount(x_a + (2*T+1)*y_a,minlength=(2*T+1)*(2*T+1))
            count = np.reshape(count,(2*T+1,2*T+1))
            count_sym = SignSym(count,T)            
            
            out = out + count_sym

    return out

# 2.2.2. Inter-block (Group 3)

def Extract_Integral_Inter_OTH(res,dx,dy,T):
    
    tx, ty = res.shape
    res[res>T] = T    
    res[res<-T] = -T
    
    out = np.zeros(int(0.5*(2*T+1)*(2*T+1)+0.5))

    range_i = []
    range_j = []
    
    if dx>=0:
        range_i = range(0,tx/8-dx)
    else:
        range_i = range(-dx,tx/8)
        
    if dy>=0:
        range_j = range(0,ty/8-dy)
    else:
        range_j = range(-dy,ty/8)

    for x in range (0,8):
        for y in range (0,8):
            x_a = []
            y_a = []
        
            for i in range_i:
                for j in range_j:
                    x_a.append(res[8*i+x,8*j+y])
                    y_a.append(res[8*(i+dx)+x,8*(j+dy)+y])
                    
            x_a = np.asarray(x_a) + T
            y_a = np.asarray(y_a) + T
            count = np.bincount(x_a + (2*T+1)*y_a,minlength=(2*T+1)*(2*T+1))
            count = np.reshape(count,(2*T+1,2*T+1))
            count_sym = SignSym(count,T)            
            
            out = out + count_sym

    return out

# 3. Extracting all submodels
# Given an image (array), extract all submodels, resulting in a feature vector
# of dimensionality 11,255

def ExtractTotal(X):

    M,N = np.shape(X)

    # Shapes adjusted to the closest multiple of 8
    M8 = int(np.floor(M/8)*8)
    N8 = int(np.floor(N/8)*8)
    
    #Residuals X_abs, X_h, X_v, X_d, X_ih, X_iv
    X_abs = np.abs(X)
    
    X_h = X_abs[:,0:N-1] - X_abs[:,1:N]
    X_v = X_abs[0:M-1,:] - X_abs[1:M,:]
    X_d = X_abs[0:M-1,0:N-1] - X_abs[1:M,1:N]
    
    X_abs = X_abs[0:M8,0:N8] #Bords
    X_h = X_h[0:M8,0:N8] #Bords
    X_v = X_h[0:M8,0:N8] #Bords
    X_d = X_h[0:M8,0:N8] #Bords
    
    X_ih = X_abs[:M8,0:N8-8] - X_abs[:M8,8:N8]
    X_iv = X_abs[0:M8-8,:N8] - X_abs[8:M8,:N8]
    
    feat = {} #Features storing
    
    feat['Abs.h'] = IntraABS1(X_abs,'h',0,1,3)
    feat['Abs.d1'] = IntraABS1(X_abs,'d1',1,1,3)
    feat['Abs.d2'] = IntraABS1(X_abs,'d2',1,-1,3)
    feat['Abs.oh'] = IntraABS1(X_abs,'oh',0,2,3)
    feat['Abs.x'] = IntraABS2(X_abs,'x',3)
    feat['Abs.od1'] = IntraABS1(X_abs,'od1',2,2,3)
    feat['Abs.od2'] = IntraABS1(X_abs,'od2',2,-2,3)
    feat['Abs.km'] = IntraABS1(X_abs,'km',-1,2,3)
    feat['Abs.ih'] = InterABS1(X_abs,'ih',0,1,3)
    feat['Abs.id'] = InterABS1(X_abs,'id',1,1,3)
    feat['Abs.im'] = InterABS1(X_abs,'im',-1,1,3)
    feat['Abs.ix'] = InterABS2(X_abs,'ix',3)
    
    feat['Hor.h'] = IntraOTH1(X_h,X_v,'h',0,1,2)
    feat['Hor.d1'] = IntraOTH1(X_h,X_v,'d1',1,1,2)
    feat['Hor.d2'] = IntraOTH1(X_h,X_v,'d2',1,-1,2)
    feat['Hor.oh'] = IntraOTH1(X_h,X_v,'oh',0,2,2)
    feat['Hor.x'] = IntraOTH2(X_h,X_v,'x',2)
    feat['Hor.od1'] = IntraOTH1(X_h,X_v,'od1',2,2,2)
    feat['Hor.od2'] = IntraOTH1(X_h,X_v,'od2',2,-2,2)
    feat['Hor.km'] = IntraOTH1(X_h,X_v,'km',-1,2,2)
    feat['Hor.ih'] = InterOTH1(X_h,X_v,'ih',0,1,2)
    feat['Hor.id'] = InterOTH1(X_h,X_v,'id',1,1,2)
    feat['Hor.im'] = InterOTH1(X_h,X_v,'im',-1,1,2)
    feat['Hor.ix'] = InterOTH2(X_h,X_v,'ix',2)
    
    feat['Diag.h'] = IntraOTH1(X_d,X_d,'h',0,1,2)
    feat['Diag.d1'] = IntraOTH1(X_d,X_d,'d1',1,1,2)
    feat['Diag.d2'] = IntraOTH1(X_d,X_d,'d2',1,-1,2)
    feat['Diag.oh'] = IntraOTH1(X_d,X_d,'oh',0,2,2)
    feat['Diag.x'] = IntraOTH2(X_d,X_d,'x',2)
    feat['Diag.od1'] = IntraOTH1(X_d,X_d,'od1',2,2,2)
    feat['Diag.od2'] = IntraOTH1(X_d,X_d,'od2',2,-2,2)
    feat['Diag.km'] = IntraOTH1(X_d,X_d,'km',-1,2,2)
    feat['Diag.ih'] = InterOTH1(X_d,X_d,'ih',0,1,2)
    feat['Diag.id'] = InterOTH1(X_d,X_d,'id',1,1,2)
    feat['Diag.im'] = InterOTH1(X_d,X_d,'im',-1,1,2)
    feat['Diag.ix'] = InterOTH2(X_d,X_d,'ix',2)
    
    feat['Ihor.h'] = IntraOTH1(X_ih,X_iv,'h',0,1,2)
    feat['Ihor.d1'] = IntraOTH1(X_ih,X_iv,'d1',1,1,2)
    feat['Ihor.d2'] = IntraOTH1(X_ih,X_iv,'d2',1,-1,2)
    feat['Ihor.oh'] = IntraOTH1(X_ih,X_iv,'oh',0,2,2)
    feat['Ihor.x'] = IntraOTH2(X_ih,X_iv,'x',2)
    feat['Ihor.od1'] = IntraOTH1(X_ih,X_iv,'od1',2,2,2)
    feat['Ihor.od2'] = IntraOTH1(X_ih,X_iv,'od2',2,-2,2)
    feat['Ihor.km'] = IntraOTH1(X_ih,X_iv,'km',-1,2,2)
    feat['Ihor.ih'] = InterOTH1(X_ih,X_iv,'ih',0,1,2)
    feat['Ihor.id'] = InterOTH1(X_ih,X_iv,'id',1,1,2)
    feat['Ihor.im'] = InterOTH1(X_ih,X_iv,'im',-1,1,2)
    feat['Ihor.ix'] = InterOTH2(X_ih,X_iv,'ix',2)
    
    feat['Int.Abs.1'] = Extract_Integral_Intra_ABS(X_abs,0,1,5)
    feat['Int.Abs.2'] = Extract_Integral_Intra_ABS(X_abs,1,1,5)
    feat['Int.Abs.3'] = Extract_Integral_Intra_ABS(X_abs,1,-1,5)
    feat['Int.Abs.4'] = Extract_Integral_Inter_ABS(X_abs,0,1,5)
    feat['Int.Abs.5'] = Extract_Integral_Inter_ABS(X_abs,1,1,5)
    
    feat['Int.h.1'] = Extract_Integral_Intra_OTH(X_h,0,1,5)
    feat['Int.h.2'] = Extract_Integral_Intra_OTH(X_h,1,0,5)
    feat['Int.h.3'] = Extract_Integral_Intra_OTH(X_h,1,1,5)
    feat['Int.h.4'] = Extract_Integral_Intra_OTH(X_h,1,-1,5)
    feat['Int.h.5'] = Extract_Integral_Inter_OTH(X_h,0,1,5)
    feat['Int.h.6'] = Extract_Integral_Inter_OTH(X_h,1,0,5)
    feat['Int.h.7'] = Extract_Integral_Inter_OTH(X_h,1,1,5)
    feat['Int.h.8'] = Extract_Integral_Inter_OTH(X_h,1,-1,5)
    
    feat['Int.v.1'] = Extract_Integral_Intra_OTH(X_v,0,1,5)
    feat['Int.v.2'] = Extract_Integral_Intra_OTH(X_v,1,0,5)
    feat['Int.v.3'] = Extract_Integral_Intra_OTH(X_v,1,1,5)
    feat['Int.v.4'] = Extract_Integral_Intra_OTH(X_v,1,-1,5)
    feat['Int.v.5'] = Extract_Integral_Inter_OTH(X_v,0,1,5)
    feat['Int.v.6'] = Extract_Integral_Inter_OTH(X_v,1,0,5)
    feat['Int.v.7'] = Extract_Integral_Inter_OTH(X_v,1,1,5)
    feat['Int.v.8'] = Extract_Integral_Inter_OTH(X_v,1,-1,5)
    
    feat['Int.d.1'] = Extract_Integral_Intra_OTH(X_d,0,1,5)
    feat['Int.d.2'] = Extract_Integral_Intra_OTH(X_d,1,0,5)
    feat['Int.d.3'] = Extract_Integral_Intra_OTH(X_d,1,1,5)
    feat['Int.d.4'] = Extract_Integral_Intra_OTH(X_d,1,-1,5)
    feat['Int.d.5'] = Extract_Integral_Inter_OTH(X_d,0,1,5)
    feat['Int.d.6'] = Extract_Integral_Inter_OTH(X_d,1,0,5)
    feat['Int.d.7'] = Extract_Integral_Inter_OTH(X_d,1,1,5)
    feat['Int.d.8'] = Extract_Integral_Inter_OTH(X_d,1,-1,5)
    
    feat['Int.ih.1'] = Extract_Integral_Intra_OTH(X_ih,0,1,5)
    feat['Int.ih.2'] = Extract_Integral_Intra_OTH(X_ih,1,0,5)
    feat['Int.ih.3'] = Extract_Integral_Intra_OTH(X_ih,1,1,5)
    feat['Int.ih.4'] = Extract_Integral_Intra_OTH(X_ih,1,-1,5)
    feat['Int.ih.5'] = Extract_Integral_Inter_OTH(X_ih,0,1,5)
    feat['Int.ih.6'] = Extract_Integral_Inter_OTH(X_ih,1,0,5)
    feat['Int.ih.7'] = Extract_Integral_Inter_OTH(X_ih,1,1,5)
    feat['Int.ih.8'] = Extract_Integral_Inter_OTH(X_ih,1,-1,5)
    
    feat['Int.iv.1'] = Extract_Integral_Intra_OTH(X_iv,0,1,5)
    feat['Int.iv.2'] = Extract_Integral_Intra_OTH(X_iv,1,0,5)
    feat['Int.iv.3'] = Extract_Integral_Intra_OTH(X_iv,1,1,5)
    feat['Int.iv.4'] = Extract_Integral_Intra_OTH(X_iv,1,-1,5)
    feat['Int.iv.5'] = Extract_Integral_Inter_OTH(X_iv,0,1,5)
    feat['Int.iv.6'] = Extract_Integral_Inter_OTH(X_iv,1,0,5)
    feat['Int.iv.7'] = Extract_Integral_Inter_OTH(X_iv,1,1,5)
    feat['Int.iv.8'] = Extract_Integral_Inter_OTH(X_iv,1,-1,5)
    
    out = []
    for k in feat.keys():
        out = np.append(out,feat[k])
    
    return out

# 4. Whole process
# Given an image (provide a path), returns the CC-JRM feature vector
# Its dimensionality is 22,510 :
# The extractor is applied to the image and its calibration (2 x 11,255)

def JRM_Process(path, QF, folder_model, color):
    pardir = os.path.dirname(path) + '/'
    basename = os.path.basename(path)    
    
    c_quant = np.asarray(np.load(folder_model+'c_quant_'+str(QF)+'.npy'),dtype=np.int64)
    #im1 = jpeg(path)
    #X1 = im1.coef_arrays[0]
    X1 = np.asarray(np.load(path),dtype=np.int64)
    o1 = ExtractTotal(X1)
    
    print 'shape o1:', np.shape(o1)    

    #im_sp = imread(path)
    im_sp = np.round(compute_spatial_from_jpeg(X1, c_quant),0).astype(np.uint8)
    x,y = im_sp.shape
    im_sp = im_sp[4:x-4,4:y-4]
    im = Image.fromarray(im_sp)
    im.save(pardir + 'tmp' + basename, 'JPEG', quality=QF)
    
    im2 = jpeg(pardir + 'tmp' + basename)
    X2 = im2.coef_arrays[0]
    o2 = ExtractTotal(X2)

    print 'shape o2:', np.shape(o2)    
    
    os.remove(pardir + 'tmp' + basename)
    out = np.hstack((o1,o2))

    print 'shape out:', np.shape(out)    
    
    return out

# 5. Main (for multiprocessing)

def main_JRM(image_dir,file_out):
    global QF #Quality factor for calibration
    QF = 75

    list_im = sorted(glob.glob(image_dir +'/*.jpg'))

    nbCores = multiprocessing.cpu_count()

    tic = time.time()
    pool = Pool(nbCores)
    results = pool.map(JRM_Process, list_im)
    pool.close()
    pool.join()
    toc = time.time()
    print "Processing time: ", (toc-tic), " seconds - ",(toc-tic)/len(list_im), "sec/im"
    
    feat = h5py.File(file_out,'w')
    feat.create_dataset('dataset_1', data=results)
    feat.close()

    return 1




"""
Implemention of JRM extraction class.
Theses features can be generated from a greyscale jpeg image.

"""

from extractor import Extractor as ParentExtractor


class Extractor(ParentExtractor) :
    """
    Extract caracteristics from an image using GFR algorithm.

    self.conf should specify :
        - imgs.QF : quality factor of cover images (corresponding to jpeg library, and most open-source softwares, such as imagemagic)
    """

    file_type = 'jpg'

    def run(self, file) :
        result = JRM_Process(file, self.conf.QF, self.conf.folder_model, color=False)
        return np.array(result)
