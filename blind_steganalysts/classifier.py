import os, sys
import numpy as np
import pickle

# Dirty import of parent module
#sys.path.append(os.path.dirname(__file__) + '/..')
from tools import msg


class Classifier() :
    """
    An abstract class containing methods an classifier should have.
    """

    def __init__(self, conf) :
        """
        Creation of a new classifier that needs to train.
        """
        self.trained = False  # becomes true if the classifier is trained
        self.conf = conf

    def train(self, cover, stego) :
        """
        Trains the classifier. Supposes cover and stego have the same dimention
        """
        msg('Started training the classifier on %d images' % np.size(cover, axis=0))
        self.trained = True


    def guess_stego(self, features) :
        """
        Guess if given features represents a stego image.
        If features represents several images, an array of answers is returned.
        """
        if not self.trained :
            raise NameError('Using an untrained classifier.')

    # def test(self, cover, stego) :
    #     """
    #     Tests the trained classifier on entries, return (success_cover, success_stego)
    #     """
    #     msg('Testing the classifier', type='status')
    #     guessed_cover = self.guess_stego(cover)
    #     guessed_stego = self.guess_stego(stego)
    #     ok_cover = np.sum(1-guessed_cover)
    #     ok_stego = np.sum(guessed_stego)
    #     return ok_cover, ok_stego


    # Je pense que cette fonction ne sert a rien (bas)
    # def test(self, cover, stego) :
    #     """
    #     Tests the trained classifier on entries, return (success_cover, success_stego)
    #     """
    #     msg('Testing the classifier', type='status')
    #     PE_min , PE_true = self.test(cover,stego)
    #     return PE_min , PE_true


    def save(self, file) :
        """
        Saves the classifier in a file.
        """
        with open(file, 'wb') as fout :
            pickle.dump(self, fout)
