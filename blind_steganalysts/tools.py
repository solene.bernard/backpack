import time
from sys import stdout


def msg(msg, type='info', show_time=True, spacing=10, format_only=False) :
    """
    Prompts a message in the standart output.

    :param msg:         content of the message
    :param type:        type of the message (eg : error, info, warning)
    :param show_time:   whether time should be prompted or not
    :param spacing:     width of the flag column
    :param format_only: wether the message sould be printed or not

    :return:            builded message
    """
    spaces = (spacing - len(type) - 2) * ' '
    head = '[%s]%s ' % (type, spaces)
    if show_time :
        head = time.strftime('%d/%m %H:%M:%S ') + head

    msg = head + msg  # adds head to first line
    msg = msg.replace('\n', '\n' + head)  # adds head to other lines

    if not format_only :
        print(msg)
        stdout.flush()
    return msg
