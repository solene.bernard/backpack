from math import sqrt, pi
import numpy as np
from scipy.ndimage.filters import convolve
from scipy.fftpack import idct
from time import time

import os, sys
# CURDIR = os.path.dirname(os.path.realpath(__file__))
# JPEGDIR = os.path.join(CURDIR, os.pardir, os.pardir, os.pardir)
# if JPEGDIR not in sys.path:
#     sys.path.append(JPEGDIR)

# from jpeg import jpeg
#from ....jpeg import jpeg

def unique_rows(a):
    a = np.ascontiguousarray(a)
    unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))


class DCTR_process:
    """
    This class represents PHARM algorithm
    """

    def __init__(self, image_path, qual):
        """
        Store the image path and the quality factor
        """
        try:
            self.imagepath = image_path
            self.QF = int(qual)
        except (ValueError):
            "Please enter an integer as quality factor"
            raise

    def idct2(self, x):
        return idct(idct(x.astype('float'), norm='ortho').T, norm='ortho').T

    def compute_spatial_domain(self, dct_coeffs, c_quant):
        """
        Decompress the JPEG Image in floats
        """
        w,h = dct_coeffs.shape
        sp_im = np.zeros((w,h))
        for bind_i in range(w//8):
            for bind_j in range(h//8):
                dct_bloc = dct_coeffs[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]
                sp_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = self.idct2(dct_bloc*c_quant)
        return sp_im+128

    # Computation of the histrogram
    def Hist(self, res):
        nbins = self.T+1
        M,N = res.shape
        flatcount = np.zeros(nbins)
        flatcount = np.bincount(res.flatten(1), minlength=nbins)
        out = flatcount.T

        return out

    def compute_DCTR(self, color=False) :
        """
        Computes features of the image.

        :param color:   specify wether the image contains color or not
        """
        # if os.path.getsize(self.imagepath) == 0 :
        #     raise Exception('Empty file : ' + self.imagepath)

        #img = jpeg(self.imagepath, verbosity=0)
        img = np.load(self.imagepath)
        if not color :
            # Force usage of only a layer
            #dct_coeff = img.coef_arrays[0]
            dct_coeff=img
            #c_quant = img.quant_tables[0]
            c_quant = np.load('./c_quant_'+str(self.QF)+'.npy')
            return self.compute_DCTR_layer(c_quant, dct_coeff)
        else :
            # Uses the three layers
            if len(img.quant_tables) < 3 :
                raise Exception('This image seems to be a greyscale image : ' + self.imagepath)
            else :
                # If there is less that 3 DCT coefficients specified, it means that they are the sames.
                # ie. same for both chrominance or same for all layers.
                for i in range(1, len(img.quant_tables)) :
                    if img.quant_tables[i] is None :
                        # don't have to copy it as it won't be modified
                        img.quant_tables[i] = img.quant_tables[i-1]

                # concatenates the 3 features
                feats = [self.compute_DCTR_layer(img.quant_tables[i], img.coef_arrays[i]) for i in range(3)]
                return np.concatenate(feats)

    def compute_DCTR_layer(self, c_quant, dct_coeffs) :
        """
        Computes features for one layer.
        For example, for a greyscale image it will be the hole features.
        """
        self.T = 4
        total_convol_time = 0
        is_empty = False
        # Set parameters
        # Number of histogram bins
        # if os.path.getsize(self.imagepath) != 0:
        #     I_STRUCT = jpeg(self.imagepath, verbosity=0)
        #     is_empty = False

        # compute quantization step based on quality factor
        if self.QF < 50 :
            q = min(8 * (50.0 / self.QF), 100)
        else:
            q = max(8 * (2 - (self.QF/50.0)), 0.2)
            # q = max(8 * (2 - (75/50.0)), 0.2)

        # Prepare for computing DCT bases
        k = np.array([0, 1, 2, 3, 4, 5, 6, 7])
        m = np.array([0, 1, 2, 3, 4, 5, 6, 7])
        [K,M] = np.meshgrid(k,m)
        A=0.5*np.cos(((2.*K+1)*M*pi)/16)
        A[0,:]=A[0,:]/sqrt(2)
        A = A.T

        # Compute DCTR locations to be merged
        mergedCoordinates = np.empty((25), dtype=object)
        for i in range(5):
            for j in range(5):
                coordinates = np.array([[i,j], [i,8-j], [8-i,j], [8-i,8-j]])
                coordinates = [row for row in coordinates if row[0] < 8 and row[1] < 8]
                coordinates = unique_rows(coordinates)
                mergedCoordinates[i*5 + j] = coordinates;
        #end = time()
        #print(str(end-start) + " : OK init")

        # Decompress to spatial domain
        # if not is_empty:
        #     dct_coeffs= I_STRUCT.coef_arrays[0]
        #     c_quant = I_STRUCT.quant_tables[0]
        I_spatial = self.compute_spatial_domain(dct_coeffs, c_quant)

        # Warning: Modfied for NS!
        #I_spatial = I_spatial[:-8,:-8]        

            #end = time()
            #print(str(end-start) + " : OK spatial")

        # Compute features
        modeFeaDim = len(mergedCoordinates)*(self.T+1)
        F = np.zeros(64*modeFeaDim) # single = 64 bits
        if is_empty:
            return F
        for mode_r in range(8):
            for mode_c in range(8):
                modeIndex = mode_r*8 + mode_c
                # Get DCT base for current mode
                DCTbase = A[:,mode_r].reshape(1,-1)*A[:,mode_c].reshape(-1,1) # B(mode_r, mode_c)

                # Obtain DCT residual R by convolution between image in spatial domain and the current DCT base
                start_convolve = time()
                R = convolve(I_spatial-128, DCTbase)
                R = R[np.ix_(list(range(3,R.shape[0]-4)), list(range(3,R.shape[0]-4)))]
                end_convolve = time()
                total_convol_time = total_convol_time + end_convolve - start_convolve

                # Quantization, rounding, absolute value, thresholding
                R = np.absolute((R / q).round())
                R = R.astype(np.int16)
                idx = R[:,:] > self.T
                R[idx] = self.T

                # Core of the feature extraction
                for merged_index in range (len(mergedCoordinates)): # 25
                    f_merged = np.zeros(self.T+1)
                    for coord_index in range(len(mergedCoordinates[merged_index])): # 1, 2, or 4
                        r_shift = mergedCoordinates[merged_index][coord_index][0] # a
                        c_shift = mergedCoordinates[merged_index][coord_index][1] # b
                        R_sub = R[np.ix_(list(range(r_shift,len(R),8)), list(range(c_shift,len(R[0]),8)))] # sous ensemble de R de coord rel (a, b)
                        f_merged = f_merged + self.Hist(R_sub) # ajout des bins
                    F_index_from = modeIndex*modeFeaDim+merged_index*(self.T+1)
                    F_index_to = modeIndex*modeFeaDim+merged_index*(self.T+1)+self.T+1
                    F[F_index_from:F_index_to] = f_merged / sum(f_merged)
        #end = time()
        #print(str(end-start) + " : OK all")
        #print("total convolve time : " + str(total_convol_time))
        return F

from extractor import Extractor as ParentExtractor


class Extractor(ParentExtractor) :
    """
    Extract caracteristics from an image using DCTR algorithm.

    self.conf should specify :
        - imgs.QF : quality factor of cover images (corresponding to jpeg library, and most open-source softwares, such as imagemagic)
        - imgs.color : specify wether image is color or not
    """

    #file_type = 'jpg'

    def run(self, file) :
        engine = DCTR_process(file, self.conf.QF)
        result = engine.compute_DCTR(color=self.conf.color)
        return np.array(result)




# Main function
if __name__ == '__main__':
    dctr = DCTR_process('image/1.jpg',75)
    t1 = time()
    res = dctr.compute_DCTR()
    t2 = time()
    print("execution time:", t2-t1, "seconds")
