# -*- coding: utf-8 -*-
# Import modules from ../../..
import os, sys
# PARDIR = os.path.dirname(os.path.realpath(__file__))
# PARDIR = os.path.join(PARDIR, os.pardir, os.pardir, os.pardir)
# if PARDIR not in sys.path:
#     sys.path.append(PARDIR)

sys.path.append('../SRNet/')
#from ....jpeg import jpeg
from jpeg import dct
from jpeg import base
#from jpeg import compress


def dequantise(C,Q):
  """Dequantise JPEG coefficients using the quantisation matrix Q."""
  [k,l] = C.shape
  [m,n] = Q.shape
  rep = (k//m, l//n)
  return C * base.repmat(Q, rep)


from math import pi, cos, sin
import numpy as np
from scipy.signal import fftconvolve, convolve2d
import time
from scipy.ndimage.filters import convolve

TIME = 0

def unique_rows(a):
    a = np.ascontiguousarray(a)
    unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))

class GFR_process:
    """
    This class represents GFR algorithm
    """
    def __init__(self, image_path, qual):
        """
        Store the image path and the quality factor
        -------------------------------------------------------------------------
        Input:  image_path ....... path to the JPEG image
                qual ............. JPEG quality factor (can be either 75 or 95)
        -------------------------------------------------------------------------
        """
        try:
            self.imagepath = image_path
            self.QF = int(qual)
            # NR = number of rotations for Gabor kernel
            self.NR = 32
        except (ValueError):
            "Please enter an integer as quality factor (QF = 75 or 95)"
            raise


    def gaborkernel(self, sigma, theta, phi, gamma):
        """
        sigma represents the scale parameter, the small sigma means high spatial
        resolution, the image filtering coefficients reflect local properties in
        fine scale, while the large sigma means low spatial resolution, the
        coefficient reflect local properties in coarse scale.
        theta specifies the orientation of 2D Gabor filters.
        phi specifies the phase offset of the cosine factor.
        gamma is the spatial aspect ratio ans specifies the ellipticity of
        Gaussian factor
        """
        # cf. page 16 of [1]
        # sigma = 0.56 * lambda
        # lambda denotes the wavelength of the cosine factor
        lbd = sigma / 0.56
        gamma2 = gamma**2
        s = 1 / (2*sigma**2)
        f = 2*pi/lbd
        # sampling points for Gabor function
        xx = np.arange(-7.0/2.0, 4)
        x,y = np.meshgrid(xx,xx)
        y = np.copy(-y)

        xp =  x * cos(theta) + y * sin(theta)
        yp = -x * sin(theta) + y * cos(theta)

        kernel = np.exp(-s*(xp*xp + gamma2*(yp*yp))) * np.cos(f*xp + phi)
        # normalization
        # in order to capture the embedding changes, all the 2D Gabor filters are
        # made zero mean bu substracting the kernel mean from all its elements
        # to form high-pass filter
        kernel = kernel- np.sum(kernel)/np.sum(abs(kernel))*abs(kernel)
        return kernel

    def compute_GFR(self,color=False):
        """
        This function extracts Gabor features for steganalysis of JPEG images
        proposed in [1]. Parameters are exactly set as they are described in the
        paper. Total dimensionality of the features with 32 rotations: 17000.
        -------------------------------------------------------------------------
        Output: F ... extracted Gabor features
        -------------------------------------------------------------------------
        [1] X. Song, F. Liu, C. Yang, X. Luo and Y. Zhang "Steganalysis of
            Adaptive JPEG Steganography Using 2D Gabor Filters", Proceedings of
            the 3rd ACM Workshop on Information Hiding and Multimedia Security,
            Pages 15-23, Portland, OR, June 2015.
        -------------------------------------------------------------------------
        """

        """
        Computes features of the image.

        :param color:   specify wether the image contains color or not
        """


        #img = jpeg(self.imagepath, verbosity=0)
        img = np.load(self.imagepath)

        if not color :
            # Force usage of only a layer
            #dct_coeff = img.coef_arrays[0]
            dct_coeff = img
            #c_quant = img.quant_tables[0]
            c_quant = np.load('./c_quant_'+str(self.QF)+'.npy')
            return self.compute_GFR_layer(c_quant, dct_coeff)
        else :
            # Uses the three layers
            if len(img.quant_tables) < 3 :
                raise Exception('This image seems to be a greyscale image : ' + self.imagepath)
            else :
                # If there is less that 3 DCT coefficients specified, it means that they are the sames.
                # ie. same for both chrominance or same for all layers.
                for i in range(1, len(img.quant_tables)) :
                    if img.quant_tables[i] is None :
                        # don't have to copy it as it won't be modified
                        img.quant_tables[i] = img.quant_tables[i-1]

                # concatenates the 3 features
                feats = [self.compute_GFR_layer(img.quant_tables[i], img.coef_arrays[i]) for i in range(3)]
                return np.concatenate(feats)

    def compute_GFR_layer(self, c_quant, dct_coeffs) :

        # number of histogram bins
        T = 4;
        q = [2, 4, 6, 8]
        # quantization steps
        if self.QF == 75:
            q = [2, 4, 6, 8]
        elif self.QF == 95 or self.QF == 100:
            q = [0.5, 1, 1.5, 2]
        elif self.QF == 85: # New
            q = [1, 2, 3, 4]

        rotations = np.arange(0, self.NR) * pi / self.NR
        sr=len(rotations)

        # Standard deviations
        sigma = [0.5, 0.75, 1, 1.25]
        ss=len(sigma)

        phase_shift = [0, pi/2]
        sp=len(phase_shift)

        aspect_ratio = 0.5

        # Decompress to spatial domain
        # dct_coeffs= img.coef_arrays[0]
        # c_quant = img.quant_tables[0]
        #I_spatial = compress.dequantise(dct_coeffs, c_quant)
        I_spatial = dequantise(dct_coeffs, c_quant)
        I_spatial = dct.ibdct(I_spatial)

        # Compute DCTR locations to be merged
        merged_coordinates = np.empty((25), dtype=object);
        for i in range(5):
            for j in range(5):
                coordinates = np.array([[i,j], [i,8-j], [8-i,j], [8-i,8-j]])
                coordinates = [row for row in coordinates if row[0] < 8 and row[1] < 8]
                coordinates = unique_rows(coordinates)
                merged_coordinates[i*5 + j] = coordinates;

        # Load Gabor Kernels : the filter bank includes 2D Gabor filters
        # with different scales and orientations
        kernel = np.empty((ss, sr, sp), dtype=object);
        s_i = 0
        for s in sigma:
            r_i = 0
            for r in rotations:
                p_i = 0
                for p in phase_shift:
                    kernel[s_i, r_i, p_i] = self.gaborkernel(s, r, p, aspect_ratio)
                    p_i += 1
                r_i +=1
            s_i += 1

        # Compute features
        mode_fea_dim = len(merged_coordinates)*(T+1)
        dim_f = sp * ss * sr
        dim_s = sp * ss * (sr // 2 + 1)
        FF = np.zeros(dim_f*mode_fea_dim, dtype=np.float32)
        F = np.zeros(dim_s*mode_fea_dim, dtype=np.float32)
        for mode_P in range(sp):
            for mode_S in range(ss):
                for mode_R in range(sr):

                     mode_index = mode_P*(sr*ss) + mode_S*sr+ mode_R + 1

                     if TIME:
                        t1 = time.time()
                     # 2D Gabor filtering
                     R = convolve(I_spatial, kernel[mode_S, mode_R, mode_P])
                     R = R[np.ix_(range(3,R.shape[0]-4), range(3,R.shape[1]-4))]
#                     cpt +=1
                     if TIME:
                        t2 = time.time()
                        print("Time for convolve() %.5f"%(t2 - t1))

                     # Subsample the filtered image
                     R = np.abs(np.round(R / q[mode_S]))
                     R[R > T] = T
                     # feature extraction and merging
                     for merged_index in range(len(merged_coordinates)):
                        f_merged = np.zeros(T+1, dtype=np.float32)
                        for coord_index in range(np.size(merged_coordinates[merged_index], 0)):
                            # According to the 64 DCt modes in 8*8 DCT block, the filtered
                            # image is subampling by step 8 to get 64 subimages
                            r_shift = merged_coordinates[merged_index][coord_index, 0]
                            c_shift = merged_coordinates[merged_index][coord_index, 1]
                            R_sub = R[r_shift::8, c_shift::8]
                            # Extract the histogram features
                            hist, bin_edges = np.histogram(R_sub, np.arange(T + 2))
                            # Merge the histgram features
                            f_merged = f_merged + hist
                        F_index_from = (mode_index - 1) * mode_fea_dim + merged_index * (T + 1)
                        F_index_to = (mode_index - 1) * mode_fea_dim + merged_index * (T + 1) + T + 1
                        FF[F_index_from:F_index_to] = f_merged / np.sum(f_merged)

                # merging of symmetrical directions
                M_index=np.arange(0, sr//2 - 1)
                MS=len(M_index)+2
                SI = (mode_index-sr)*mode_fea_dim

                F_out=FF[SI:  SI + MS* mode_fea_dim]
                F_M=FF[SI:  SI + sr* mode_fea_dim]
                # For example, suppose the orientation parameter
                # theta = {0, pi/8, 2pi/8, ..., 6pi/8, 7pi/8}, then the histogram
                # features of the filtered image with theta = pi/8,7pi/8,
                # theta=2pi/8, 6pi/8 and so on should be merged by averaging
                for i in M_index:
                    F_out[(i + 1) * mode_fea_dim:(i + 1) * mode_fea_dim + mode_fea_dim]= (F_M[(i + 1) * mode_fea_dim:(i + 1) * mode_fea_dim + mode_fea_dim]+ F_M[(sr - i - 1) * mode_fea_dim: (sr - i - 1) * mode_fea_dim + mode_fea_dim]) / 2
                ind = mode_P * ss + mode_S
                F[ind * MS * mode_fea_dim :ind * MS * mode_fea_dim + MS * mode_fea_dim]= F_out
        return F


from extractor import Extractor as ParentExtractor


class Extractor(ParentExtractor) :
    """
    Extract caracteristics from an image using DCTR algorithm.

    self.conf should specify :
        - imgs.QF : quality factor of cover images (corresponding to jpeg library, and most open-source softwares, such as imagemagic)
        - imgs.color : specify wether image is color or not
    """

    #file_type = 'jpg'

    def run(self, file) :
        engine = GFR_process(file, self.conf.QF)
        result = engine.compute_GFR(color=self.conf.color)
        return np.array(result)


# Main function
if __name__ == '__main__':
    gfr= GFR_process('image/1.jpg',75)
    t1 = time()
    res = gfr.compute_GFR()
    t2 = time()
    print("execution time:", t2-t1, "seconds")
