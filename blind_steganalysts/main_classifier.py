import numpy as np
import os, sys
import argparse

from LCLSMR import Classifier


if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])

    argparser.add_argument('--path_folder', type=str,default='/home/bernasol/python3/DCTR_GFR/experience_0_2/')
    argparser.add_argument('--iteration_step', type=int)

    argparser.add_argument('--features',type=str)  #DCTR, GFR or JRM
    argparser.add_argument('--model',type=str)  #xu-net or srnet

    argparser.add_argument('--train_size', type=int, default=4000)
    argparser.add_argument('--valid_size', type=int, default=1000)
    argparser.add_argument('--test_size', type=int, default=5000)

    #argparser.add_argument('--model',type=str,default='xu_net')

    conf = argparser.parse_args()
    conf.path_proj_cover_file = conf.path_folder+'proj_cover_'+conf.features
    conf.path_proj_stego_file = conf.path_folder+'proj_data_adv_'+str(conf.iteration_step)+'_'+conf.features

    path_cover = conf.path_folder+'cover_'+conf.features+'.npy'
    if(conf.model=='None'):
        paths_stego = [conf.path_folder+'data_adv_'+str(i)+'_'+conf.features+'.npy' for i in range(conf.iteration_step+1)]
    else:
        paths_stego = [conf.path_folder+'data_adv_0_'+conf.features+'.npy',conf.path_folder+'data_adv_0_'+conf.features+'.npy']
        for i in range(1,conf.iteration_step+1):
            paths_stego += [conf.path_folder+'data_adv_xu_net_'+str(i)+'_'+conf.features+'.npy', conf.path_folder+'data_adv_srnet_'+str(i)+'_'+conf.features+'.npy']
    cover, stegos = np.load(path_cover), np.array([np.load(path_stego) for path_stego in paths_stego])
    if conf.iteration_step==0:
        index = np.zeros(conf.train_size+conf.valid_size+conf.test_size,dtype=int)
    else:
        index = np.load(conf.path_folder+'data_train_'+str(conf.iteration_step)+'/index.npy')
    stego = np.array([stegos[x,i] for i,x in enumerate(index)])

    cover_tr, cover_va, cover_te = \
                cover[:conf.train_size], \
                cover[conf.train_size:conf.train_size+conf.valid_size], \
                cover[conf.train_size+conf.valid_size:conf.train_size+conf.valid_size+conf.test_size]
    stego_tr, stego_va, stego_te = \
                stego[:conf.train_size], \
                stego[conf.train_size:conf.train_size+conf.valid_size], \
                stego[conf.train_size+conf.valid_size:conf.train_size+conf.valid_size+conf.test_size]

    lin_class = Classifier(conf)
    PE_train , PINV , th  = lin_class.train(np.concatenate((cover_tr,cover_va),axis=0), \
            np.concatenate((stego_tr,stego_va),axis=0))
    TFP_tr , TFN_tr , TTN_tr , TTP_tr , PE_min , PE_true_tr = lin_class.test(cover_tr, stego_tr, conf)
    TFP_va , TFN_va , TTN_va , TTP_va , PE_min , PE_true_va = lin_class.test(cover_va, stego_va, conf)
    TFP_te , TFN_te , TTN_te , TTP_te , PE_min , PE_true_te = lin_class.test(cover_te, stego_te, conf)

    np.save(conf.path_folder+'train_'+str(conf.iteration_step)+'_'+conf.features+'.npy', np.array([PE_true_tr,PE_true_va,PE_true_te]))

    print(PE_true_tr,PE_true_va,PE_true_te)


