import os
from time import time, sleep


def is_finished(label):
    liste = os.popen("squeue --user=ulc18or").read()
    liste = liste.split('\n')[1:]
    for x in liste:
        if (label + '_' in x):
            return(False)
    return(True)


def wait(label):
    while not is_finished(label):
        sleep(60)


def create_training_dictionnary(model, batch_size_classif_xu, batch_size_eval_xu, epoch_num_xu,
                                CL_xu, start_emb_rate_xu, pair_training_xu, batch_size_classif_sr, batch_size_eval_sr, epoch_num_sr,
                                CL_sr, start_emb_rate_sr, pair_training_sr, batch_size_classif_ef, batch_size_eval_ef, epoch_num_ef,
                                CL_ef, start_emb_rate_ef, pair_training_ef, train_on_cost_map):

    training_dictionnary = {}
    models = model.split(',')

    for model in models:
        if(model == 'xunet'):
            training_dictionnary[model] = {'batch_size_classif': batch_size_classif_xu,
                                           'batch_size_eval': batch_size_eval_xu,
                                           'epoch_num': epoch_num_xu,
                                           'CL': CL_xu,
                                           'start_emb_rate': start_emb_rate_xu,
                                           'pair_training': pair_training_xu,
                                           'train_on_cost_map': train_on_cost_map}
        elif(model == 'srnet'):
            training_dictionnary[model] = {'batch_size_classif': batch_size_classif_sr,
                                           'batch_size_eval': batch_size_eval_sr,
                                           'epoch_num': epoch_num_sr,
                                           'CL': CL_sr,
                                           'start_emb_rate': start_emb_rate_sr,
                                           'pair_training': pair_training_sr,
                                           'train_on_cost_map': train_on_cost_map}
        elif(model == 'efnet'):
            training_dictionnary[model] = {'batch_size_classif': batch_size_classif_ef,
                                           'batch_size_eval': batch_size_eval_ef,
                                           'epoch_num': epoch_num_ef,
                                           'CL': CL_ef,
                                           'start_emb_rate': start_emb_rate_ef,
                                           'pair_training': pair_training_ef,
                                           'train_on_cost_map': train_on_cost_map}

    return(training_dictionnary)


def write_command(mode, iteration_step, model, data_dir_prot, data_dir_cover, data_dir_stego_0, cost_dir,
                  image_size, QF, folder_model, permutation_files, version_eff, stride, n_loops,
                  train_size, valid_size, test_size, attack, attack_last, emb_rate,
                  batch_adv, n_iter_max_backpack, N_samples, tau_0, precision, lr,
                  num_of_threads, training_dictionnary, spatial):

    com = ' --data_dir_prot='+data_dir_prot
    com += ' --data_dir_cover='+data_dir_cover
    com += ' --image_size='+str(image_size)
    com += ' --QF=' + str(QF)
    com += ' --folder_model='+folder_model
    com += ' --permutation_files=' + permutation_files
    com += ' --version_eff=' + version_eff
    com += ' --stride=' + str(stride)
    com += ' --n_loops=' + str(n_loops)

    if(mode == 'classif'):
        com += ' --batch_size_eval=' + \
            str(training_dictionnary[model]['batch_size_eval'])
        com += ' --data_dir_stego_0='+data_dir_stego_0
        com += ' --train_size='+str(train_size)
        com += ' --valid_size='+str(valid_size)
        com += ' --test_size='+str(test_size)
        com += ' --model='+model

    elif (mode == 'attack'):
        com += ' --iteration_step='+str(iteration_step)
        com += ' --attack=' + str(attack)
        com += ' --attack_last=' + str(attack_last)
        com += ' --emb_rate=' + str(emb_rate)
        com += ' --cost_dir=' + str(cost_dir)
        com += ' --batch_adv=' + str(batch_adv)
        com += ' --n_iter_max_backpack=' + str(n_iter_max_backpack)
        com += ' --N_samples=' + str(N_samples)
        com += ' --tau_0=' + str(tau_0)
        com += ' --precision=' + str(precision)
        com += ' --lr=' + str(lr)
        com += ' --model=' + model

    elif (mode == 'train'):
        com += ' --iteration_step='+str(iteration_step)
        com += ' --train_size='+str(train_size)
        com += ' --valid_size='+str(valid_size)
        com += ' --test_size='+str(test_size)
        com += ' --model='+model
        com += ' --num_of_threads='+str(num_of_threads)
        com += ' --emb_rate=' + str(emb_rate)
        com += ' --cost_dir=' + str(cost_dir)
        com += ' --data_dir_stego_0=' + str(data_dir_stego_0)
        com += ' --spatial=' + str(spatial)

        com += ' --batch_size_classif=' + \
            str(training_dictionnary[model]['batch_size_classif'])
        com += ' --batch_size_eval=' + \
            str(training_dictionnary[model]['batch_size_eval'])
        com += ' --epoch_num='+str(training_dictionnary[model]['epoch_num'])
        com += ' --CL=' + str(training_dictionnary[model]['CL'])
        com += ' --start_emb_rate=' + \
            str(training_dictionnary[model]['start_emb_rate'])
        com += ' --pair_training=' + \
            str(training_dictionnary[model]['pair_training'])
        com += ' --train_on_cost_map=' + \
            str(training_dictionnary[model]['train_on_cost_map'])

    return(com)


def run_job(mode, label, command, iteration, name_account, num_of_threads=None,
            num_batch=None, gpu=True):

    name = label + '_' + str(iteration) + '_' + mode
    job_file = "./%s.job" % name

    with open(job_file, 'w+') as fh:
        fh.writelines("#!/bin/bash\n")
        fh.writelines("#SBATCH --nodes=1\n")
        fh.writelines("#SBATCH --job-name=%s\n" % name)

        if(gpu):
            fh.writelines("#SBATCH --gres=gpu:1\n")
            fh.writelines("#SBATCH --ntasks=1\n")
            fh.writelines("#SBATCH --hint=nomultithread\n")
            fh.writelines("#SBATCH --time=15:00:00 \n")
            fh.writelines("#SBATCH --account=%s@gpu\n" % name_account)

        else:
            fh.writelines("#SBATCH --time=2:00:00 \n")
            fh.writelines("#SBATCH --account=%s@cpu\n" % name_account)

        if(mode == 'attack'):
            fh.writelines("#SBATCH -C v100-32g\n")
            fh.writelines("#SBATCH --array="+str(0)+'-'+str(num_batch)+" \n")
            fh.writelines("module purge\n")
            fh.writelines("module load pytorch-gpu/py3/1.7.1\n")
            # WRITE LOAD MODULES
            fh.writelines("python -u " + command)

        elif('train' in mode):
            fh.writelines("#SBATCH -C v100-32g\n")
            fh.writelines("#SBATCH --cpus-per-task="+str(num_of_threads)+"\n")
            fh.writelines("module purge\n")
            fh.writelines("module load pytorch-gpu/py3/1.7.1\n")
            fh.writelines("time python -u " + command)

        else:
            fh.writelines("module purge\n")
            fh.writelines("module load pytorch-gpu/py3/1.7.1\n")
            fh.writelines("time python -u " + command)

    os.system("sbatch %s" % job_file)
