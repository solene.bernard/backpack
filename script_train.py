import sys, os
sys.path.append('models/')
from torch.utils.data import Dataset, DataLoader
import argparse
import sklearn
from torch.utils.data.sampler import SequentialSampler, RandomSampler
import torch
import numpy as np
from srnet import TrainGlobalConfig as TrainGlobalConfig_sr
from srnet import get_net as get_net_sr
from xunet import TrainGlobalConfig as TrainGlobalConfig_xu
from xunet import get_net as get_net_xu
from efficientnet import TrainGlobalConfig as TrainGlobalConfig_ef
from efficientnet import get_net as get_net_ef
#from catalyst.data.sampler import BalanceClassSampler
from data_loader import load_dataset, DatasetRetriever, get_train_transforms, get_valid_transforms
from train import Fitter




def my_collate(batch, pair_training=False):
    if pair_training:
        cover = torch.stack([x[0][0] for x in batch])
        stego = torch.stack([x[0][1] for x in batch])
        label_cover = torch.stack([x[1][0] for x in batch])
        label_stego = torch.stack([x[1][1] for x in batch])
        r = torch.randperm(len(cover)*2)
        return(torch.cat((cover, stego))[r], torch.cat((label_cover, label_stego))[r])

    else:
        return torch.utils.data.dataloader.default_collate(batch)


def run_training(iteration_step, model, net, trainGlobalConfig, data_dir_prot, start_emb_rate, emb_rate, pair_training,
                 version_eff, load_checkpoint, train_dataset, validation_dataset, test_dataset, folder_model, train_on_cost_map):

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    train_dataset.emb_rate = start_emb_rate
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=trainGlobalConfig.batch_size,
        pin_memory=True,
        drop_last=True,
        shuffle=True,
        num_workers=trainGlobalConfig.num_workers,
        collate_fn=lambda x: my_collate(x, pair_training=pair_training)
    )
    val_loader = torch.utils.data.DataLoader(
        validation_dataset,
        batch_size=trainGlobalConfig.batch_size,
        num_workers=trainGlobalConfig.num_workers,
        shuffle=False,
        # sampler=SequentialSampler(validation_dataset),
        pin_memory=True,
        collate_fn=lambda x: my_collate(x, pair_training=pair_training)
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=trainGlobalConfig.batch_size,
        num_workers=trainGlobalConfig.num_workers,
        shuffle=False,
        # sampler=SequentialSampler(test_dataset),
        pin_memory=True,
        collate_fn=lambda x: my_collate(x, pair_training=pair_training)
    )

    save_path = data_dir_prot + 'train_' + model + '_' + str(iteration_step) + '/'
    #save_path = data_dir_prot + 'train_'+model+'_stego_'+str(iteration_step) +'/'
    #save_path = data_dir_prot + 'train_'+model+'_'+str(iteration_step) +'_'+str(emb_rate)+'/'
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    fitter = Fitter(model=net, device=device, config=trainGlobalConfig,
                    save_path=save_path, model_str=model, train_on_cost_map=train_on_cost_map)
    print(f'{fitter.base_dir}')

    if(load_checkpoint is not None):
        save_path_load = save_path
        #save_path_load = data_dir_prot + 'train_'+model+'_'+str(iteration_step) +'/'
        if(load_checkpoint == 'best'):
            best_epoch = [int(x.split('-')[-1][:3])
                          for x in os.listdir(save_path_load)
                          if 'best' in x]
            best_epoch.sort()
            best_epoch = str(best_epoch[-1])
            path = save_path_load + 'best-checkpoint-'+'0' * \
                (3-len(best_epoch))+best_epoch+'epoch.bin'
            fitter.load(path)
        else:
            fitter.load(save_path+load_checkpoint)

    fitter.fit(train_loader, val_loader, emb_rate)

    train_loader.dataset.emb_rate = emb_rate

    _, train = fitter.validation(train_loader)
    print('Pe_err train :', train.avg)
    _, val = fitter.validation(val_loader)
    print('Pe_err val :', val.avg)
    _, test = fitter.validation(test_loader)
    print('Pe_err test :', test.avg)

    np.save(save_path+'error_rate.npy',
            np.array([train.avg, val.avg, test.avg]))

    return(train.avg, val.avg, test.avg)


def train(iteration_step, model, folder_model, data_dir_prot, permutation_files, num_of_threads,
          data_dir_cover, data_dir_stego_0, cost_dir, train_size, valid_size, test_size,
          image_size, batch_size_classif, batch_size_eval, epoch_num, QF, emb_rate,
          spatial, train_on_cost_map, H1_filter, L1_filter, L2_filter,
          version_eff, stride, n_loops, CL, start_emb_rate, pair_training, load_model):

    pair_training = pair_training == 'yes'
    spatial = spatial == 'yes'
    train_on_cost_map = train_on_cost_map == 'yes'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    dataset = load_dataset(iteration_step, permutation_files, train_size, valid_size, test_size,
                           data_dir_prot, pair_training=pair_training)

    train_valid_test_names = [dataset[dataset['fold']
                                      == i].image_name.values for i in range(3)]
    train_valid_test_indexs_db = [
        dataset[dataset['fold'] == i].indexs_db.values for i in range(3)]

    if pair_training:
        train_valid_test_labels = [None, None, None]
    else:
        train_valid_test_labels = [
            dataset[dataset['fold'] == i].label.values for i in range(3)]

    train_valid_test_transforms = [
        get_train_transforms(), get_valid_transforms(), None]

    # TRAIN FIRST WITH COST MAPS
    datasets = [DatasetRetriever(image_names, folder_model, QF, emb_rate, image_size,
                                 data_dir_cover, data_dir_stego_0, cost_dir, data_dir_prot,
                                 H1_filter, L1_filter, L2_filter, indexs_db, train_on_cost_map=True,
                                 labels=labels,  transforms=transforms, pair_training=pair_training, spatial=spatial)
                for image_names, indexs_db, labels, transforms in
                zip(train_valid_test_names, train_valid_test_indexs_db,
                    train_valid_test_labels, train_valid_test_transforms)]
    train_dataset, validation_dataset, test_dataset = datasets[0], datasets[1], datasets[2]

    if(model == 'efnet'):
        net = get_net_ef(version_eff, stride).to(device)
        trainGlobalConfig = TrainGlobalConfig_ef(
            num_of_threads, batch_size_classif, epoch_num)
    elif(model == 'xunet'):
        net = get_net_xu(folder_model, n_loops, image_size).to(device)
        trainGlobalConfig = TrainGlobalConfig_xu(
            num_of_threads, batch_size_classif, epoch_num)
    elif(model == 'srnet'):
        net = get_net_sr(image_size).to(device)
        net.init()
        trainGlobalConfig = TrainGlobalConfig_sr(
            num_of_threads, batch_size_classif, epoch_num)

    if(CL == 'yes'):
        start_emb_rate = start_emb_rate
    else:
        start_emb_rate = emb_rate


    train, val, test = run_training(iteration_step, model, net, trainGlobalConfig, data_dir_prot, start_emb_rate,
                                    emb_rate, pair_training, version_eff, load_model,
                                    train_dataset, validation_dataset, test_dataset, folder_model, train_on_cost_map=True)
    print(train, val, test)

    # TRAIN THEN WITH STEGO IMAGES
    if(model == 'efnet'):
        net = get_net_ef(version_eff, stride).to(device)
        trainGlobalConfig = TrainGlobalConfig_ef(
            num_of_threads, batch_size_classif, epoch_num)
    elif(model == 'xunet'):
        net = get_net_xu(folder_model, n_loops, image_size).to(device)
        trainGlobalConfig = TrainGlobalConfig_xu(
            num_of_threads, batch_size_classif, epoch_num)
    elif(model == 'srnet'):
        net = get_net_sr(image_size).to(device)
        net.init()
        trainGlobalConfig = TrainGlobalConfig_sr(
            num_of_threads, batch_size_classif, epoch_num)
        
    datasets = [DatasetRetriever(image_names, folder_model, QF, emb_rate, image_size,
                                 data_dir_cover, data_dir_stego_0, cost_dir, data_dir_prot,
                                 H1_filter, L1_filter, L2_filter, indexs_db, train_on_cost_map=False,
                                 labels=labels,  transforms=transforms, pair_training=pair_training, spatial=spatial)
                for image_names, indexs_db, labels, transforms in
                zip(train_valid_test_names, train_valid_test_indexs_db,
                    train_valid_test_labels, train_valid_test_transforms)]
    train_dataset, validation_dataset, test_dataset = datasets[0], datasets[1], datasets[2]

    # Train then with the stegos from the best iteration obtained from the training with cost maps
    # (set load_model to 'best')
    train, val, test = run_training(iteration_step, model, net, trainGlobalConfig, data_dir_prot, start_emb_rate,
                                    emb_rate, pair_training, version_eff, 'best',
                                    train_dataset, validation_dataset, test_dataset, folder_model, train_on_cost_map=False)
    print(train, val, test)

    # train, val, test = run_training(iteration_step, model, net, trainGlobalConfig, data_dir_prot, start_emb_rate, \
    #     emb_rate, pair_training, version_eff, load_model, \
    #     train_dataset, validation_dataset, test_dataset)

    return(train, val, test)


if __name__ == '__main__':

    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--iteration_step', type=int, help='Iteration step')
    argparser.add_argument('--folder_model', type=str,
                           help='The path to the folder where the architecture of models are saved')
    argparser.add_argument('--data_dir_prot', type=str,
                           help='Path of the protocol')
    argparser.add_argument('--permutation_files',
                           type=str, help='List of files')

    argparser.add_argument('--num_of_threads', type=int,
                           help='Number of CPUs available')

    argparser.add_argument('--data_dir_cover', type=str,
                           help='Where are the .npy files of cover')
    argparser.add_argument('--data_dir_stego_0', type=str,
                           help='Where are the inital stegos')
    argparser.add_argument('--cost_dir', type=str,
                           help='Where are the .npy of costs')

    argparser.add_argument('--train_size', type=int, help='Size of train set')
    argparser.add_argument('--valid_size', type=int, help='Size of valid set')
    argparser.add_argument('--test_size', type=int, help='Size of test  set')

    argparser.add_argument('--image_size', type=int)
    argparser.add_argument('--batch_size_classif', type=int)
    argparser.add_argument('--batch_size_eval', type=int)
    argparser.add_argument('--epoch_num', type=int, help='Number of epochs')
    argparser.add_argument(
        '--QF', type=int, help='Quality factor, values accepted are 75, 95 and 100')
    argparser.add_argument('--emb_rate', type=float,
                           help='Float between 0 and 1')
    argparser.add_argument('--spatial', type=str, default='no',
                           help='yes if for spatial steganography, no for jpeg steganography ')
    argparser.add_argument('--train_on_cost_map', type=str, default='yes',
                           help='yes or no. If yes stegos are created from the cost maps at during the training, elif no classifiers are trained directly with the stegos.')

    # FOR CUSTOM FILTERS FOR HILL
    argparser.add_argument('--H1_filter', type=str,
                           default=None, help='Path to the saved H1 filter')
    argparser.add_argument('--L1_filter', type=str,
                           default=None, help='Path to the saved L1 filter')
    argparser.add_argument('--L2_filter', type=str,
                           default=None, help='Path to the saved L2 filter')

    # Model parameters
    argparser.add_argument('--model', type=str,
                           help='Model : efnet, xunet or srnet')
    # for efnet
    argparser.add_argument('--version_eff', type=str,
                           help='Version of efficient-net, from b0 to b7')
    argparser.add_argument('--stride', type=int,
                           help='Stride at the beginning. Values=1 or 2')
    # for xunet
    argparser.add_argument('--n_loops', type=int, default=5,
                           help='Number of loops in th xunet architecture')

    # Training parameters
    argparser.add_argument(
        '--CL', type=str, help='yes or no. If yes starts from emb_rate of start_emb_rate and decrease until reach emb_rate')
    argparser.add_argument('--start_emb_rate', type=float,
                           default=0.7, help='Is CL=yes, is the starting emb_rate')
    argparser.add_argument('--pair_training', type=str, help='Yes or no')

    argparser.add_argument('--load_model', type=str,
                           default=None, help='Path to the saved efficient model')

    params = argparser.parse_args()

    train, val, test = train(**vars(params))
    print(train, val, test)
