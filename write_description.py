from datetime import date


def describe_exp_txt(begin_step, number_steps, num_of_threads,
                     QF, image_size, emb_rate, data_dir_cover, data_dir_stego_0, cost_dir,
                     strategy, model, version_eff, stride, n_loops,
                     train_size, valid_size, test_size, permutation_files, training_dictionnary,
                     attack, n_iter_max_backpack, N_samples, tau_0, precision, data_dir_prot):

    n_images = train_size + valid_size + test_size
    models = model.split(',')

    tab = []
    tab.append(date.today().strftime("%b-%d-%Y"))
    tab.append('Launch of the protocol, starting from iteration ' +
               str(begin_step) + ' to ' + str(begin_step+number_steps) + '\n')
    tab.append('Number of CPUs called : ' + str(num_of_threads) + '\n \n')

    tab.append('PARAMETERS \n')

    tab.append('Image characteristics \n')
    tab.append('- QF = ' + str(QF) + '\n')
    tab.append('- Image size = ' + str(image_size) + '\n')
    tab.append('- Embedding rate = ' + str(emb_rate) + ' bpnzAC\n')
    tab.append('- Cover images are taken in folder ' + data_dir_cover + '\n')
    tab.append('- Stego images are taken in folder ' + data_dir_stego_0 + '\n')
    tab.append('- Cost maps are taken in folder ' + cost_dir + '\n \n')

    tab.append('Protocol setup \n')
    tab.append('- Strategy =' + strategy + ' \n \n')

    tab.append('Model description \n')
    if(len(models) == 1):
        tab.append('- Model architecture is ' + model +
                   ' with the following setup :\n')
        if model == 'efnet':
            tab.append('     - Efficient-net version is ' +
                       str(version_eff) + ' pretrained on image-net \n')
            tab.append('     - First conv stem is with stride = ' +
                       str(stride) + ' \n \n')
        elif model == 'xunet':
            tab.append('     - XuNet architecture is composed with ' +
                       str(n_loops) + ' big blocks \n \n')
    else:
        tab.append('- The '+str(len(models)) + ' model architectures are ' +
                   model + ' with the following setup :\n')
        if 'efnet' in models:
            tab.append('     - Efficient-net version is ' +
                       str(version_eff) + ' pretrained on image-net \n')
            tab.append('     - First conv stem is with stride = ' +
                       str(stride) + ' \n \n')
        if 'xunet' in models:
            tab.append('     - XuNet architecture is composed with ' +
                       str(n_loops) + ' big blocks \n \n')

    tab.append('Training setup \n')
    tab.append('- Train size = '+str(train_size) + '\n')
    tab.append('- Valid size = '+str(valid_size) + '\n')
    tab.append('- Test size = '+str(test_size) + '\n')
    tab.append('- Files permutation, which order determines train, valid and test sets is ' +
               permutation_files + '\n')

    for model in models:
        tab.append('- Model '+model+' is trained during ' +
                   str(training_dictionnary[model]['epoch_num']) + ' epochs \n')
        if(training_dictionnary[model]['pair_training'] == 'no'):
            tab.append('- Pair training is not used \n')
            tab.append('- Batch size is ' +
                       str(training_dictionnary[model]['batch_size_classif']) + ' \n')
        else:
            tab.append('- Pair training is used \n')
            tab.append('- Batch size is 2*' +
                       str(training_dictionnary[model]['batch_size_classif']) + ' \n')
        if(training_dictionnary[model]['CL'] == 'yes'):
            tab.append('- Curriculum is used : the embedding rate starts from ' + str(
                training_dictionnary[model]['start_emb_rate']) + ' and decreases every two epochs by factor 0.9 to reach target embedding rate '+str(emb_rate)+'\n \n')
        else:
            tab.append('- Curriculum is not used : the embedding rate = ' +
                       str(emb_rate)+' is constant during training \n \n')

    tab.append('Attack setup \n')
    tab.append('- The smoothing function is ' + str(attack) + ' \n')
    tab.append('- Maximum number of steps is ' +
               str(n_iter_max_backpack) + ' \n')
    tab.append('- Number of samples is ' + str(N_samples) + ' \n')
    tab.append('- Tau is initialized with value ' + str(tau_0) +
               ' and decreases by factor 0.5 when needed\n')
    tab.append(
        '- The exit condition is required to be respected with precision = '+str(precision)+'\n \n')

    file = open(data_dir_prot+"description.txt", "w")
    file.writelines(tab)
    file.close()


def update_exp_txt(params, lines_to_append):
    file_name = params.data_dir_prot+"description.txt"
    # Open the file in append & read mode ('a+')
    with open(file_name, "a+") as file_object:
        appendEOL = False
        # Move read cursor to the start of file.
        file_object.seek(0)
        # Check if file is not empty
        data = file_object.read(100)
        if len(data) > 0:
            appendEOL = True
        # Iterate over each string in the list
        for line in lines_to_append:
            # If file is not empty then append '\n' before first line for
            # other lines always append '\n' before appending line
            if appendEOL == True:
                file_object.write("\n")
            else:
                appendEOL = True
            # Append element at the end of file
            file_object.write(line)
