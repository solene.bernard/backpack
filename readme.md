# Overall idea

The protocol is made to run on a multi-GPU platform, with orders launched via .slurm files.
The protocols runs automatically via main.py, which is the script which creates slurm files with CLI commands depending on the task of the protocol.

The python files contained at the root of this folder are codes called by main.py. 
A run of a protocol is saved in a folder like 'experiment/'. All stegos, cost maps, classifiers, evaluation on every classifier on every data bases, are saved in it. The structure of thoses files are shown in the .png file displayed at the end of this documentation. 

A run of a protocol need data in input, which are:
* a data base of cover images, where each cover is saved in a different .npy file.
* a data base of stego images (each in a different .npy file)
* a data base of initial costs (each in a different .npy). It can have two shapes:
	* (3,image_size,image_size) : in that case, first channel for cost of -1, second for 0 and final for +1 
	* or (image_size, image_size):  in that case, only channel for symmetric cost of doing -1 or +1

# Details

### Here are the steps of the protocol:
If --begin_step=0, the run of the protocol will begin with initialization which contains the following steps:
* training of classifiers (depending of --model) between cover and stego in folder 'train_$model_0/'. 
* evaluation of the classifiers and both databases: cover and initial stegos.

For further iteration $k, the following data is generateed:
* adversarial stegos stored in a .npy file in the folder "data_adv_$k/adv_final/"
* optimized costs maps of shape 3*image_size*image_size stored in the folder "data_adv_$k/adv_cost/"
* evaluation of all the classifiers (all possible models trained at all previous iterations) on this new database of stegos in "data_adv_$k/eval_$model_$i/" of i between 0 and k-1.
* index of the database of stego images (vector of shape 10000 containing integer between 0 and k) stored in "data_train_$k/index.npy"
* train of classifier "model" stored in "train_$model_k/"
* evaluation of this new classifier on cover (in "cover/eval_$model_k/") and all the databases of stegos (in "data_adv_$i/eval_$model_k/") saved in "data_adv_$i/adv_final/" for all i between 0 and k.



### Structure of the results of a run of the protocol
A run of a protocol is an experiment for given values of QF, emb_rate, intial cost_maps, different steganalysts...
A run of the protocol will save all values in a folder, which is defined in the parameter --data_dir_prot.
The organization of this folder is described by the illustration, and in the following.
At the beginning, it creates file description.txt which resumes all parameters parsed at the beggining of run of main.py. 

* data_adv_$k/ 
	* adv_final/
		* $i.npy: adversarial stego images of size (image_size, image_size) stored in different .npy files
	* adv_cost/
		* $i: optimized cost maps of size (3,image_size,image_size) stored in different .npy files
	* eval_$model_$j/
		* logits.npy: of size (10000,2) containing the raw logits given by the evaluation of classifier with architecture $model trained at iteration $j on stego images contained in folder data_adv_$k/adv_final/. Evaluations of the 10000 images are ordered following the permutation files given in parameter --permutation_files.npy (given in input of the protocol)
		* probas.npy: of size (10000,) which are the stego class probability given by the softmax of the logits. The order in the same than logits.
* cover/
	* eval_$model_$j/
		* logits.npy: same as before but for classifier $model trained at iteration $j evaluated on the cover images (stored in --data_dir_cover).
		* probas.npy: output stego class probability obtained from the softmax of logits.npy.
* data_train_$k/
	* index.npy: of size (10000,), index of the adversarial stegos picked from the strategy given in input of the protocol. The index of stegos are stored following file --permutation_files.npy. The index are integers i comprised between 0 (for initial stego images stored in --data_dir_stego_0) and k (for adversarial stegos stored in data_adv_$i/adv_final/ for i>=1).
* train_$model_$k/
	* error_rate.npy: of size (3,). Error rate given at the end of the training of model $model at iteration $k, on the train set, validation set and test set (in this order).
	* checkpoints.ckpt: during the training, every time the classifier achieved a higher accuracy on the validation set, the weights of the CNN are saved in a .ckpt file with the value of the epoch in its name. Such as at the end the best classifier is saved, and it is the one with the highest integer in its name.
	* log.txt: the value of the error rate on train and validation set during the training are saved in this .txt file at each epoch.


### How are trained the classifiers
Three different architectures of CNN are available: XuNet, SRNet and EfficientNet. Each are defined in a file in the ./models/ folder. To add more, you can add a file, defining the function get_net() and class TrainGlobalConfig() and modify the file script_train.py.

EfficientNet is initialized with weights given by the training on ImageNet, saved in file in ./models/. You can choose the version of Efficient Net (from B0 to B7) and the stride of the first convolution operation.

The classifiers are trained between cover images, and new stegos are generated in each batch with the corresponding cost map. It allows to train the classifier, if desired (depending on parameters --CL and --start_emb_rate) to use curriculum learning during the training, such as new stegos embedding any size of payload can be generated during the training.

### How works backpack
Multiple version of backpack are available, which are or will be introduced in past/future publications. Those are:
* SGE: Softmax Gumbel Estimation
* Double Tanh
* Double Compact Tanh. 

To do the Gradient Descent, one needs to compute 


# Parameters to pass in main.py

Finally that we discussed about all the steps of the protocol, here the explanation of which parameters to pass in the CLI with main.py: 

* begin_step: first iteration of the protocol. Should be equals to 0 if you never launched it.
* server: 'yes' or 'no'. If you use a server and can launch command via slurm files, you can use 'yes' and probably you need to custom function 'run_job' in write_jobs.py to adapt it to you server. If it is not available, use 'no'.
* name_account: If run on a server, name of the account from which launch the computations.  
* number_step: for how many further iteration to lauchn the protocol
* folder_model: absolute path leading to the folder './models/' stored in this folder. It contains the model and the architectures of the steganalysts networks. 
* permutation_files: path to the numpy array, containing a permutation of range(1,n), where n is the total number of samples. The $train_size first indices are for the train set, the following $valid_size are for the validation set and the last $test_size are the the test set.
* data_dir_prot: folder where all the stegos and classifiers produced during the algorithm are saved.
* data_dir_cover: folder containing all cover images in the .npy format. One file for each image is required. 
* data_dir_stego_0: folder containing stegos at iteration 0, in the same format as cover images. 
* cost_dir: folder containing all initial modification maps at iteration 0. Each cost map is saved in a single file for format .npy.  It has the shape 
	* 3 x image_size x image_size : in that case, first channel for cost of -1, second for 0 and final for +1 
	* or image_size x image_size:  in that case, only channel for symmetric cost of doing -1 or +1
* strategy='minmax': one of the three strategies available: 'minmax' (by default), 'random' or 'lastit'
* image_size='512': height of the squared images used
* QF: quality factor of the images used. For a given QF, the quantification table should be saved in folder 'folder_model/' in the format 'c_quant_$QF.npy'
* emb_rate: embedding rate in bpnzAC coefficients for JPEG images, or bpp for spatial images
* model: string value of models against Alice optimizes herself. 3 models are implemented: 'xunet', 'srnet' or 'efnet'. For multiple models, the models should be seperated by a coma ','. For example, models='xunet,srnet,efnet'
* version_eff: string, which value is from 'b0', 'b1', ... to 'b7'. This is the size of the models of efficient net.
* stride: 1 or 2, for the stride of the convolution at the beginning of efficient net.
* batch_size_classif_ef, batch_size_classif_sr, batch_size_classif_xu: batch size to use during training of each classifier (efficient net, SRNet or XU-Net)
* batch_size_eval_ef, batch_size_eval_sr, batch_size_eval_xu: batch size to use during evaluation of networks.
* epoch_num_ef, epoch_num_sr, epoch_num_xu: number of epochs for the training for each classifier
* CL_ef, CL_sr, CL_xu: 'yes' for using curriculum learning during training, 'no' for not using it. 
* start_emb_rate_ef, start_emb_rate_sr, start_emb_rate_xu: if parameter CL is set to 'yes' for the corresponding classifier, starting value of the embedding rate during the training. It is during training decreased bu substracting value 0.1 every two epochs. It can be modified in class Fitter defined in file train.py in the method fit. 
* pair_training_ef, pair_training_sr, pair_training_xu: 'yes' for using pair training during the training of each network, 'no' for not using it.
* n_iter_max_backpack: integer for the maximum number of steps to use during the gradient descent of the optimization with backpack
* tau_0: starting float value of the temperature controlling the smoothness in the softmax gumbel distribution.
* precision: float value for the precision for the stopping condition of the gradient descent, for checking inequalities like f(stego) "<" f(cover), which is replaced by f(stego) "<" f(cover)+precision
* N_samples: integer for the number of samples of stego to use during the gradient descent for checking the average detectability of the cost map.
* attack: 'SGE', 'DoTanh' or 'DoCoTanh' for softmax gumbel estimation, double tanh or double compact tanh. This is for which differentiable function to use for approximating the discrete changes.
* attack_last: 'no' or 'yes', for how many previous classifiers to optimize during the gradient descent. By default equals to 'yes', because the attack needs to load all previous classifiers which might not fit in GPU. If it doesn't fit, you can set this parameter to 'yes' such as it will take classifiers trained in the 3 previous iterations. This can be changed in python file script_attack.py, in the line "n_classifier_min = max(params.iteration_step-3, 0)".
* lr: float value for the value of the learning rate to use in ADAM optimizer for the gradient descent. Advices: use 0.5 for QF 75 and 0.05 for QF 100.
	


# Files and folders in the root

##### Folders
* models: folder containing some constants, and .py files describing three different models 
	* NUMPY FILES : 
		* DCT_4.npy : preprocessing kernel of size 4x4 containing DCT coefficients used in XUNet architecture
		* c_quant_75.npy, c_quant_95.npy, c_quant_100.npy : quantization tables for QF 75, 95 or 100
		* permutation_files.npy : permutation of files (take the first images to be in train, after in valid and last in test set)
	* PYTHON FILES:
		* efficientnet.py, srnet.py and xunet.py for describing three steganalysts models
##### Files
* main.py: Script to call for launching a protocol. It will itself run during the whole protocol and call other scripts to proceed to some actions. At the beginning, it creates the files description.txt which resumes the parameters of the experiment.
* backpack.py: the class Backpack with SGE, which provides the output and the gradient of the output of a classifier w.r.t. costs.
* double_tanh.py: the class with Backpack but with Double Tanh or Double Compact Tanh instead of SGE
* eval_classifier.py: function which evaluates a classifier on a database
* generate_train_db.py: produces the index (files index.npy) of the adversarial stegos picked from the minmax strategy. It stores the result of np.argmin(np.max(probas,axis=1),axis=0).
* data_loader.py : contains input_fn to feed the network
* script_attack.py: script which launches the gradient descent for each images.
* script_evalute_classif.py: script which launches the evaluation of any classifier on any database.
* script_train.py: script which launche the training of a classifier
* train.py: definition of the class Fitter useful for training a classifier.


![arborescence](arborescence_protocol.png)





