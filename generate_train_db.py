import numpy as np
import argparse
import sys
from shutil import copy
import os
import glob
from time import time
from statsmodels.distributions.empirical_distribution import ECDF


def generate_train_db(iteration_step, strategy, models, permutation_files, data_dir_prot):
    files = np.load(permutation_files)
    if strategy == 'minmax':
        # Probas = Probas(image==stego==1)
        # nb_data_bases * nb_classifs * n_images
        probas = np.zeros(
            (iteration_step+1, iteration_step*len(models), len(files)))
        # lignes * colonnes * profondeur
        # CALIBRATION
        ecdf_list = []
        for i in range(iteration_step):
            for model in models:
                ecdf = ECDF(np.load(data_dir_prot+'cover/eval_' +
                                    model+'_'+str(i)+'/probas.npy'))
                probas[0, i] = ecdf(
                    np.load(data_dir_prot+'data_adv_0/eval_'+model+'_'+str(i)+'/probas.npy'))
                ecdf_list.append(ecdf)
        for j in range(1, iteration_step+1):
            for i in range(iteration_step):
                for k, model in enumerate(models):
                    ecdf = ecdf_list[i*len(models)+k]
                    probas[j, i*len(models)+k] = ecdf(np.load(data_dir_prot +
                                                              'data_adv_'+str(j)+'/eval_'+model+'_'+str(i)+'/probas.npy'))
        index = np.argmin(np.max(probas, axis=1), axis=0)
    elif strategy == 'random':
        index = np.random.randint(iteration_step+1, size=len(files))
    elif strategy == 'lastit':
        index = np.zeros(len(files), dtype=int)+iteration_step
    if not os.path.exists(data_dir_prot+'data_train_'+str(iteration_step)+'/'):
        os.makedirs(data_dir_prot+'data_train_'+str(iteration_step)+'/')
    np.save(data_dir_prot+'data_train_' +
            str(iteration_step)+'/index.npy', index)
    return(index)
