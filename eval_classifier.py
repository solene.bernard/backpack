import os
import numpy as np
from scipy.fftpack import dct, idct
import sys
import torch
from functools import partial
import argparse
from data_loader import compute_spatial_from_jpeg
from efficientnet import get_net


def softmax(array):
    exp = np.exp(array-np.max(array, axis=1, keepdims=True))
    return(exp/np.sum(exp, axis=1, keepdims=True))


class cover_stego_loader(object):

    def __init__(self, params, iteration, mode):  # mode = stego or cover
        self.params = params
        n_images = params.train_size + params.valid_size + params.test_size
        self.files = np.load(params.folder_model +
                             'permutation_files.npy')[:n_images]
        self.train_counter = 0
        self.train_data_size = len(self.files)
        self.train_num_batches = int(
            np.ceil(1.0 * self.train_data_size / params.batch_size_eval))
        self.iteration_step = iteration
        self.mode = mode
        self.c_quant = np.load(params.folder_model +
                               'c_quant_'+str(params.QF)+'.npy')

    def next_batch(self):

        borne_sup = min(self.train_counter +
                        self.params.batch_size_eval, len(self.files))
        n_images = borne_sup-self.train_counter

        next_batch_X = np.zeros(
            (n_images, self.params.image_size, self.params.image_size), dtype=np.float32)

        for i, file in enumerate(self.files[self.train_counter:borne_sup]):
            if(self.mode == 'stego'):
                if(self.iteration_step > 0):
                    try:
                        image = np.load(self.params.data_dir_prot+'data_adv_' +
                                        str(self.iteration_step)+'/adv_final/'+file[:-4]+'.npy')
                    except:
                        image = np.load(
                            self.params.data_dir_stego_0 + file[:-4]+'.npy')
                else:
                    image = np.load(
                        self.params.data_dir_stego_0 + file[:-4]+'.npy')
            elif(self.mode == 'cover'):
                image = np.load(self.params.data_dir_cover +
                                file[:-4] + '.npy')

            spat_image = compute_spatial_from_jpeg(image, self.c_quant)
            next_batch_X[i, :, :] = spat_image

        next_batch_X = np.reshape(
            next_batch_X, (next_batch_X.shape[0], 1, next_batch_X.shape[1], next_batch_X.shape[2]))

        self.train_counter = (
            self.train_counter + self.params.batch_size_eval) % self.train_data_size
        return(next_batch_X, self.files[self.train_counter:borne_sup])

    def reset_counter(self):
        self.train_counter = 0


def evaluate_step_i(params, iteration_f, iteration_adv):  # if iteration_adv == -1 : cover

    net = get_net().cuda()
    path = params.save_path + "last-checkpoint.bin"
    checkpoint = torch.load(path)
    net.load_state_dict(checkpoint['model_state_dict'])

   # Create directory
    if(iteration_adv == -1):
        directory = params.data_dir_prot+'cover/eval_f'+str(iteration_f)+'/'
        dataloader = cover_stego_loader(params, iteration_adv, 'cover')
    else:
        directory = params.data_dir_prot+'data_adv_' + \
            str(iteration_adv)+'/eval_f'+str(iteration_f)+'/'
        dataloader = cover_stego_loader(params, iteration_adv, 'stego')

    dataloader = cover_stego_loader(params, iteration_adv, 'stego')
    result_fi = np.empty((0, 2))
    dataloader.reset_counter()
    for batch in range(dataloader.train_num_batches):
        batch_x, images_path = dataloader.next_batch()
        with torch.no_grad():
            l = net.forward(torch.tensor(batch_x).cuda())
        result_fi = np.concatenate((result_fi, l.cpu().detach().numpy()))
    np.save(directory+'probas', softmax(result_fi)[:, 1])
    np.save(directory+'logits', result_fi)
    return(result_fi, softmax(result_fi)[:, 1])
